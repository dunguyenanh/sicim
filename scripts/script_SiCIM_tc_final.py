################################################################################
# It's a very messy file, so here is the structure of the file
# I had no time to clean it
# 1. Some funtion to print with color
# 2. Options of the scripts
# 3. Folders to excute and produce results
# 4. Conversion of problem size to bits
# 5. Output file names and setting up parameters
# 6. Some functions for checking
# 7. CACTI for DRAM estimation
# 8. Mcpat for processors and L1, L2 caches
# 9. SiNUCA results
# 10. McPAT results
# 11. NVSIM results
# 12. Estimation model
# 13. Execute SiNUCA
# 14. Execute NVSIM
# 15. Execute for one architecture (CIMA or CPU)
# 16. Initialize setting and parameter for further execution
# 17. Output results to a file --- to be processed in Matlab
# 18. Generate traces (if called to generate traces only)
# 19. Complete execution
# ---19.1. Generate traces  
# ---19.2. Execute both architectures	
# ---19.3. Execute each architecture as specify in the input parameters

import sys
import os
import subprocess
import re
import stat
import math
#import xlsxwriter

################################################################################
# Define colors
################################################################################
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
	
    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''

################################################################################
# This prints a passed string into this function
# 1. Some funtion to print with color
################################################################################
def PRINT( str ):
   print "PYTHON: " + str
   return

def PRINT_WARN( str ):
   print bcolors.WARNING +  "PYTHON: " + str + bcolors.ENDC
   return

def PRINT_ERR( str ):
   print bcolors.FAIL +  "PYTHON: " + str + bcolors.ENDC
   return

def PRINT_GOOD( str ):
   print bcolors.OKGREEN +  "PYTHON: " + str + bcolors.ENDC
   return

def PRINT_DEBUG( str ):
   print bcolors.OKBLUE +  "PYTHON: " + str + bcolors.ENDC
   return

################################################################################
# This listed all options to run the whole model
# 2. Options of the scripts
# execution_list has three modes: 
# --- result to only run the estimation model (with cacti and mcpat-- still has some errors) 
# --- trace to generate only traces 
# --- execute to generate traces, run sinuca, and estimate using models
# benchmark_list has all the benchmarks that can be run
# --- vecsum: do logical operation using the trace provided by Marco, it's lighter, faster, without any program overhead
# --- forloop: do the same logical operation using a C program, it's slower, as there is a lot program overhead
# --- simple_otp_2: perform encryption application, using a C program, there must be a c program in the examples folder
# --- tcph: perform bitmap indexing operation of TCP-H, traces from Macro, only one problem size of 1GB
# --- mem-base: perform memory accesses using for loop with regular iteration - constant strike - Imran provided this
# --- mem-indirection: perform memory accesses using for loop with irregular iteration - long strike - Imran provided this
# --- mem_random: perform memory accesses using for loop with irregular iteration - random strike - Imran provided this
# architecture_list has two architecture: CPU and CIMA, they can both be run with the term "all" 
# they both has the same configurations such as
# a single cpu, l1, l2 caches, maybe l3 cache. But CPU has 1GB DRAM, CIMA has 500MB DRAM and also has extra 500MB NVM.
# These specify in the configuration file of MCPAT, will be generated using a form of .xml file.
# !!!Note: if the options are not listed in these list, the script will not run!!!
# Other arguments from the run_script file will feed into the latter field. 
# They can be used or not used later. But a full arguments are required to execute the scripts.
################################################################################
EXECUTION_LIST = ["result", "trace", "execute"]
BENCHMARK_LIST = ["vecsum", "forloop", "simple_otp_2", "tcph", 'mem_base', 'mem_indirection', 'mem_random']
ARCHITECURE_LIST = ["cpu", "cima"]
#ARCHITECURE_LIST = ["cpu", "hive", "cima"]

## Check the ARGS and [EXECUTION] and [BENCHMARK] and [ARCHITECTURE]
if ((len(sys.argv) != 13) or (sys.argv[1] not in EXECUTION_LIST) or (sys.argv[2] not in BENCHMARK_LIST) or ((sys.argv[3] not in ARCHITECURE_LIST) and (sys.argv[3] != "all"))):
    if (len(sys.argv) != 13):
        PRINT_ERR("Missing parameter!\n")
    elif (sys.argv[1] not in EXECUTION_LIST):
        PRINT_ERR("Wrong execution command!\n")
    elif (sys.argv[2] not in BENCHMARK_LIST):
        PRINT_ERR("Wrong benchmark!\n")
    elif ((sys.argv[3] not in ARCHITECURE_LIST) and (sys.argv[3] != "all")):
        PRINT_ERR("Wrong architecture!\n")
    sys.exit("Usage:\n"
            +"\t python script_sinuca.py \\\n"
            +"\t\t"+str(EXECUTION_LIST)+" \\\n"
            +"\t\t"+str(BENCHMARK_LIST)+ " \\\n"
			+"\t\t"+str(ARCHITECURE_LIST)+ " \\\n"
            +"\t\t ARG_MCPAT_TEMPL_CPU \n"
			+"\t\t ARG_MCPAT_TEMPL_CIMA \n"
			+"ARG_SINUCA_CONF_FOLDER_CPU  \n"
			+"ARG_SINUCA_CONF_FOLDER_CIMA  \n"
			+"ARG_PROBLEM_SIZE \n"
			+"ARG_DATA_WIDTH_CPU \n"
			+"ARG_DATA_WIDTH_CIMA \n"
			+"ARG_NUM_CORES_CPU \n" 
			+"ARG_SYNC_CONF \n" )
else:
    arg_execution = sys.argv[1]
    arg_benchmark = sys.argv[2]
    arg_architecture = sys.argv[3]
    arg_mcpat_template_cpu = sys.argv[4]
    arg_mcpat_template_cima = sys.argv[5]
    arg_sinuca_conf_folder_cpu = sys.argv[6]
    arg_sinuca_conf_folder_cima = sys.argv[7]
    arg_problem_size = sys.argv[8]
    arg_data_width_cpu = sys.argv[9]
    arg_data_width_cima = sys.argv[10]
    arg_num_cores_cpu = sys.argv[11]
    arg_sync_conf = sys.argv[12]

################################################################################
## Export variables
# 3. Folders to excute and produce results
# !!! Folder of all the tools required are listed here.
# This script need:
# --- PIN (include PIN and PINPLAY) for the traces
# --- MCPAT for CPU and CACHES
# --- CACTI for DRAM
# --- NVSIM for NVM
# --- BOOST for the extension of CIM instruction
################################################################################
PRINT_GOOD("============================= Folders checking ==========================")	
PROJECT_HOME = "/shares/bulk/hadunguyen/SiCIM/"
os.putenv("PROJECT_HOME", PROJECT_HOME)
os.system("echo PROJECT_HOME=$PROJECT_HOME")

#WORKING_DIR   =  "/home/hadunguyen/bitbucket_repository/sinuca-cim-allInstr/examples/" +  arg_benchmark + "14/"
#WORKING_DIR   =  "/home/hadunguyen/bulk/sinuca/sinuca-cim/examples/" +  arg_benchmark + "_04/"
# The folder should not contain traces, .sinuca file
# If those files are ready, the traces file can be overridden, 
# but SiNUCA simulation will not re-executed, old file .sinuca will be used for the simulation
# I don't want to add automated remove option here, in case some important files are removed!!!
# rm $(ls -I "*.cpp" )
WORKING_DIR   =  PROJECT_HOME + "sinuca-cim/examples/" +  arg_benchmark + "_" + arg_sync_conf + "/"
if (os.path.isdir(WORKING_DIR)):
    os.putenv("WORKING_DIR", WORKING_DIR)
    os.system("echo WORKING_DIR=$WORKING_DIR")	
else :
    if (not os.path.isdir(WORKING_DIR)):
        sys.exit("Directory " + WORKING_DIR + " does not exist\n")

PRINT_GOOD("============================= Tools preparing ===========================")			

PIN_HOME = PROJECT_HOME + "pin-3.2-81205-gcc-linux/"
PIN_OPT = " -ifeellucky -t "
os.putenv("PIN_HOME", PIN_HOME)
os.system("echo PIN_HOME=$PIN_HOME")
#os.system("export PIN_HOME=$PIN_HOME")

PINPLAY_HOME = PROJECT_HOME + "pinplay-drdebug-3.2-pin-3.2-81205-gcc-linux/"
os.putenv("PINPLAY_HOME", PINPLAY_HOME)
os.system("echo PINPLAY_HOME=$PINPLAY_HOME")
#os.system("export PINPLAY_ROOT=$PINPLAY_HOME")

BOOST_HOME = PROJECT_HOME + "boost_1_65_1/"
os.putenv("BOOST_HOME", BOOST_HOME)
os.system("echo BOOST_HOME=$BOOST_HOME")

McPAT_HOME =  PROJECT_HOME + "mcpat/"
os.putenv("McPAT_HOME", McPAT_HOME)
os.system("echo McPAT_HOME=$McPAT_HOME")

CACTI_HOME =  PROJECT_HOME + "cacti65/"
os.putenv("CACTI_HOME", CACTI_HOME)
os.system("echo CACTI_HOME=$CACTI_HOME")

SINUCA_HOME = PROJECT_HOME + "sinuca-cim/"
#SINUCA_HOME = "/home/hadunguyen/bitbucket_repository/sinuca-cim-allInstr/"
os.putenv("SINUCA_HOME", SINUCA_HOME)
os.system("echo SINUCA_HOME=$SINUCA_HOME")

PIN_TRACER = PINPLAY_HOME + "extras/pinplay/bin/intel64/sinuca_tracer.so"
os.putenv("PIN_TRACER", PIN_TRACER)
os.system("echo PIN_TRACER=$PIN_TRACER")

NVSIM_HOME = PROJECT_HOME + "nvsim/"
os.putenv("NVSIM_HOME", NVSIM_HOME)
os.system("echo NVSIM_HOME=$NVSIM_HOME")


## Openned Files
os.system("ulimit -n 2048")

################################################################################
## Specific benchmark related parameters
# 4. Conversion of problem size to bits
################################################################################
# problem size in bits
PROBLEM_SIZE_DICT = { '1KB': 8192,\
					  '2KB': 16384,\
					  '4KB': 32768,\
					  '8KB': 65536,\
					  '16KB': 131072,\
					  '32KB': 262144,\
					  '64KB': 524288,\
					  '128KB': 1048576,\
					  '256KB': 2097152,\
					  '512KB': 4194304,\
					  '1MB': 8388608,\
					  '2MB': 16777216,\
					  '4MB': 33554432,\
					  '8MB': 67108864,\
					  '16MB': 134217728,\
					  '32MB': 268435456,\
					  '64MB': 536870912,\
					  '128MB': 1073741824,\
					  '256MB': 2147483648,\
					  '512MB': 4294967296,\
					  '1GB': 8589934592	}


################################################################################
## Generate file names and transferring parameter to correct form for latter use
# 5. Output file names and setting up parameters
################################################################################		
def set_param (func_arg_benchmark, func_arg_architecture, func_arg_mcpat_template_cpu, func_arg_mcpat_template_cima, \
				 func_arg_sinuca_conf_folder_cpu, func_arg_sinuca_conf_folder_cima, func_arg_problem_size, \
				 func_arg_data_width_cpu, func_arg_data_width_cima, func_arg_num_cores_cpu, func_arg_sync_conf):
# def setting (arg_benchmark, arg_architecture, arg_mcpat_template_cpu, arg_mcpat_template_cima,arg_sinuca_conf_folder_cpu, arg_sinuca_conf_folder_cima, arg_problem_size, arg_data_width_cpu, arg_data_width_cima, arg_num_cores_cpu, arg_sync_conf)
# problem size in bits

	thread_opt	= 1
	arg_mcpat_template = 'mcpattemplate'	
	arg_sinuca_conf_folder = 'sinucafolder'
	arg_loop_size = 1
	cpp_file_name = 'test.cpp'
	bin_file_name = 'test'
	arg_basename = func_arg_architecture + "_" + func_arg_benchmark + "_" + func_arg_problem_size 
	if (func_arg_architecture == 'cpu'):
		arg_data_width = func_arg_data_width_cpu
		arg_mcpat_template = arg_mcpat_template_cpu
		arg_sinuca_conf_folder = arg_sinuca_conf_folder_cpu		
		if 'vecsum' in func_arg_benchmark or ('tcph' in func_arg_benchmark):
			arg_conf =  "_" + str(arg_data_width) + "B_" + func_arg_num_cores_cpu + "cores_" + func_arg_sync_conf

		elif 'forloop' in func_arg_benchmark:
			func_arg_benchmark = func_arg_benchmark + "_arg" 
			arg_loop_size = PROBLEM_SIZE_DICT[func_arg_problem_size] // int(arg_data_width)
			thread_opt = func_arg_num_cores_cpu
			arg_conf =  "_" + str(arg_data_width) + "B_" + func_arg_num_cores_cpu + "cores_" + func_arg_sync_conf
			cpp_file_name = func_arg_architecture + "_" + func_arg_benchmark + ".cpp"
			bin_file_name = func_arg_architecture + "_" + func_arg_benchmark			

		elif 'simple_otp' in func_arg_benchmark:
			func_arg_benchmark = func_arg_benchmark
			arg_loop_size = convert2byte(func_arg_problem_size)
			thread_opt = func_arg_num_cores_cpu
			arg_conf =  "_" + str(arg_data_width) + "B_" + func_arg_num_cores_cpu + "cores_" + func_arg_sync_conf
			cpp_file_name = func_arg_architecture + "_" + func_arg_benchmark + ".cpp"
			bin_file_name = func_arg_architecture + "_" + func_arg_benchmark

		else:
			func_arg_benchmark = func_arg_benchmark
			arg_loop_size = 0
			thread_opt = func_arg_num_cores_cpu
			arg_conf =  "_" + str(arg_data_width) + "B_" + func_arg_num_cores_cpu + "cores_" + func_arg_sync_conf
			cpp_file_name = func_arg_architecture + "_" + func_arg_benchmark + ".cpp"
			bin_file_name = func_arg_architecture + "_" + func_arg_benchmark
			
	elif (func_arg_architecture == 'cima'):
		arg_data_width = func_arg_data_width_cima
		arg_mcpat_template = func_arg_mcpat_template_cima
		arg_sinuca_conf_folder = func_arg_sinuca_conf_folder_cima
		if 'vecsum' in func_arg_benchmark or ('tcph' in func_arg_benchmark):
			arg_conf = "_" + str(arg_data_width) + "B_" + func_arg_sync_conf

		elif 'forloop' in func_arg_benchmark:
			func_arg_benchmark = func_arg_benchmark + "_arg" 
			arg_loop_size = PROBLEM_SIZE_DICT[func_arg_problem_size] // int(arg_data_width)
			cpp_file_name = func_arg_architecture + "_" + func_arg_benchmark + ".cpp"
			bin_file_name = func_arg_architecture + "_" + func_arg_benchmark
			thread_opt = func_arg_num_cores_cpu # no use
			arg_conf =  "_" + str(arg_data_width) + "B_" + func_arg_sync_conf

		elif 'simple_otp' in func_arg_benchmark:
			func_arg_benchmark = func_arg_benchmark
			arg_loop_size = convert2byte(func_arg_problem_size)
			cpp_file_name = func_arg_architecture + "_" + func_arg_benchmark + ".cpp"
			bin_file_name = func_arg_architecture + "_" + func_arg_benchmark
			thread_opt = func_arg_num_cores_cpu # no use
			arg_conf =  "_" + str(arg_data_width) + "B_" + func_arg_sync_conf	
		
	else:
		#arg_basename = ARCHITECURE_LIST[1] + "_" + func_arg_benchmark + "_" + func_arg_problem_size
		arg_data_width = func_arg_data_width_cima
		arg_mcpat_template = func_arg_mcpat_template_cpu
		arg_sinuca_conf_folder = func_arg_sinuca_conf_folder_cpu
		
		if 'vecsum' in func_arg_benchmark or ('tcph' in func_arg_benchmark):
			arg_conf =  "_" + str(arg_data_width) + "B_" + func_arg_num_cores_cpu + "cores_" + func_arg_sync_conf

		elif 'forloop' in func_arg_benchmark:
			func_arg_benchmark = func_arg_benchmark + "_arg" 
			arg_loop_size = PROBLEM_SIZE_DICT[func_arg_problem_size] // int(arg_data_width)
			cpp_file_name = func_arg_architecture + "_" + func_arg_benchmark + ".cpp"
			bin_file_name = func_arg_architecture + "_" + func_arg_benchmark
			thread_opt = func_arg_num_cores_cpu
			arg_conf =  "_" + str(arg_data_width) + "B_" + func_arg_num_cores_cpu + "cores_" + func_arg_sync_conf

		elif 'simple_otp' in func_arg_benchmark:
			func_arg_benchmark = func_arg_benchmark
			arg_loop_size = convert2byte(func_arg_problem_size)
			cpp_file_name = func_arg_architecture + "_" + func_arg_benchmark + ".cpp"
			bin_file_name = func_arg_architecture + "_" + func_arg_benchmark
			thread_opt = func_arg_num_cores_cpu
			arg_conf =  "_" + str(arg_data_width) + "B_" + func_arg_num_cores_cpu + "cores_" + func_arg_sync_conf
			
	arg_filename = arg_basename + arg_conf
	return { 'filename': arg_filename,\
			 'mcpat'   : arg_mcpat_template,\
			 'sinuca'  : arg_sinuca_conf_folder,\
			 'threads' : thread_opt,\
			 'loopsize' : arg_loop_size,\
			 'cpp'     : cpp_file_name,\
			 'bin'     : bin_file_name}
	
	
################################################################################
# This check if a number
# 6. Some functions for checking
########################################################################
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False
################################################################################
# This convert to byte
# 6. Some functions for checking
########################################################################
def convert2byte(s):
	num = map(int, re.findall('\d+', s))
	print(str(num[0]) + '\n')
	if ('K' in s):
		return 1024 * int(num[0])
	elif ('M' in s):
		return 1024 * 1024 * int(num[0])
	elif ('G' in s):
		return 1024 * 1024 * 1024 * int(num[0])

################################################################################
# This check to delele file
# 6. Some functions for checking
################################################################################
def check_file_del (func_arg_app_file_name, extension):
	check_file_name = WORKING_DIR + func_arg_app_file_name + extension
	status = os.path.isfile(check_file_name)
	if (status):
		PRINT_WARN('File exists: ' + check_file_name)
		os.system("rm " + check_file_name )
		PRINT_WARN('File deleted to continue')

	return status
################################################################################
# This check if a file exists
# 6. Some functions for checking
################################################################################
def check_file_exist (func_arg_app_file_name, extension):
	check_file_name = WORKING_DIR + func_arg_app_file_name + extension
	status = os.path.isfile(check_file_name)
	if os.path.isfile(check_file_name):
		PRINT_GOOD('File ready: ' + func_arg_app_file_name + extension)
	else:
		sys.exit("File does not exist" + func_arg_app_file_name + extension + "\n")
	return status
	
################################################################################
# This check if all trace file exists
# 6. Some functions for checking
################################################################################	
def check_trace_file_exist (func_arg_app_file_name):
	for type in ["stat", "dyn", "mem"]:
		if ((type != "stat") and ('cpu' in func_arg_app_file_name)):
			for ths in range (0, int(thread_opt)):
				TRACE_EXTENSION = ".tid" + str(ths) + "." + type + ".out.gz"
				check_file_name = WORKING_DIR + func_arg_app_file_name + TRACE_EXTENSION
				status = os.path.isfile(check_file_name)
				if (status == 0):
					return status
				else:
					PRINT_WARN("Traces: " + check_file_name)
		else:
			TRACE_EXTENSION = ".tid0." + type + ".out.gz"
			check_file_name = WORKING_DIR + func_arg_app_file_name + TRACE_EXTENSION
			status = os.path.isfile(check_file_name)
			if (status == 0):
				return status
			else:
				PRINT_WARN("Traces: " + check_file_name)
	return status
################################################################################
# This convert units
# 6. Some functions for checking
################################################################################
def from_mega(num):
    res = num * (10**6)
    return res

def to_nano(num):
    res = num * (10**9)
    return res
	

############################################################################
### CACTI FOR DRAM
# 7. CACTI for estimation DRAM cost based on number of memory accesses
############################################################################

# Function called by cacti_defined_cfg
def cacti_base_cfg(TMP_CFG_FILE_NAME):
    TMP_CFG_FILE = open(TMP_CFG_FILE_NAME, 'a')

    # 300-400 in steps of 10
    TMP_CFG_FILE.write("-operating temperature (K) 350 \n")

    # DESIGN OBJECTIVE for UCA (or banks in NUCA)
    TMP_CFG_FILE.write("-design objective (weight delay, dynamic power, leakage power, cycle time, area) 0:0:100:0:0 \n")

    # Percentage deviation from the minimum value
    # Ex: A deviation value of 10:1000:1000:1000:1000 will try to find an organization
    # that compromises at most 10% delay.
    TMP_CFG_FILE.write("-deviate (delay, dynamic power, leakage power, cycle time, area) 20:100000:100000:100000:100000 \n")

    # Objective for NUCA
    TMP_CFG_FILE.write("-NUCAdesign objective (weight delay, dynamic power, leakage power, cycle time, area) 100:100:0:0:100 \n")
    TMP_CFG_FILE.write("-NUCAdeviate (delay, dynamic power, leakage power, cycle time, area) 10:10000:10000:10000:10000 \n")

    # Set optimize tag to ED or ED^2 to obtain a cache configuration optimized for
    # energy-delay or energy-delay sq. product
    # Note: Optimize tag will disable weight or deviate values mentioned above
    # Set it to NONE to let weight and deviate values determine the
    # appropriate cache configuration
    TMP_CFG_FILE.write("-Optimize ED or ED^2 (ED, ED^2, NONE): \"NONE\" \n")

    # In order for CACTI to find the optimal NUCA bank value the following
    # variable should be assigned 0.
    TMP_CFG_FILE.write("-NUCA bank count 0 \n")

    # NOTE: for nuca network frequency is set to a default value of
    # 5GHz in time.c. CACTI automatically
    # calculates the maximum possible frequency and downgrades this value if necessary

    # Interconnection project type "conservative" or "aggressive"
    TMP_CFG_FILE.write("-Interconnect projection - \"conservative\" \n")

    # Contention in network (which is a function of core count and cache level) is one of
    # the critical factor used for deciding the optimal bank count value
    # core count can be 4, 8, or 16
    TMP_CFG_FILE.write("-Core count 8 \n")

    # Enable ECC error checking
    TMP_CFG_FILE.write("-Add ECC - \"true\" \n")

    # Output detail level "DETAILED" or "CONCISE"
    TMP_CFG_FILE.write("-Print level (DETAILED, CONCISE) - \"CONCISE\" \n")

    # for debugging ("true" or "false")
    TMP_CFG_FILE.write("-Print input parameters - \"true\" \n")

    # force CACTI to model the cache with the
    # following Ndbl, Ndwl, Nspd, Ndsam,
    # and Ndcm values
    TMP_CFG_FILE.write("-Force cache config - \"false\" \n")
    TMP_CFG_FILE.write("-Ndwl 1 \n")
    TMP_CFG_FILE.write("-Ndbl 1 \n")
    TMP_CFG_FILE.write("-Nspd 0 \n")
    TMP_CFG_FILE.write("-Ndcm 1 \n")
    TMP_CFG_FILE.write("-Ndsam1 0 \n")
    TMP_CFG_FILE.write("-Ndsam2 0 \n")

    # FOR cacti 6.5
    # ~ TMP_CFG_FILE.write("-Ndwl 64 \n")
    # ~ TMP_CFG_FILE.write("-Ndbl 64 \n")
    # ~ TMP_CFG_FILE.write("-Nspd 64 \n")
    # ~ TMP_CFG_FILE.write("-Ndcm 1 \n")
    # ~ TMP_CFG_FILE.write("-Ndsam1 4 \n")
    # ~ TMP_CFG_FILE.write("-Ndsam2 1 \n")

    TMP_CFG_FILE.write("-Power Gating - \"false\"\n")

    # Customized VDD
    TMP_CFG_FILE.write("-hp Vdd (V) \"default\"\n")
    TMP_CFG_FILE.write("-lstp Vdd (V) \"default\"\n")
    TMP_CFG_FILE.write("-lop Vdd (V) \"default\"\n")

    TMP_CFG_FILE.close()
    return
# Function called by DynEnergy_StatPower
def cacti_defined_cfg(TMP_CFG_FILE_NAME, CacheLevel, CacheSize, LineSize, CacheAssoc, IO_Output, TechInt, BankCount, CacheType, TagSize, UCA, AccessMode, DataArrayCell, DataArrayType, TagArrayCell, TagArrayType, RW_Port, R_Port, W_Port, PageSize, Burst, Pfetch):
    TMP_CFG_FILE = open(TMP_CFG_FILE_NAME, 'w')

    TMP_CFG_FILE.write("####################################################\n")
    # Cache Level
    TMP_CFG_FILE.write("-Cache level (L2/L3) - \"L"+str(CacheLevel)+"\" \n")

    ### Cache size
    TMP_CFG_FILE.write("-size (bytes) "+str(CacheSize)+" \n")
    ### Line size
    TMP_CFG_FILE.write("-block size (bytes) "+str(LineSize)+" \n")
    ### To model Fully Associative cache, set associativity to zero
    TMP_CFG_FILE.write("-associativity "+str(CacheAssoc)+" \n")
    ### Multiple banks connected using a bus
    TMP_CFG_FILE.write("-UCA bank count "+str(BankCount)+" \n")
    ### Integration Technology -- (0.032, 0.045, 0.068, 0.090)
    TMP_CFG_FILE.write("-technology (u) "+str(TechInt)+" \n")
    ### Bus width include data bits and address bits required by the decoder
    TMP_CFG_FILE.write("-output/input bus width "+str(IO_Output)+" \n")
    # Type of memory - cache (with a tag array) or ram (scratch ram similar to a register file)
    # or main memory (no tag array and every access will happen at a page granularity Ref: CACTI 5.3 report)
    TMP_CFG_FILE.write("-cache type \""+CacheType+"\" \n")
    # to model special structure like branch target buffers, directory, etc.
    # change the tag size parameter if you want cacti to calculate the tagbits, set the tag size to "default"
    #//-tag size (b) "default"
    TMP_CFG_FILE.write("-tag size (b) "+str(TagSize)+" \n")# 42(Normal) +8(Sectors)
    # Normal "UCA" or multi-banked "NUCA" cache.
    TMP_CFG_FILE.write("-Cache model (NUCA, UCA)  - \""+str(UCA)+"\" \n")
    # fast - data and tag access happen in parallel
    # sequential - data array is accessed after accessing the tag array
    # normal - data array lookup and tag access happen in parallel
    #          final data block is broadcasted in data array h-tree
    #          after getting the signal from the tag array
    TMP_CFG_FILE.write("-access mode (normal, sequential, fast) - \""+AccessMode+"\" \n")

    # following parameter can have one of five values -- (itrs-hp, itrs-lstp, itrs-lop, lp-dram, comm-dram)
    TMP_CFG_FILE.write("-Data array cell type - \""+DataArrayCell+"\" \n")
    TMP_CFG_FILE.write("-Data array peripheral type - \""+DataArrayType+"\" \n")

    TMP_CFG_FILE.write("-Tag array cell type - \""+TagArrayCell+"\" \n")
    TMP_CFG_FILE.write("-Tag array peripheral type - \""+TagArrayType+"\" \n")

    # Input and Output ports
    TMP_CFG_FILE.write("-read-write port "+str(RW_Port)+" \n")
    TMP_CFG_FILE.write("-exclusive read port "+str(R_Port)+" \n")
    TMP_CFG_FILE.write("-exclusive write port "+str(W_Port)+" \n")
    TMP_CFG_FILE.write("-single ended read ports 0 \n")

    # following three parameters are meaningful only for main memories
    if (CacheType == "main memory"):
        TMP_CFG_FILE.write("-page size (bits) "+str(PageSize)+" \n")
        TMP_CFG_FILE.write("-burst length "+str(Burst)+" \n")
        TMP_CFG_FILE.write("-internal prefetch width "+str(Pfetch)+" \n")

        # By default CACTI considers both full-swing and low-swing
        # wires to find an optimal configuration. However, it is possible to
        # restrict the search space by changing the signalling from "default" to
        # "fullswing" or "lowswing" type.
        TMP_CFG_FILE.write("-Wire signalling (fullswing, lowswing, default) - \"Global_10\" \n")

        # Wire inside the mat "global" or "semi-global"
        TMP_CFG_FILE.write("-Wire inside mat - \"global\" \n")
        # Wire outside the mat
        TMP_CFG_FILE.write("-Wire outside mat - \"global\" \n")

    else:
        TMP_CFG_FILE.write("-page size (bits) 8192 \n")
        TMP_CFG_FILE.write("-burst length 8 \n")
        TMP_CFG_FILE.write("-internal prefetch width 8 \n")

        # Whether to use long (10%) channel devices
        # When "true" assuming 90% of the device (non time critical) can be long channel device
        if (CacheLevel == 3):
            TMP_CFG_FILE.write("-Long channel devices - \"true\"\n")
        else :
            TMP_CFG_FILE.write("-Long channel devices - \"false\"\n")

        # By default CACTI considers both full-swing and low-swing
        # wires to find an optimal configuration. However, it is possible to
        # restrict the search space by changing the signalling from "default" to
        # "fullswing" or "lowswing" type.
        TMP_CFG_FILE.write("-Wire signalling (fullswing, lowswing, default) - \"Global_30\" \n")

        # Wire inside the mat "global" or "semi-global"
        TMP_CFG_FILE.write("-Wire inside mat - \"semi-global\" \n")
        # Wire outside the mat
        TMP_CFG_FILE.write("-Wire outside mat - \"semi-global\" \n")




    TMP_CFG_FILE.write("####################################################\n")
    TMP_CFG_FILE.close()
    return
########################################################################
# Functions called by cacti_dram
def DynEnergy_StatPower(PredictorName, CacheLevel, CacheSize, LineSize, CacheAssoc, IO_Output, TechInt, BankCount, CacheType, TagSize, UCA, AccessMode, DataArrayCell, DataArrayType, TagArrayCell, TagArrayType, RW_Port, R_Port, W_Port, PageSize, Burst, Pfetch):

    # Maintain the Energy for the total access
    read_dynamic_energy = 0.0
    write_dynamic_energy = 0.0
    # Maintain the Energy for the total cycles
    static_power = 0.0
    area_mm2 = 1.0

    if (LineSize < 2):
        LineSize = 2

    TMP_FILE_NAME = "cacti_"+ PredictorName \
                    + "_L"+ str(CacheLevel) \
                    + "_CS"+ str(CacheSize) \
                    + "_LS"+ str(LineSize) \
                    + "_CA"+ str(CacheAssoc) \
                    + "_CO"+ str(IO_Output) \
                    + "_TI"+ str(TechInt) \
                    + "_BC"+ str(BankCount) \
                    + "_CT"+ str(TagSize) \
                    + "_RWP"+ str(RW_Port) \
                    + "_RP"+ str(R_Port) \
                    + "_WP"+ str(W_Port) \
                    + "_"  + re.sub(r"\s+", '_', CacheType) # Replace all runs of whitespace with a single underline
    TMP_CFG_FILE_NAME = WORKING_DIR + TMP_FILE_NAME + ".cfg"
    TMP_OUT_FILE_NAME = WORKING_DIR + TMP_FILE_NAME + ".out"

    try:
        TMP_OUT_FILE = open(TMP_OUT_FILE_NAME, 'r')
        #print("Using Old Cacti Model: " + TMP_OUT_FILE_NAME)
    except IOError:
        PRINT("Creating New Cacti Model: " + TMP_OUT_FILE_NAME)
        cacti_defined_cfg(TMP_CFG_FILE_NAME, CacheLevel, CacheSize, LineSize, CacheAssoc,\
                            IO_Output, TechInt, BankCount, CacheType, TagSize, UCA, AccessMode,\
                            DataArrayCell, DataArrayType, TagArrayCell, TagArrayType, RW_Port, R_Port, W_Port,\
                            PageSize, Burst, Pfetch)
        cacti_base_cfg(TMP_CFG_FILE_NAME)
        os.system(CACTI_HOME + "cacti -infile " + TMP_CFG_FILE_NAME + " > " + TMP_OUT_FILE_NAME)
        TMP_OUT_FILE = open(TMP_OUT_FILE_NAME, 'r')

    if (CacheType == "main memory"):
        for out_file_line in TMP_OUT_FILE:
            out_file_line = out_file_line.rstrip('\r\n')
            # ~ if  "Total dynamic read energy/access" in out_file_line: #(nJ)
            if  "Total dynamic read energy per access (nJ)" in out_file_line: #(nJ)
                out_file_line_split = out_file_line.split(": ")
                for number in out_file_line_split:
                    if is_number(number.strip()):
                        if (read_dynamic_energy != 0.0) :
                            PRINT_ERR("Found multiple dynamic energy results")
                            # ~ exit()
                        else:
                            # ~ PRINT("Dynamic: " + number)
                            read_dynamic_energy = float(number)
            if  "Read Energy (nJ)" in out_file_line: #(nJ)
                out_file_line_split = out_file_line.split(": ")
                for number in out_file_line_split:
                    if is_number(number.strip()):
                        if (read_dynamic_energy != 0.0) :
                            PRINT_ERR("Found multiple dynamic energy results")
                            # ~ exit()
                        else:
                            # ~ PRINT("Dynamic: " + number)
                            read_dynamic_energy = float(number)

            if  "Write Energy (nJ)" in out_file_line: #(nJ)
                out_file_line_split = out_file_line.split(": ")
                for number in out_file_line_split:
                    if is_number(number.strip()):
                        if (write_dynamic_energy != 0.0) :
                            PRINT_ERR("Found multiple dynamic energy results")
                            # ~ exit()
                        else:
                            # ~ PRINT("Dynamic: " + number)
                            write_dynamic_energy = float(number)


            if  "Total leakage power of a bank without power gating, including its network outside (mW)" in out_file_line: #(mW)
                out_file_line_split = out_file_line.split(": ")
                for number in out_file_line_split:
                    if is_number(number.strip()):
                        # ~ PRINT("Leakage: " + number)
                        static_power += float(number)
            if  "Leakage Power Open Page (mW)" in out_file_line: #(mW)
                out_file_line_split = out_file_line.split(": ")
                for number in out_file_line_split:
                    if is_number(number.strip()):
                        # ~ PRINT("Leakage: " + number)
                        static_power += float(number)
            if  "Leakage Power I/O (mW)" in out_file_line: #(mW)
                out_file_line_split = out_file_line.split(": ")
                for number in out_file_line_split:
                    if is_number(number.strip()):
                        # ~ PRINT("Leakage: " + number)
                        static_power += float(number)
            if  "Refresh power (mW)" in out_file_line: #(mW)
                out_file_line_split = out_file_line.split(": ")
                for number in out_file_line_split:
                    if is_number(number.strip()):
                        # ~ PRINT("Leakage: " + number)
                        static_power += float(number)
            if  "Cache height x width (mm)" in out_file_line: #(mW)
                out_file_line_split = out_file_line.split(": ")
                number_split = out_file_line_split[-1].split(" x ")
                for number in number_split:
                    if is_number(number.strip()):
                        area_mm2 *= float(number)
                        # ~ PRINT("Area: " + str(area))
						
    else:
        for out_file_line in TMP_OUT_FILE:
            out_file_line = out_file_line.rstrip('\r\n')
            # ~ if  "Total dynamic read energy/access" in out_file_line: #(nJ)
            if  "Total dynamic read energy per access" in out_file_line: #(nJ)
                out_file_line_split = out_file_line.split(": ")
                for number in out_file_line_split:
                    if is_number(number.strip()):
                        if (read_dynamic_energy != 0.0) :
                            PRINT_ERR("Found multiple dynamic energy results")
                            # ~ exit()
                        else:
                            # ~ PRINT("Dynamic: " + number)
                            read_dynamic_energy = float(number)
            # ~ elif  "Total leakage read/write power of a bank" in out_file_line: #(mW)
            if  "Total leakage power of a bank" in out_file_line: #(mW)
                out_file_line_split = out_file_line.split(": ")
                for number in out_file_line_split:
                    if is_number(number.strip()):
                        if (static_power != 0.0) :
                            PRINT_ERR("Found multiple static energy results")
                            # ~ exit()
                        else:
                            # ~ PRINT("Leakage: " + number)
                            static_power = float(number)

    TMP_OUT_FILE.close()

    if (read_dynamic_energy == 0.0) or (static_power == 0.0) :
        PRINT_ERR("Could not find energy results")
        exit()

    if (area_mm2 == 1.0) :
        PRINT_ERR("Could not find area results")
        exit()
		
    return {'area_mm2': area_mm2,\
			'read_dyn': read_dynamic_energy,\
			'write_dyn': write_dynamic_energy,\
			'stat': static_power}	
def cactiDRAM(func_arg_app_file_name, func_arg_mem_size_mb):

    PRINT_GOOD("============================= Cacti DRAM modeling ===========================")
    PRINT("===================================================================")
    PRINT("Install CACTI inside: " + CACTI_HOME)
    PRINT("===================================================================")

    app_file_name = WORKING_DIR + func_arg_app_file_name + ".sinuca"
    try:
        app_file = open(app_file_name, 'r')
    except IOError:
		# ~ PRINT_WARN("\t Application Not Found: " + arg_benchmark)
		PRINT_WARN("\t File Not Found: " + app_file_name + " ... skipping")
		#continue

	############################################################################
	### Find Memory Names
	############################################################################
    memory_label_list = []
    configurations_inside_file = 0
	# Iterates over the application results FILE
    app_file.seek(0,0)
    for parameter_line in app_file:
		parameter_line = parameter_line.rstrip('\r\n')
		#Comments
		if (parameter_line == "#Configuration of SINUCA_ENGINE"):
			configurations_inside_file += 1
			if (configurations_inside_file > 1):
				PRINT_ERR("Multiple results for application: " + app_file_name)
				exit()

		if parameter_line[0] == '#':
			continue

		split_parameter_line = parameter_line.split('.')
		if (split_parameter_line[0] == "MEMORY_CONTROLLER") and (split_parameter_line[1] not in memory_label_list):
			memory_label_list.append(split_parameter_line[1])

	############################################################################
	### Find Parameters/Statistics for each memory Name
	############################################################################

	#####################################
	## PARAMETERS
	#####################################
    List_memory_burst_length = []
    List_memory_line_size = []

	#####################################
	## STATISTICS
	#####################################
    List_global_cycle = []
    List_reset_cycle = []

    List_memory_stat_accesses = []

    List_memory_stat_sum_read = []
    List_memory_stat_sum_write = []

	## MVX
    List_memory_stat_hive_load_completed = []
    List_memory_stat_hive_store_completed = []


    memoryNum = -1
    for memory_label in memory_label_list:
		memoryNum += 1

		#####################################
		## PARAMETERS
		#####################################
		List_memory_burst_length.append(0)
		List_memory_line_size.append(0)

		########################################################################
		# FIND CONFIGURATION PARAMETERS
		########################################################################
		app_file.seek(0,0)
		for parameter_line in app_file:
			parameter_line = parameter_line.rstrip('\r\n')
			#Comments
			if parameter_line[0] == '#':
				continue
			split_parameter_line = re.split('\.|:', parameter_line)
			## Find memory Parameters
			# ~ if (split_parameter_line[0] == "CACHE_MEMORY") and (split_parameter_line[1] == memory_label):
			if (split_parameter_line[1] == memory_label):
				if (split_parameter_line[2] == "burst_length") :
					List_memory_burst_length[memoryNum] = int(split_parameter_line[3])
				elif (split_parameter_line[2] == "line_size") :
					List_memory_line_size[memoryNum] = int(split_parameter_line[3])

		#####################################
		## STATISTICS
		#####################################
		List_global_cycle.append(0)
		List_reset_cycle.append(0)

		List_memory_stat_accesses.append(0)

		List_memory_stat_sum_read.append(0)
		List_memory_stat_sum_write.append(0)

		## MVX
		List_memory_stat_hive_load_completed.append(0)
		List_memory_stat_hive_store_completed.append(0)

		########################################################################
		# STATISTICS RESULT
		########################################################################

		app_file.seek(0,0)
		for parameter_line in app_file:
			parameter_line = parameter_line.rstrip('\r\n')
			# Comments
			if parameter_line[0] == '#':
				continue
			split_parameter_line = re.split('\.|:', parameter_line)

			## Global statistics
			if (split_parameter_line[1] == "SINUCA_ENGINE"):
				if (split_parameter_line[2] == "global_cycle") :
					List_global_cycle[memoryNum] = int(split_parameter_line[3])
				elif (split_parameter_line[2] == "reset_cycle") :
					List_reset_cycle[memoryNum] = int(split_parameter_line[3])

			## memory statistics
			elif (split_parameter_line[1] == memory_label):
				if (split_parameter_line[2] == "stat_accesses") :
					List_memory_stat_accesses[memoryNum] = int(split_parameter_line[3])

				elif (split_parameter_line[2] == "stat_sum_read") :
					List_memory_stat_sum_read[memoryNum] = int(split_parameter_line[3])
				elif (split_parameter_line[2] == "stat_sum_write") :
					List_memory_stat_sum_write[memoryNum] = int(split_parameter_line[3])

				## MVX
				elif (split_parameter_line[2] == "stat_hive_load_completed") :
					List_memory_stat_hive_load_completed[memoryNum] = int(split_parameter_line[3])
				elif (split_parameter_line[2] == "stat_hive_store_completed") :
					List_memory_stat_hive_store_completed[memoryNum] = int(split_parameter_line[3])


	############################################################################
	## All Parameters and Statistics found !
    memoryNum = -1
    for memory_label in memory_label_list:
		memoryNum += 1

		PRINT("#" + str(memoryNum))
		PRINT(" memory_label: " + memory_label_list[memoryNum])
		PRINT(" memory_hierarchy_level: " + str(List_memory_burst_length[memoryNum]))
		PRINT(" memory_line_size: " + str(List_memory_line_size[memoryNum]))

		PRINT(" global_cycle: " + str(List_global_cycle[memoryNum]))
		PRINT(" reset_cycle: " + str(List_reset_cycle[memoryNum]))
		PRINT(" memory_stat_accesses: " + str(List_memory_stat_accesses[memoryNum]))

		PRINT(" memory_stat_sum_read: " + str(List_memory_stat_sum_read[memoryNum]))
		PRINT(" memory_stat_sum_write: " + str(List_memory_stat_sum_write[memoryNum]))

		## MVX
		PRINT(" memory_stat_hive_load_completed: " + str(List_memory_stat_hive_load_completed[memoryNum]))
		PRINT(" memory_stat_hive_store_completed: " + str(List_memory_stat_hive_store_completed[memoryNum]))

	############################################################################
	### Create all the CACTI Models
    memory_static_power = []
    memory_read_dynamic_energy = []
    memory_write_dynamic_energy = []
    memory_area = []

    memoryNum = -1
    for memory_label in memory_label_list:
		memoryNum += 1

		# Maintain the Energy for the total cycles
		static_power = 0.0
		# Maintain the Energy for the total access
		read_dynamic_energy = 0.0
		write_dynamic_energy = 0.0
		
		memory_static_power.append(0.0)
		memory_read_dynamic_energy.append(0.0)
		memory_write_dynamic_energy.append(0.0)
		memory_area.append(0)

		############################
		# Simulation Parameters
		############################
		# Simulation Parameters
		component_name = "DRAM"
		memory_level = 3
		memory_line_size = List_memory_line_size[memoryNum]
		# if (func_arg_nvm_exist):
			# memory_size = (4*1024*1024*1024)/8 ## 512MB
		# else:
			# memory_size = (8*1024*1024*1024)/8 ## 1GB	
		memory_size = func_arg_mem_size_mb * (1024*1024) # by bytes
		memory_associativity = 1
		burst_length = List_memory_burst_length[memoryNum]
		############################
		# User definitions
		MILI_TO_NANO = 1000000 # 1 mili = 1,000,000 nano

		memory_bank = 1
		# ~ memory_integration_technology = 0.065 # 65nm
		# memory_integration_technology = 0.045 # 45nm
		memory_integration_technology = 0.032 # 32nm

		# compute the tag size considering and 64 bits address and a sectored memory
		memory_tag_size = "default"

		memory_model = "main memory"
		memory_type = "UCA"

        # following parameter can have one of five values -- (itrs-hp, itrs-lstp, itrs-lop, lp-dram, comm-dram)

		data_cell = "comm-dram"
		data_array = "itrs-hp"
		tag_cell = "itrs-hp"
		tag_array = "itrs-hp"

		memory_out = List_memory_burst_length[memoryNum] * 8
		access_type = "normal"
		rw_port = 1
		read_port = 0
		write_port = 0

		########################################################################
		## Compute the Normal memory Energy
		########################################################################
		result = DynEnergy_StatPower(component_name,\
							memory_level, memory_size, memory_line_size, memory_associativity, memory_out,\
							memory_integration_technology, memory_bank, memory_model,\
							memory_tag_size, memory_type, access_type,\
							data_cell, data_array, tag_cell, tag_array, rw_port,\
							read_port, write_port, 8192, burst_length, burst_length)
		static_power = result.get('stat')
		read_dynamic_energy = result.get('read_dyn')
		write_dynamic_energy = result.get('write_dyn')
		area = result.get('area_mm2')
		
		memory_static_power[memoryNum] = (MILI_TO_NANO * static_power)
		memory_read_dynamic_energy[memoryNum] = read_dynamic_energy
		memory_write_dynamic_energy[memoryNum] = write_dynamic_energy
		memory_area[memoryNum] = area
		########################################################################
		### After get all Simulation and CACTI information
		########################################################################
		# PRINT("=================================")
		# PRINT(func_arg_app_file_name)
		# PRINT("#" + str(memoryNum))
		# PRINT("Cycle: " + str(memory_static_power[memoryNum]))
		# PRINT("Reset_Cycle: " + str(memory_read_dynamic_energy[memoryNum]))

    app_file.close()

	############################################################################
	### Write the output file
	############################################################################
	## Create the new cacti output file
    out_cacti_filename = WORKING_DIR + func_arg_app_file_name + ".cacti_dram"
    output_results_file = open(out_cacti_filename, 'w')

	## Output file header
    output_results_file.write("\n")

    memoryNum = -1
    for memory_label in memory_label_list:
		memoryNum += 1

		output_results_file.write(memory_label_list[memoryNum] + ".Cycles:"      + str(List_global_cycle[memoryNum] - List_reset_cycle[memoryNum]) + "\n")
		output_results_file.write(memory_label_list[memoryNum] + ".MemAccesses:" + str(List_memory_stat_accesses[memoryNum]) + "\n")
		output_results_file.write(memory_label_list[memoryNum] + ".MemRead:"     + str(List_memory_stat_sum_read[memoryNum]) + "\n")
		output_results_file.write(memory_label_list[memoryNum] + ".MemWrite:"    + str(List_memory_stat_sum_write[memoryNum]) + "\n")
		output_results_file.write(memory_label_list[memoryNum] + ".MVXLoad:"     + str(List_memory_stat_hive_load_completed[memoryNum]) + "\n")
		output_results_file.write(memory_label_list[memoryNum] + ".MVX_Store:"   + str(List_memory_stat_hive_store_completed[memoryNum]) + "\n")
		output_results_file.write(memory_label_list[memoryNum] + ".Stat(nW):"    + str(memory_static_power[memoryNum]) + "\n")
		output_results_file.write(memory_label_list[memoryNum] + ".ReadDyn(nJ):"     + str(memory_read_dynamic_energy[memoryNum]) + "\n")
		output_results_file.write(memory_label_list[memoryNum] + ".WriteDyn(nJ):"     + str(memory_write_dynamic_energy[memoryNum]) + "\n")
		output_results_file.write("\n")
    output_results_file.close()

    #PRINT("===================================================================")

    return {'area_mm2': area,\
			 'stat_pow_nW': memory_static_power,\
			 'read_dyn_energy_nJ': memory_read_dynamic_energy,\
			 'write_dyn_energy_nJ': memory_write_dynamic_energy}
	
############################################################################
### Mcpat for processor
# 8. Mcpat for processors and L1, L2 caches
############################################################################
def mcpatProcessor(func_arg_app_file_name, func_arg_mcpat_template):

    PRINT_GOOD("============================= McPAT modeling ===========================")
    PRINT("===================================================================")
    PRINT("Install McPAT inside: " + McPAT_HOME)
    PRINT("===================================================================")


    ## Create the new mcpat cfg file
    mcpat_file_name = WORKING_DIR + func_arg_app_file_name + ".xml"
    mcpat_file = open(mcpat_file_name, 'w')

    try:
        configure_file = open(func_arg_mcpat_template, 'r')
    except IOError:
        PRINT_WARN("\t Configure File Not Found: " + func_arg_mcpat_template + " ... skipping")
        exit()

    for cfg_line in configure_file:
        if (cfg_line.find('$') == -1):
            # Only copy the cfg into the output.
            mcpat_file.write(cfg_line)
            continue


        cfg = cfg_line.split('$')
        if len(cfg) != 3:
            PRINT_ERR("Wrong format inside File: " + func_arg_mcpat_template + "\n Line: " + cfg_line)
            configure_file.close()
            exit()

        app_file_name = WORKING_DIR + func_arg_app_file_name + ".sinuca"
        try:
            app_file = open(app_file_name, 'r')
        except IOError:
            # ~ PRINT_WARN("\t Application Not Found: " + app_name)
            PRINT_WARN("\t Application File Not Found: " + app_file_name + " ... skipping")
            mcpat_file.write(cfg[0])
            mcpat_file.write(cfg[1])
            mcpat_file.write(cfg[2])
            break

        ## Now we need to find the parameter to substitute here.
        app_file.seek(0, 0)
        found_parameter = 0
        for parameter_line in app_file:
            parameter_line = parameter_line.rstrip('\r\n')
            #Comments
            if parameter_line[0] == '#':
                continue

            split_parameter_line = parameter_line.split(':')

            # ~ if (str.find(split_parameter_line[0].lower(), parameter_name.lower()) != -1):
            ## Now we are taking only the exact match to avoid problems
            if (split_parameter_line[0].lower() == cfg[1].lower()):
                found_parameter = 1
                cfg[1] = split_parameter_line[1]
                break;
        if (found_parameter == 0):
            found_error = 1
            PRINT_ERR("\t\t Parameter Not Found: " + cfg[1] + " ... skipping")
            # ~ exit()

        # ~ PRINT("Found: "+cfg[1])
        mcpat_file.write(cfg[0])
        mcpat_file.write(cfg[1])
        mcpat_file.write(cfg[2])

    configure_file.close()
    mcpat_file.close()

    out_mcpat_filename = WORKING_DIR + func_arg_app_file_name + ".mcpat"
    #PRINT_GOOD(McPAT_HOME + "mcpat -infile " + mcpat_file_name + " -print_level 5")
    status = 0
    try: 
		mcpat_old = os.popen(McPAT_HOME + "mcpat -infile " + mcpat_file_name + " -print_level 5", "r")
    except IOError:
        status = 1
    mcpat_new = open(out_mcpat_filename, "w")
    #mcpat_new2 = open(out_mcpat_filename + "_2", "w")

    title = ""
    for line in mcpat_old:
        #mcpat_new2.write(line)
        line = line.rstrip('\r\n')
        line = line.strip()
        if line.find(':') != -1:
            if line.find('McPAT') == -1:
                title = line
        else:
            line = title + line + "\n"
            line = re.sub('[ \t]+', ' ', line)
            line = re.sub('\ \ +', ' ', line)
            mcpat_new.write(line)

    mcpat_old.close()
    mcpat_new.close()

    return status
############################################################################
### Get SiNUCA results to input mcpat and cacti
# 9. SiNUCA results
############################################################################
def findSiNUCAResults(func_arg_app_file_name):

	PRINT("===================================================================")
	PRINT("SiNUCA result for " + func_arg_app_file_name)
	PRINT("===================================================================")


	sinuca_file_name = WORKING_DIR + func_arg_app_file_name + ".sinuca"
	try:
		sinuca_file = open(sinuca_file_name, 'r')
	except IOError:
		PRINT_WARN("\t SiNUCA File Not Found: " + sinuca_file_name + " ... skipping")
		exit()
	############################################################################
	### Find Memory Names
	############################################################################
	memory_label_list = []
	proc_label_list = []
	configurations_inside_file = 0
	# Iterates over the application results FILE
	sinuca_file.seek(0,0)
	for parameter_line in sinuca_file:
		parameter_line = parameter_line.rstrip('\r\n')
		#Comments
		if (parameter_line == "#Configuration of SINUCA_ENGINE"):
			configurations_inside_file += 1
			if (configurations_inside_file > 1):
				PRINT_ERR("Multiple results for application\n ")
				exit()

		if parameter_line[0] == '#':
			continue

		split_parameter_line = parameter_line.split('.')
		#PRINT_GOOD(split_parameter_line[0]+"\n")
		#PRINT_GOOD(split_parameter_line[1]+"\n")
		if (split_parameter_line[0] == "MEMORY_CONTROLLER") and (split_parameter_line[1] not in memory_label_list):
			memory_label_list.append(split_parameter_line[1])
			#PRINT(split_parameter_line[1]+"\n")
		if (split_parameter_line[0] == "PROCESSOR") and (split_parameter_line[1] not in proc_label_list):
			proc_label_list.append(split_parameter_line[1])
			#PRINT_GOOD(split_parameter_line[1]+"\n")
	############################################################################
	### Find Parameters/Statistics for each memory Name
	############################################################################

	#####################################
	## PARAMETERS
	#####################################
	List_memory_burst_length = []
	List_memory_line_size = []

	#####################################
	## STATISTICS
	#####################################
	List_global_cycle = []
	List_reset_cycle = []

	List_memory_stat_accesses = []

	List_memory_stat_sum_read = []
	List_memory_stat_sum_write = []

	## MVX
	List_memory_stat_hive_load_completed = []
	List_memory_stat_hive_store_completed = []
	List_memory_stat_hive_lock_completed = []

	List_memory_stat_cim_btw_or_completed = []
	List_memory_stat_cim_btw_and_completed = []
	List_memory_stat_cim_btw_xor_completed = []
	List_memory_stat_cim_eq_cmp_completed = []
	List_memory_stat_cim_adr_ld_completed = []
	List_memory_stat_cim_mem_ld_completed = []
	List_memory_stat_cim_mem_st_completed = []	

	memoryNum = -1
	for memory_label in memory_label_list:
		memoryNum += 1

		#####################################
		## PARAMETERS
		#####################################
		List_memory_burst_length.append(0)
		List_memory_line_size.append(0)

		########################################################################
		# FIND CONFIGURATION PARAMETERS
		########################################################################
		sinuca_file.seek(0,0)
		for parameter_line in sinuca_file:
			parameter_line = parameter_line.rstrip('\r\n')
			#Comments
			if parameter_line[0] == '#':
				continue
			split_parameter_line = re.split('\.|:', parameter_line)
			## Find memory Parameters
			# ~ if (split_parameter_line[0] == "CACHE_MEMORY") and (split_parameter_line[1] == memory_label):
			if (split_parameter_line[1] == memory_label):
				if (split_parameter_line[2] == "burst_length") :
					List_memory_burst_length[memoryNum] = int(split_parameter_line[3])
				elif (split_parameter_line[2] == "line_size") :
					List_memory_line_size[memoryNum] = int(split_parameter_line[3])

		#####################################
		## STATISTICS
		#####################################
		
        List_global_cycle.append(0)
        List_reset_cycle.append(0)

        List_memory_stat_accesses.append(0)

        List_memory_stat_sum_read.append(0)
        List_memory_stat_sum_write.append(0)
		## MVX
        List_memory_stat_hive_load_completed.append(0)
        List_memory_stat_hive_store_completed.append(0)
        List_memory_stat_hive_lock_completed.append(0)

        List_memory_stat_cim_btw_or_completed.append(0)
        List_memory_stat_cim_btw_and_completed.append(0)
        List_memory_stat_cim_btw_xor_completed.append(0)
        List_memory_stat_cim_eq_cmp_completed.append(0)
        List_memory_stat_cim_adr_ld_completed.append(0)
        List_memory_stat_cim_mem_ld_completed.append(0)
        List_memory_stat_cim_mem_st_completed.append(0)
		########################################################################
		# STATISTICS RESULT
		########################################################################
		
        sinuca_file.seek(0,0)
        for parameter_line in sinuca_file:
            parameter_line = parameter_line.rstrip('\r\n')
			# Comments
            if parameter_line[0] == '#':
                continue
            split_parameter_line = re.split('\.|:', parameter_line)

			## Global statistics
            if (split_parameter_line[1] == "SINUCA_ENGINE"):
                if (split_parameter_line[2] == "global_cycle") :
                    List_global_cycle[memoryNum] = int(split_parameter_line[3])
                elif (split_parameter_line[2] == "reset_cycle") :
                    List_reset_cycle[memoryNum] = int(split_parameter_line[3])
									
			## memory statistics
            elif (split_parameter_line[1] == memory_label):
                if (split_parameter_line[2] == "stat_accesses") :
                    List_memory_stat_accesses[memoryNum] = int(split_parameter_line[3])

                elif (split_parameter_line[2] == "stat_sum_read") :
                    List_memory_stat_sum_read[memoryNum] = int(split_parameter_line[3])
                elif (split_parameter_line[2] == "stat_sum_write") :
                    List_memory_stat_sum_write[memoryNum] = int(split_parameter_line[3])

				## MVX
                elif (split_parameter_line[2] == "stat_hive_lock_completed") :
                    List_memory_stat_hive_lock_completed[memoryNum] = int(split_parameter_line[3])
                elif (split_parameter_line[2] == "stat_hive_load_completed") :
                    List_memory_stat_hive_load_completed[memoryNum] = int(split_parameter_line[3])
                elif (split_parameter_line[2] == "stat_hive_store_completed") :
                    List_memory_stat_hive_store_completed[memoryNum] = int(split_parameter_line[3])

				## CIMA
                elif (split_parameter_line[2] == "stat_cim_btw_or_completed") :
                    List_memory_stat_cim_btw_or_completed[memoryNum] = int(split_parameter_line[3])
                elif (split_parameter_line[2] == "stat_cim_btw_and_completed") :
                    List_memory_stat_cim_btw_and_completed[memoryNum] = int(split_parameter_line[3])
                elif (split_parameter_line[2] == "stat_cim_btw_xor_completed") :
                    List_memory_stat_cim_btw_xor_completed[memoryNum] = int(split_parameter_line[3])
                elif (split_parameter_line[2] == "stat_cim_eq_cmp_completed") :
                    List_memory_stat_cim_eq_cmp_completed[memoryNum] = int(split_parameter_line[3])
                elif (split_parameter_line[2] == "stat_cim_adr_ld_completed") :
                    List_memory_stat_cim_adr_ld_completed[memoryNum] = int(split_parameter_line[3])
                elif (split_parameter_line[2] == "stat_cim_mem_ld_completed") :
                    List_memory_stat_cim_mem_ld_completed[memoryNum] = int(split_parameter_line[3])
                elif (split_parameter_line[2] == "stat_cim_mem_st_completed") :
                    List_memory_stat_cim_mem_st_completed[memoryNum] = int(split_parameter_line[3])

	List_proc_active_cycle = []
	List_proc_idle_cycle = []
	procNum = -1
	for proc_label in proc_label_list:
		procNum += 1
		List_proc_active_cycle.append(0)
		List_proc_idle_cycle.append(0)
		sinuca_file.seek(0,0)
		for parameter_line in sinuca_file:
			parameter_line = parameter_line.rstrip('\r\n')
			# Comments
			if parameter_line[0] == '#':
				continue
			split_parameter_line = re.split('\.|:', parameter_line)

			## Statistics
			if (split_parameter_line[1] == proc_label) :
				if(split_parameter_line[2] == "stat_idle_cycles") :
					List_proc_idle_cycle[procNum] = int(split_parameter_line[3])
				elif (split_parameter_line[2] == "stat_active_cycles") :
					List_proc_active_cycle[procNum] = int(split_parameter_line[3])
					
	############################################################################
	## All Parameters and Statistics found !
	memoryNum = -1
	for memory_label in memory_label_list:
		memoryNum += 1
		PRINT("NVM")
		PRINT("#" + str(memoryNum))
		PRINT(" memory_label: " + memory_label_list[memoryNum])
		# ## CIMA
		PRINT(" memory_stat_cim_btw_or_completed: " + str(List_memory_stat_cim_btw_or_completed[memoryNum]))
		PRINT(" memory_stat_cim_btw_and_completed: " + str(List_memory_stat_cim_btw_and_completed[memoryNum]))
		PRINT(" memory_stat_cim_btw_xor_completed: " + str(List_memory_stat_cim_btw_xor_completed[memoryNum]))
		PRINT(" memory_stat_cim_eq_cmp_completed: " + str(List_memory_stat_cim_eq_cmp_completed[memoryNum]))
		PRINT(" memory_stat_cim_adr_ld_completed: " + str(List_memory_stat_cim_adr_ld_completed[memoryNum]))
		PRINT(" memory_stat_cim_mem_ld_completed: " + str(List_memory_stat_cim_mem_ld_completed[memoryNum]))
		PRINT(" memory_stat_cim_mem_st_completed: " + str(List_memory_stat_cim_mem_st_completed[memoryNum]))
		# PRINT(" memory_hierarchy_level: " + str(List_memory_burst_length[memoryNum]))
		# PRINT(" memory_line_size: " + str(List_memory_line_size[memoryNum]))

		# PRINT(" global_cycle: " + str(List_global_cycle[memoryNum]))
		# PRINT(" reset_cycle: " + str(List_reset_cycle[memoryNum]))
		# PRINT(" cpu0_active_cycle: " + str(CPU0_active_cycle))
		# PRINT(" cpu0_idle_cycle: " + str(CPU0_idle_cycle))
		# PRINT(" memory_stat_accesses: " + str(List_memory_stat_accesses[memoryNum]))

		# PRINT(" memory_stat_sum_read: " + str(List_memory_stat_sum_read[memoryNum]))
		# PRINT(" memory_stat_sum_write: " + str(List_memory_stat_sum_write[memoryNum]))
		
		# ## MVX
		# PRINT(" memory_stat_hive_lock_completed: " + str(List_memory_stat_hive_lock_completed[memoryNum]))
		# PRINT(" memory_stat_hive_unlock_completed: " + str(List_memory_stat_hive_lock_completed[memoryNum]))
		# PRINT(" memory_stat_hive_load_completed: " + str(List_memory_stat_hive_load_completed[memoryNum]))
		# PRINT(" memory_stat_hive_store_completed: " + str(List_memory_stat_hive_store_completed[memoryNum]))
		


	sinuca_file.close()	
	return {'memory_num':memoryNum,\
			'global_cycle':List_global_cycle, \
			'reset_cycle':List_reset_cycle,\
			'active_cyle':List_proc_active_cycle,\
			'idle_cycle':List_proc_idle_cycle,\
			'mem_access':List_memory_stat_accesses,\
			'mem_read':List_memory_stat_sum_read,\
			'mem_write':List_memory_stat_sum_write,\
			'hive_lock':List_memory_stat_hive_lock_completed,\
			'hive_unlock':List_memory_stat_hive_lock_completed,\
			'hive_load':List_memory_stat_hive_load_completed,\
			'hive_store':List_memory_stat_hive_store_completed,\
			'cim_btw_or':List_memory_stat_cim_btw_or_completed,\
			'cim_btw_and':List_memory_stat_cim_btw_and_completed,\
			'cim_btw_xor':List_memory_stat_cim_btw_xor_completed,\
			'cim_eq_cmp':List_memory_stat_cim_eq_cmp_completed,\
			'cim_adr_ld':List_memory_stat_cim_adr_ld_completed,\
			'cim_mem_ld':List_memory_stat_cim_mem_ld_completed,\
			'cim_mem_st':List_memory_stat_cim_mem_st_completed}

############################################################################
### Get McPAT results to input estimation model
# 10. McPAT results
############################################################################
def findMcPATResults(func_arg_app_file_name):

	PRINT("===================================================================")
	PRINT("McPAT result for " + func_arg_app_file_name)
	PRINT("===================================================================")


	mcpat_file_name = WORKING_DIR + func_arg_app_file_name + ".mcpat"
	#PRINT("Trying to open McPAT file name:" + mcpat_file_name + "\n")	
	try:
		mcpat_file = open(mcpat_file_name, 'r')
	except IOError:
		PRINT_WARN("\t McPAT File Not Found: " + mcpat_file_name + " ... skipping")
		exit()
	############################################################################
	### Find Values for Some Predefined Parameters
	############################################################################
	# Iterates over the application results FILE
	param_list = ["Core clock Rate(MHz)", "Processor:Area", "Processor:Total Leakage", "Processor:Runtime Dynamic"]
	value_list = []
	#count = 0;
	for param in param_list:
		mcpat_file.seek(0,0)
		#PRINT(param)
		for line in mcpat_file:
			if param in line:
				if (param == "Core clock Rate(MHz)"):
					value = re.findall(r'[\d]+',line)
				else:
					value = re.findall(r'[\d]+\.[\d]+',line)
				value_list.append(value)
				#PRINT("value: " + str(value))
				#count = count + 1;
				break
	mcpat_file.close()	
	#print(value_list)
	return {'clock_rate_mhz': value_list[0],\
			'proc_area_mm2': value_list[1],\
			'proc_leak_pow': value_list[2],\
			'proc_dyn_pow': value_list[3]}

############################################################################
### Get NVSIM results to input estimation model
# 11. NVSIM results
############################################################################
def findNVSIMResults(func_arg_app_file_name):

	PRINT("===================================================================")
	PRINT("NVSIM result for " + func_arg_app_file_name)
	PRINT("===================================================================")


	nvsim_file_name = WORKING_DIR + func_arg_app_file_name + ".nvsim"
	#PRINT("Trying to open NVSIM file name:" + nvsim_file_name + "\n")	
	try:
		nvsim_file = open(nvsim_file_name, 'r')
	except IOError:
		PRINT_WARN("\t NVSIM File Not Found: " + nvsim_file_name + " ... skipping")
		exit()
	############################################################################
	### Find Values for Some Predefined Parameters
	############################################################################
	# Iterates over the application results FILE
	param_int_list = ["Capacity", "Data Width", "Bank Organization"]
	param_float_list = ["Total Area", "Area Efficiency", "Read Latency", "RESET Latency", "Read Dynamic Energy", "RESET Dynamic Energy"]
	param_list = param_int_list + param_float_list
	value_list = []
	#count = 0;
	#PRINT(str(param_list))
	for param in param_list:
		nvsim_file.seek(0,0)
		#PRINT(param)
		for line in nvsim_file:
			if param in line:
				if param in param_int_list:
					value = re.findall(r'[\d]+',line)
				else:
					value = re.findall(r'[\d]+\.[\d]+',line)
				value_list.append(value)
				#PRINT("value: " + str(value))
				#count = count + 1;
				break
	nvsim_file.close()	
	#print(value_list)
	return { 'nvm_capacity_gb' 		: value_list[0],\
			 'nvm_data_width_x_x' 	: value_list[1],\
			 'nvm_bank_org_x_x' 	: value_list[2],\
			 'nvm_area_x_x_x'		: value_list[3],\
			 'nvm_area_efficiency'	: value_list[4],\
			 'nvm_read_latency_ns' 	: value_list[5],\
			 'nvm_write_latency_ns' 	: value_list[6],\
			 'nvm_read_power_nJ' 		: value_list[7],\
			 'nvm_write_power_nJ' 		: value_list[8]}
			
############################################################################
### Calculate latency, energy and area
# 12. Estimation model
############################################################################
def resultEstimation(func_arg_app_file_name, func_arg_result_file_name, func_arg_total_mem_size_mb, func_arg_nvm_mem_size_mb):
	# Reopen result file to write in results
    result_file = open(func_arg_result_file_name, 'a')	
    if ("cima" in func_arg_app_file_name):
        mem_size_dram_mb = func_arg_total_mem_size_mb - func_arg_nvm_mem_size_mb
    else:
        mem_size_dram_mb = func_arg_total_mem_size_mb
        func_arg_nvm_mem_size_mb = 0

	# Execute Cacti model and process Cacti result
    cacti_res = cactiDRAM(func_arg_app_file_name, mem_size_dram_mb) 
    dram_area_mm2 = cacti_res.get('area_mm2')
    dram_stat_nW_tup = cacti_res.get('stat_pow_nW')
    dram_stat_nW = dram_stat_nW_tup[0]
    dram_read_dyn_nJ_tup = cacti_res.get('read_dyn_energy_nJ')
    dram_read_dyn_nJ = dram_read_dyn_nJ_tup[0]
    dram_write_dyn_nJ_tup = cacti_res.get('write_dyn_energy_nJ')
    dram_write_dyn_nJ = dram_write_dyn_nJ_tup[0]

    PRINT_GOOD("============================= Results Processing ===========================")
	
    # Read SiNUCA result  
    sinuca_res = findSiNUCAResults(func_arg_app_file_name)	
    global_cycle_tup = sinuca_res.get('global_cycle')
    global_cycle = global_cycle_tup[0]
    mem_read_tup = sinuca_res.get('mem_read')
    mem_read = mem_read_tup[0]
    mem_write_tup = sinuca_res.get('mem_write')
    mem_write = mem_write_tup[0]
    cim_read = sinuca_res.get('cim_btw_or') + sinuca_res.get('cim_btw_and') + sinuca_res.get('cim_btw_xor') + sinuca_res.get('cim_mem_ld')
    nvm_read = cim_read[0]
    cim_cmos = sinuca_res.get('cim_eq_cmp')
    nvm_cmos = cim_cmos[0]
    cim_write = sinuca_res.get('cim_mem_st')
    nvm_write = cim_write[0]
    proc_idle_cc_tup = sinuca_res.get('idle_cycle')
    #proc_idle_cc = proc_idle_cc_tup[0]
    proc_active_cc_tup = sinuca_res.get('active_cyle')
    proc_active_cc = proc_active_cc_tup[0]

    for proc in range (0, len(proc_active_cc_tup)):
		if (proc_active_cc_tup[proc] > proc_active_cc):
			proc_active_cc = proc_active_cc_tup[proc]
			
	# Read McPAT result
    mcpat_res = findMcPATResults(func_arg_app_file_name)
    clock_rate_mhz_tup = mcpat_res.get('clock_rate_mhz')
    clock_rate_mhz = clock_rate_mhz_tup[0]
    proc_area_mm2_tup = mcpat_res.get('proc_area_mm2')
    proc_area_mm2 = proc_area_mm2_tup[0]
    proc_leak_pow_tup = mcpat_res.get('proc_leak_pow')
    proc_leak_pow = proc_leak_pow_tup[0]
    proc_dyn_pow_tup = mcpat_res.get('proc_dyn_pow')
    proc_dyn_pow = proc_dyn_pow_tup[0]
	
	# Read NVSIM results	
    nvsim_res = findNVSIMResults(func_arg_app_file_name)
    nvm_area_tup = nvsim_res.get('nvm_area_x_x_x')
    if ("cima" in func_arg_app_file_name):
        nvm_area_mm2 = nvm_area_tup[2]
    else:
        nvm_area_mm2 = 0
    nvm_read_power_tup = nvsim_res.get('nvm_read_power_nJ')
    nvm_read_power_nJ = nvm_read_power_tup[0]
    nvm_write_power_tup = nvsim_res.get('nvm_write_power_nJ')
    nvm_write_power_nJ = nvm_write_power_tup[0]

    PRINT("===================================================================")
    PRINT("Final report for " + func_arg_app_file_name)
    PRINT("===================================================================")

	# Calculate latency
    total_latency_ns = to_nano(float(global_cycle) / (from_mega(float(clock_rate_mhz))))
    PRINT("total_latency_ns: " + str(total_latency_ns))
    
	
	# Calculate total energy
	# Processor
    procNum = 0
    proc_stat_energy_nJ = 0
    proc_dyn_energy_nJ = 0
    for proc in proc_active_cc_tup:
		proc_stat_energy_nJ += to_nano(float(proc_leak_pow) * int(proc_idle_cc_tup[procNum]) / from_mega(float(clock_rate_mhz)))
		proc_dyn_energy_nJ += to_nano(float(proc_dyn_pow) * int(proc_active_cc_tup[procNum]) / from_mega(float(clock_rate_mhz)))
		procNum += 1
    proc_energy_nJ = proc_stat_energy_nJ + proc_dyn_energy_nJ
	# DRAM
    dram_stat_energy_nJ = float(dram_stat_nW) * float(global_cycle) / from_mega(float(clock_rate_mhz))
	
    dram_dyn_energy_nJ = int(mem_read) * float(dram_read_dyn_nJ) + int(mem_write) * float(dram_write_dyn_nJ)
    dram_energy_nJ = dram_stat_energy_nJ + dram_dyn_energy_nJ
	
	# NVM
    nvm_stat_energy_nJ = float(0);
	# Assume operation related to CMOS has the same cost as read operations --- as the cost is mostly inside the read operations, cmos is very energy efficient
    nvm_dyn_energy_nJ = (int(nvm_read) + int(nvm_cmos)) * float(nvm_read_power_nJ) + int(nvm_write) * float(nvm_write_power_nJ)
    nvm_energy_nJ = nvm_stat_energy_nJ + nvm_dyn_energy_nJ

    total_energy_nJ = dram_energy_nJ + nvm_energy_nJ + proc_energy_nJ
    PRINT("total_energy_nJ: " + str(total_energy_nJ))
    
	
	# Calculate total area
    total_area_mm2 = float(proc_area_mm2) + float(dram_area_mm2) + float(nvm_area_mm2)
    PRINT("proc_area_mm2: " + str(proc_area_mm2))
    PRINT("dram_area_mm2: " + str(dram_area_mm2))
    PRINT("nvm_area_mm2: " + str(nvm_area_mm2))
    PRINT("total_area_mm2: " + str(total_area_mm2))
    
	# Print results

	# Application size
	# Memory size
		# Cache L1
		# Cache L2
		# Cache L3
		# NVM
		# DRAM
	# Memory size
    result_file.write(func_arg_app_file_name + "_dram_size = " + str(mem_size_dram_mb) + " (MB)\n")
    result_file.write(func_arg_app_file_name + "_nvm_size = " + str(func_arg_nvm_mem_size_mb) + " (MB)\n")	
	# Latency
    result_file.write(func_arg_app_file_name + "_latency = " + str(total_latency_ns) + " (ns)\n")
	# Energy
    result_file.write(func_arg_app_file_name + "_energy = " + str(total_energy_nJ) + " (nJ)\n")	
	# Area
    result_file.write(func_arg_app_file_name + "_area = " + str(total_area_mm2) + " (mm2)\n")
	
    result_file.close()
	
    PRINT("===================================================================\n")

    return {'latency_ns':total_latency_ns,\
			'energy_nJ': total_energy_nJ,\
			'area_mm2':total_area_mm2}
			
################################################################################
# def print_to_excel_file(func_arg_worksheet, func_arg_app_file_name, func_arg_row, func_arg_col, total_latency_ns, total_energy_nJ, total_area_mm2):
	# # Print results

	# # Application size
	# # Memory size
		# # Cache L1
		# # Cache L2
		# # Cache L3
		# # NVM
		# # DRAM
	# result_mat = (	[func_arg_app_file_name + "_latency", "ns", total_latency_ns],
					# [func_arg_app_file_name + "_energy", "nJ", total_energy_nJ],
					# [func_arg_app_file_name + "_area", "mm2", total_area_mm2])
	# for param, unit, value in (result_mat):
		# func_arg_worksheet.write(func_arg_row, func_arg_col, param)
		# func_arg_worksheet.write(func_arg_row, func_arg_col+1, unit)
		# func_arg_worksheet.write(func_arg_row, func_arg_col+2, value)
		# func_arg_row += 1
		
	# return { 'row': func_arg_row,\
			 # 'col': func_arg_col}
	
############################################################################
### Execute SiNUCA
# 13. Execute SiNUCA
############################################################################
def sinucaExecution(func_arg_app_file_name, func_arg_sinuca_conf_folder):
	PRINT_GOOD("============================= SiNUCA simulating ===========================")
	PRINT("===================================================================")
	PRINT("Install SiNUCA inside: " + SINUCA_HOME)
	PRINT("===================================================================")

	config_file = SINUCA_HOME + "config_examples/" + func_arg_sinuca_conf_folder + "main.cfg "
	trace_file = WORKING_DIR + func_arg_app_file_name
	result_file = WORKING_DIR + func_arg_app_file_name + ".sinuca"

	# execute SiNUCA
	status = os.system(SINUCA_HOME + "sinuca --config " + config_file + " --trace " + trace_file + " --result " + result_file)
	PRINT(SINUCA_HOME + "sinuca --config " + config_file + " --trace " + trace_file + " --result " + result_file)

	return status
	
############################################################################
### Execute NVSIM
# 14. Execute NVSIM
############################################################################
def nvsimExecution(func_arg_app_file_name, func_arg_nvm_mem_size_mb, func_arg_data_width_cima):
	PRINT_GOOD("============================= NVSIM modeling ===========================")
	nvsim_config = "cima_" + str(func_arg_nvm_mem_size_mb) + "MB_" +  str(int(func_arg_data_width_cima)) + "B"
	config_file = NVSIM_HOME + nvsim_config + ".cfg"
	if os.path.isfile(config_file):
		PRINT_GOOD('Configuration file ready: ' + config_file)
	else:
		sys.exit("Configuration file does not exist" + config_file + "\n")
	
	result_file_name = WORKING_DIR + func_arg_app_file_name + ".nvsim"
	# try:
		# result_file = open(result_file_name, 'r')
        # #print("Using Old Cacti Model: " + TMP_OUT_FILE_NAME)
	# except IOError:
		# print("Creating New NVSIM Result: " + result_file_name)
	
	status = os.system(NVSIM_HOME + "nvsim " + config_file + " > " + result_file_name)
	if (status == 1):
		PRINT_ERR("Error executing NVSIM\n")
		sys.exit()	
	PRINT(NVSIM_HOME + "nvsim " + config_file + " > " + result_file_name)
	
	#result_file.close()
	return status

############################################################################
### Execute for one architecture (calling above functions and checking if there is failure)
# 15. Execute for one architecture (CIMA or CPU)
############################################################################
def singleArchExecution(func_arg_app_file_name, func_arg_mcpat_template, func_arg_result_file_name, func_arg_sinuca_conf_folder, func_arg_data_width_cima):
	# Configure memory size
	total_mem_size_mb = 1024 # MB = 1GB
	nvm_mem_size_mb = 512 # MB 
	# Execute SiNUCA
	status = check_file_del(func_arg_app_file_name, ".sinuca")
	status = sinucaExecution(func_arg_app_file_name, func_arg_sinuca_conf_folder)
	if (status == 0):
		check_file_exist (func_arg_app_file_name, ".sinuca")
	else:
		PRINT_ERR("Error executing SiNUCA\n")
		sys.exit()
		
	# Execute McPAT
	status = mcpatProcessor(func_arg_app_file_name, func_arg_mcpat_template)
	if (status == 0):
		check_file_exist (func_arg_app_file_name, ".mcpat")
	else:
		PRINT_ERR("Error executing McPAT\n")
		sys.exit()
	# to do check file mcpat
	# PRINT_DEBUG("I dont check mcpat, but it could have problem\n")
	# Execute NVSIM
	nvsimExecution(func_arg_app_file_name, nvm_mem_size_mb, func_arg_data_width_cima)
	if (status == 0):
		check_file_exist (func_arg_app_file_name, ".nvsim")
	else:
		PRINT_ERR("Error executing NVSIM\n")
		sys.exit()
	# Execute the whole result
	resultEstimation(func_arg_app_file_name, func_arg_result_file_name, total_mem_size_mb, nvm_mem_size_mb)	
	return
  
################################################################################
## MAIN PROGRAM
# 16. Initialize setting and parameter for further execution
# input: APP_FILE_NAME.sinuca, APP_FILE_NAME.mcpat, APP_FILE_NAME.cacti_dram, APP_FILE_NAME.nvsim
# provided: APP_FILE_NAME.sinuca, APP_FILE_NAME.mcpat, APP_FILE_NAME.cacti_dram, APP_FILE_NAME.nvsim
# output: APP_FILE_NAME.result
################################################################################
setting = set_param (arg_benchmark, arg_architecture, \
					arg_mcpat_template_cpu, arg_mcpat_template_cima, \
					arg_sinuca_conf_folder_cpu, arg_sinuca_conf_folder_cima, \
					arg_problem_size, arg_data_width_cpu, arg_data_width_cima, \
					arg_num_cores_cpu, arg_sync_conf)
APP_FILE_NAME = setting.get('filename')
arg_mcpat_template = setting.get('mcpat')
arg_sinuca_conf_folder = setting.get('sinuca')
thread_opt = setting.get('threads')
arg_loop_size = setting.get('loopsize')
cpp_file_name = setting.get('cpp')
bin_file_name = setting.get('bin')

################################################################################
## GENERATE RESULTS
# 17. Output results to a file --- to be processed in Matlab
# input: APP_FILE_NAME.sinuca, APP_FILE_NAME.mcpat, APP_FILE_NAME.cacti_dram, APP_FILE_NAME.nvsim
# provided: APP_FILE_NAME.sinuca, APP_FILE_NAME.mcpat, APP_FILE_NAME.cacti_dram, APP_FILE_NAME.nvsim
# output: APP_FILE_NAME.result
################################################################################

if (arg_execution == "result"):
	
    result_file_name = WORKING_DIR + APP_FILE_NAME + ".result"

	# this is only for decoration
    result_file = open(result_file_name, 'w')
    result_file.write("####################################################\n")
    result_file.write("Application: " + arg_benchmark + "\n")
    result_file.write("Architecture: " + arg_architecture)
    result_file.write("\n####################################################\n")
    result_file.close()	

    resultEstimation(APP_FILE_NAME, result_file_name, total_mem_size_mb, nvm_mem_size_mb)

################################################################################
## GENERATE TRACES
# 18. Generate traces (if called to generate traces only)
# input: two options:
# ------------------- vecsum: 
# --------------------------- 1 core: trace_generator_vecsum_singlecore.py
# --------------------------- 2 or more cores: trace_generator_vecsum_singlecore.py
# ------------------- applications: c or cpp source code
# provided: trace generator (py scripts or sinuca trace generator)
# output: trace in gz form
################################################################################
elif (arg_execution == "trace"):
	if ("vecsum" in arg_benchmark) or ('tcph' in arg_benchmark):
		if ("vecsum" in arg_benchmark):		
			if (int(arg_num_cores_cpu) == 1):
				arg_trace_gen_pyscript = 'trace_generator_vecsum_singlecore_v4.py'
			else:
				arg_trace_gen_pyscript = 'trace_generator_vecsum_multicores_v4.py'	
		elif ('tcph' in arg_benchmark):
			arg_trace_gen_pyscript = 'trace_generator_tpch_cs_cimaNew_vectorized_unrolled_v4f.py'
		arg_cima_on = 1
		#PRINT_DEBUG(arg_architecture)
		if (arg_architecture == 'cpu'):
			arg_cima_on = 0
		else:
			if (arg_architecture == 'all'):
				setting = set_param (arg_benchmark, 'cima', \
									arg_mcpat_template_cpu, arg_mcpat_template_cima, \
									arg_sinuca_conf_folder_cpu, arg_sinuca_conf_folder_cima, \
									arg_problem_size, arg_data_width_cpu, arg_data_width_cima, \
									arg_num_cores_cpu, arg_sync_conf)
				APP_FILE_NAME = setting.get('filename')
				if (check_trace_file_exist(APP_FILE_NAME) != 0):
					arg_cima_on = 0
				
						
		#PRINT_DEBUG(str(arg_cima_on))
		status = os.system("python " + arg_trace_gen_pyscript + " " + WORKING_DIR + " " + arg_benchmark + " " + arg_problem_size + " " + str(arg_data_width_cpu) + " " + str(arg_data_width_cima) + " " + str(arg_num_cores_cpu) + " " + str(arg_cima_on) + " " + str(arg_sync_conf) )
		if (status != 0):
			PRINT_ERR("Error running python script to generate traces\n")
			sys.exit()			
	elif ("forloop" in arg_benchmark):
		if (arg_architecture != 'all'):
			status = os.system("g++ -o " + WORKING_DIR+bin_file_name + " " + WORKING_DIR+cpp_file_name + " -I " + BOOST_HOME)
			if (status != 0):
				PRINT_ERR("Error running g++\n")
				sys.exit()			
			if (arg_architecture == 'cpu'):
				status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_loop_size) + " " + str(arg_sync_conf))
				PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_loop_size) + " " + str(arg_sync_conf))
				if (status != 0):
					PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arg_architecture + "\n")
					sys.exit()	
			else:
				status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_loop_size) + " " + str(arg_sync_conf))
				PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_loop_size) + " " + str(arg_sync_conf))
				if (status != 0):
					PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arg_architecture + "\n")
					sys.exit()	
			#check_trace_file_exist(APP_FILE_NAME)
		else:
			for arch in ARCHITECURE_LIST:
				if (arch == 'cpu'):
					setting = set_param (arg_benchmark, arch, \
										arg_mcpat_template_cpu, arg_mcpat_template_cima, \
										arg_sinuca_conf_folder_cpu, arg_sinuca_conf_folder_cima, \
										arg_problem_size, arg_data_width_cpu, arg_data_width_cima, \
										arg_num_cores_cpu, arg_sync_conf)
					APP_FILE_NAME = setting.get('filename')
					thread_opt = setting.get('threads')
					arg_loop_size = setting.get('loopsize')
					cpp_file_name = setting.get('cpp')
					bin_file_name = setting.get('bin')

					if (check_trace_file_exist(APP_FILE_NAME) == 0):
						status = os.system("g++ -o " + WORKING_DIR+bin_file_name + " " + WORKING_DIR+cpp_file_name + " -I " + BOOST_HOME)
						if (status != 0):
							PRINT_ERR("Error running g++\n")
							sys.exit()
						status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_loop_size) + " " + str(arg_sync_conf))
						PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_loop_size) + " " + str(arg_sync_conf))
						if (status != 0):
							PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arch + "\n")
							sys.exit()						
					else:
						PRINT_WARN("These old trace files are used\n")
				else:
					setting = set_param (arg_benchmark, arch, \
										arg_mcpat_template_cpu, arg_mcpat_template_cima, \
										arg_sinuca_conf_folder_cpu, arg_sinuca_conf_folder_cima, \
										arg_problem_size, arg_data_width_cpu, arg_data_width_cima, \
										arg_num_cores_cpu, arg_sync_conf)
					APP_FILE_NAME = setting.get('filename')
					thread_opt = setting.get('threads')
					arg_loop_size = setting.get('loopsize')
					cpp_file_name = setting.get('cpp')
					bin_file_name = setting.get('bin')

					if (check_trace_file_exist(APP_FILE_NAME) == 0):
						status = os.system("g++ -o " + WORKING_DIR+bin_file_name + " " + WORKING_DIR+cpp_file_name + " -I " + BOOST_HOME)
						if (status != 0):
							PRINT_ERR("Error running g++\n")
							sys.exit()
						status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_loop_size) + " " + str(arg_sync_conf))
						PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_loop_size) + " " + str(arg_sync_conf))
						if (status != 0):
							PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arch + "\n")
							sys.exit()	
					else:
						PRINT_WARN("These old trace files are used\n")
	elif ("simple_otp" in arg_benchmark):
		if (arg_architecture != 'all'):
			status = os.system("g++ -Wall -Wextra -std=c++11 -O2 -o " + WORKING_DIR+bin_file_name + " " + WORKING_DIR+cpp_file_name + " -I " + BOOST_HOME)
			if (status != 0):
				PRINT_ERR("Error running g++\n")
				sys.exit()			
			if (arg_architecture == 'cpu'):
				status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_data_width_cpu) + " " + str(arg_loop_size))
				PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_data_width_cpu) + " " + str(arg_loop_size))
				if (status != 0):
					PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arg_architecture + "\n")
					sys.exit()	
			else:
				status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_data_width_cima) + " " + str(arg_loop_size))
				PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_data_width_cima) + " " + str(arg_loop_size))
				if (status != 0):
					PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arg_architecture + "\n")
					sys.exit()	
			#check_trace_file_exist(APP_FILE_NAME)
		else:
			for arch in ARCHITECURE_LIST:
				if (arch == 'cpu'):
					setting = set_param (arg_benchmark, arch, \
										arg_mcpat_template_cpu, arg_mcpat_template_cima, \
										arg_sinuca_conf_folder_cpu, arg_sinuca_conf_folder_cima, \
										arg_problem_size, arg_data_width_cpu, arg_data_width_cima, \
										arg_num_cores_cpu, arg_sync_conf)
					APP_FILE_NAME = setting.get('filename')
					thread_opt = setting.get('threads')
					arg_loop_size = setting.get('loopsize')
					cpp_file_name = setting.get('cpp')
					bin_file_name = setting.get('bin')

					if (check_trace_file_exist(APP_FILE_NAME) == 0):
						status = os.system("g++ -Wall -Wextra -std=c++11 -O2 -o " + WORKING_DIR+bin_file_name + " " + WORKING_DIR+cpp_file_name + " -I " + BOOST_HOME)
						if (status != 0):
							PRINT_ERR("Error running g++\n")
							sys.exit()
						status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_data_width_cpu) + " " + str(arg_loop_size))
						PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_data_width_cpu) + " " + str(arg_loop_size))
						if (status != 0):
							PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arch + "\n")
							sys.exit()						
					else:
						PRINT_WARN("These old trace files are used\n")
				else:
					setting = set_param (arg_benchmark, arch, \
										arg_mcpat_template_cpu, arg_mcpat_template_cima, \
										arg_sinuca_conf_folder_cpu, arg_sinuca_conf_folder_cima, \
										arg_problem_size, arg_data_width_cpu, arg_data_width_cima, \
										arg_num_cores_cpu, arg_sync_conf)
					APP_FILE_NAME = setting.get('filename')
					thread_opt = setting.get('threads')
					arg_loop_size = setting.get('loopsize')
					cpp_file_name = setting.get('cpp')
					bin_file_name = setting.get('bin')

					if (check_trace_file_exist(APP_FILE_NAME) == 0):
						status = os.system("g++ -Wall -Wextra -std=c++11 -O2 -o " + WORKING_DIR+bin_file_name + " " + WORKING_DIR+cpp_file_name + " -I " + BOOST_HOME)
						if (status != 0):
							PRINT_ERR("Error running g++\n")
							sys.exit()
						status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_data_width_cima) + " " + str(arg_loop_size))
						PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_data_width_cima) + " " + str(arg_loop_size))
						if (status != 0):
							PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arch + "\n")
							sys.exit()	
					else:
						PRINT_WARN("These old trace files are used\n")
	else:
		if (arg_architecture != 'all'):
			status = os.system("g++ -Wall -Wextra -std=c++11 -O2 -o " + WORKING_DIR+bin_file_name + " " + WORKING_DIR+cpp_file_name + " -I " + BOOST_HOME)
			if (status != 0):
				PRINT_ERR("Error running g++\n")
				sys.exit()			
			if (arg_architecture == 'cpu'):
				status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name)
				PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name)
				if (status != 0):
					PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arg_architecture + "\n")
					sys.exit()	
			else:
				PRINT_ERR("No execution as this was not implemented " + arg_architecture + "\n")
	
			#check_trace_file_exist(APP_FILE_NAME)
		else:
			PRINT_ERR("No execution as this was not implemented " + arg_architecture + "\n")
	
				#check_trace_file_exist(APP_FILE_NAME)
				# "Example of Usage: >$ ../../../pin -t obj-intel64/trace.so -output <output_file> -threads <threads> -parallel_start <parallel_start> -parallel_end <parallel_end>  -- <executable>\n";
	# Traces need to be ready now

	
################################################################################
## COMPLETE EXECUTION - GENERATE TRACES AND RESULTS
# 19. Complete execution
# input: two options:
# ------------------- vecsum: 
# --------------------------- 1 core: trace_generator_vecsum_singlecore.py
# --------------------------- 2 or more cores: trace_generator_vecsum_multicores.py
# ------------------- applications: c or cpp source code
# provided: trace generator (py scripts or sinuca trace generator)
# output: APP_FILE_NAME.result
################################################################################
elif (arg_execution == "execute"):				

	# Application

	PRINT_GOOD("=========================================================================")
	PRINT_GOOD("                          SiCIM for " + APP_FILE_NAME)
	PRINT_GOOD("=========================================================================")
	
	# Generate traces (exactly the same like above generate traces program
  # I don't want to call a function as a lot of parameters required
  # 19.1. Generate traces

	PRINT_GOOD("============================= TRACE GENERATOR ===========================")
	if ("vecsum" in arg_benchmark) or ('tcph' in arg_benchmark):
		if ("vecsum" in arg_benchmark):		
			if (int(arg_num_cores_cpu) == 1):
				arg_trace_gen_pyscript = 'trace_generator_vector_singlecore_v4.py'
			else:
				arg_trace_gen_pyscript = 'trace_generator_vector_multicores_v4.py'	
		elif ('tcph' in arg_benchmark):
			arg_trace_gen_pyscript = 'trace_generator_tpch_cs_cimaNew_vectorized_unrolled_v4f.py'
		arg_cima_on = 1
		#PRINT_DEBUG(arg_architecture)
		if (arg_architecture == 'cpu'):
			arg_cima_on = 0
		else:
			if (arg_architecture == 'all'):
				setting = set_param (arg_benchmark, 'cima', \
									arg_mcpat_template_cpu, arg_mcpat_template_cima, \
									arg_sinuca_conf_folder_cpu, arg_sinuca_conf_folder_cima, \
									arg_problem_size, arg_data_width_cpu, arg_data_width_cima, \
									arg_num_cores_cpu, arg_sync_conf)
				APP_FILE_NAME = setting.get('filename')
				if (check_trace_file_exist(APP_FILE_NAME) != 0):
					arg_cima_on = 0
				
						
		#PRINT_DEBUG(str(arg_cima_on))
		status = os.system("python " + arg_trace_gen_pyscript + " " + WORKING_DIR + " " + arg_benchmark + " " + arg_problem_size + " " + str(arg_data_width_cpu) + " " + str(arg_data_width_cima) + " " + str(arg_num_cores_cpu) + " " + str(arg_cima_on) + " " + str(arg_sync_conf) )
		if (status != 0):
			PRINT_ERR("Error running python script to generate traces\n")
			sys.exit()			
	elif ("forloop" in arg_benchmark):
		if (arg_architecture != 'all'):
			status = os.system("g++ -o " + WORKING_DIR+bin_file_name + " " + WORKING_DIR+cpp_file_name + " -I " + BOOST_HOME)
			if (status != 0):
				PRINT_ERR("Error running g++\n")
				sys.exit()			
			if (arg_architecture == 'cpu'):
				status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_loop_size) + " " + str(arg_sync_conf))
				PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_loop_size) + " " + str(arg_sync_conf))
				if (status != 0):
					PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arg_architecture + "\n")
					sys.exit()	
			else:
				status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_loop_size) + " " + str(arg_sync_conf))
				PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_loop_size) + " " + str(arg_sync_conf))
				if (status != 0):
					PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arg_architecture + "\n")
					sys.exit()	
			#check_trace_file_exist(APP_FILE_NAME)
		else:
			for arch in ARCHITECURE_LIST:
				if (arch == 'cpu'):
					setting = set_param (arg_benchmark, arch, \
										arg_mcpat_template_cpu, arg_mcpat_template_cima, \
										arg_sinuca_conf_folder_cpu, arg_sinuca_conf_folder_cima, \
										arg_problem_size, arg_data_width_cpu, arg_data_width_cima, \
										arg_num_cores_cpu, arg_sync_conf)
					APP_FILE_NAME = setting.get('filename')
					thread_opt = setting.get('threads')
					arg_loop_size = setting.get('loopsize')
					cpp_file_name = setting.get('cpp')
					bin_file_name = setting.get('bin')

					if (check_trace_file_exist(APP_FILE_NAME) == 0):
						status = os.system("g++ -o " + WORKING_DIR+bin_file_name + " " + WORKING_DIR+cpp_file_name + " -I " + BOOST_HOME)
						if (status != 0):
							PRINT_ERR("Error running g++\n")
							sys.exit()
						status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_loop_size) + " " + str(arg_sync_conf))
						PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_loop_size) + " " + str(arg_sync_conf))
						if (status != 0):
							PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arch + "\n")
							sys.exit()						
					else:
						PRINT_WARN("These old trace files are used\n")
				else:
					setting = set_param (arg_benchmark, arch, \
										arg_mcpat_template_cpu, arg_mcpat_template_cima, \
										arg_sinuca_conf_folder_cpu, arg_sinuca_conf_folder_cima, \
										arg_problem_size, arg_data_width_cpu, arg_data_width_cima, \
										arg_num_cores_cpu, arg_sync_conf)
					APP_FILE_NAME = setting.get('filename')
					thread_opt = setting.get('threads')
					arg_loop_size = setting.get('loopsize')
					cpp_file_name = setting.get('cpp')
					bin_file_name = setting.get('bin')

					if (check_trace_file_exist(APP_FILE_NAME) == 0):
						status = os.system("g++ -o " + WORKING_DIR+bin_file_name + " " + WORKING_DIR+cpp_file_name + " -I " + BOOST_HOME)
						if (status != 0):
							PRINT_ERR("Error running g++\n")
							sys.exit()
						status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_loop_size) + " " + str(arg_sync_conf))
						PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_loop_size) + " " + str(arg_sync_conf))
						if (status != 0):
							PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arch + "\n")
							sys.exit()	
					else:
						PRINT_WARN("These old trace files are used\n")
	elif ("simple_otp" in arg_benchmark):
		if (arg_architecture != 'all'):
			status = os.system("g++ -Wall -Wextra -std=c++11 -O2 -o " + WORKING_DIR+bin_file_name + " " + WORKING_DIR+cpp_file_name + " -I " + BOOST_HOME)
			if (status != 0):
				PRINT_ERR("Error running g++\n")
				sys.exit()			
			if (arg_architecture == 'cpu'):
				status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_data_width_cpu) + " " + str(arg_loop_size))
				PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_data_width_cpu) + " " + str(arg_loop_size))
				if (status != 0):
					PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arg_architecture + "\n")
					sys.exit()	
			else:
				status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_data_width_cima) + " " + str(arg_loop_size))
				PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_data_width_cima) + " " + str(arg_loop_size))
				if (status != 0):
					PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arg_architecture + "\n")
					sys.exit()	
			#check_trace_file_exist(APP_FILE_NAME)
		else:
			for arch in ARCHITECURE_LIST:
				if (arch == 'cpu'):
					setting = set_param (arg_benchmark, arch, \
										arg_mcpat_template_cpu, arg_mcpat_template_cima, \
										arg_sinuca_conf_folder_cpu, arg_sinuca_conf_folder_cima, \
										arg_problem_size, arg_data_width_cpu, arg_data_width_cima, \
										arg_num_cores_cpu, arg_sync_conf)
					APP_FILE_NAME = setting.get('filename')
					thread_opt = setting.get('threads')
					arg_loop_size = setting.get('loopsize')
					cpp_file_name = setting.get('cpp')
					bin_file_name = setting.get('bin')

					if (check_trace_file_exist(APP_FILE_NAME) == 0):
						status = os.system("g++ -Wall -Wextra -std=c++11 -O2 -o " + WORKING_DIR+bin_file_name + " " + WORKING_DIR+cpp_file_name + " -I " + BOOST_HOME)
						if (status != 0):
							PRINT_ERR("Error running g++\n")
							sys.exit()
						status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_data_width_cpu) + " " + str(arg_loop_size))
						PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_data_width_cpu) + " " + str(arg_loop_size))
						if (status != 0):
							PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arch + "\n")
							sys.exit()						
					else:
						PRINT_WARN("These old trace files are used\n")
				else:
					setting = set_param (arg_benchmark, arch, \
										arg_mcpat_template_cpu, arg_mcpat_template_cima, \
										arg_sinuca_conf_folder_cpu, arg_sinuca_conf_folder_cima, \
										arg_problem_size, arg_data_width_cpu, arg_data_width_cima, \
										arg_num_cores_cpu, arg_sync_conf)
					APP_FILE_NAME = setting.get('filename')
					thread_opt = setting.get('threads')
					arg_loop_size = setting.get('loopsize')
					cpp_file_name = setting.get('cpp')
					bin_file_name = setting.get('bin')

					if (check_trace_file_exist(APP_FILE_NAME) == 0):
						status = os.system("g++ -Wall -Wextra -std=c++11 -O2 -o " + WORKING_DIR+bin_file_name + " " + WORKING_DIR+cpp_file_name + " -I " + BOOST_HOME)
						if (status != 0):
							PRINT_ERR("Error running g++\n")
							sys.exit()
						status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_data_width_cima) + " " + str(arg_loop_size))
						PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -- " + WORKING_DIR+bin_file_name + " " + str(arg_data_width_cima) + " " + str(arg_loop_size))
						if (status != 0):
							PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arch + "\n")
							sys.exit()	
					else:
						PRINT_WARN("These old trace files are used\n")
	else:
		if (arg_architecture != 'all'):
			status = os.system("g++ -Wall -Wextra -std=c++11 -O2 -o " + WORKING_DIR+bin_file_name + " " + WORKING_DIR+cpp_file_name + " -I " + BOOST_HOME)
			if (status != 0):
				PRINT_ERR("Error running g++\n")
				sys.exit()			
			if (arg_architecture == 'cpu'):
				status = os.system(PIN_HOME + "pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name)
				PRINT_GOOD("pin" + PIN_OPT + PIN_TRACER + " -output " + WORKING_DIR+APP_FILE_NAME + " -threads " + thread_opt + " -- " + WORKING_DIR+bin_file_name)
				if (status != 0):
					PRINT_ERR("Error running Intel pin or SiNUCA trace generator " + arg_architecture + "\n")
					sys.exit()	
			else:
				PRINT_ERR("No execution as this was not implemented " + arg_architecture + "\n")
	
			#check_trace_file_exist(APP_FILE_NAME)
		else:
			PRINT_ERR("No execution as this was not implemented " + arg_architecture + "\n")
	
				#check_trace_file_exist(APP_FILE_NAME)
				# "Example of Usage: >$ ../../../pin -t obj-intel64/trace.so -output <output_file> -threads <threads> -parallel_start <parallel_start> -parallel_end <parallel_end>  -- <executable>\n";
	# Traces need to be ready now


	# Open result file
	setting = set_param (arg_benchmark, arg_architecture, \
						arg_mcpat_template_cpu, arg_mcpat_template_cima, \
						arg_sinuca_conf_folder_cpu, arg_sinuca_conf_folder_cima, \
						arg_problem_size, arg_data_width_cpu, arg_data_width_cima, \
						arg_num_cores_cpu, arg_sync_conf)
	APP_FILE_NAME = setting.get('filename')
	arg_mcpat_template = setting.get('mcpat')
	arg_sinuca_conf_folder = setting.get('sinuca')
	thread_opt = setting.get('threads')
	arg_loop_size = setting.get('loopsize')
	cpp_file_name = setting.get('cpp')
	bin_file_name = setting.get('bin')
	result_file_name = WORKING_DIR + APP_FILE_NAME + ".result"
	result_file = open(result_file_name, 'w')
	result_file.write("####################################################\n")
	result_file.write("Application: " + arg_benchmark + "\n")

	# Open an excel file for the result
	# workbook_file_name  = WORKING_DIR + APP_FILE_NAME + ".xlsx"
	# workbook_file = xlsxwriter.Workbook(workbook_file_name)
	# worksheet = workbook_file.add_worksheet()
  
  # Execute both architectures or each architecture separately
  # Both architecture
  # 19.2. Execute both architectures
	if (arg_architecture != "all"):
	
		PRINT_GOOD("===========================================================================")
		PRINT_GOOD("                       Architecture " + arg_architecture)
		PRINT_GOOD("===========================================================================")
		result_file.write("Architecture: " + arg_architecture)
		result_file.write("\n####################################################\n")
		result_file.close()
		performance = singleArchExecution(APP_FILE_NAME, arg_mcpat_template, result_file_name, arg_sinuca_conf_folder, arg_data_width_cima)
		# latency_ns = performance.get('latency_ns')
		# energy_nJ = performance.get('energy_nJ')
		# area_mm2 = performance.get('area_mm2')
		# row = 0
		# col = 0
		# pointer = print_to_excel_file(worksheet, APP_FILE_NAME, row, col, latency_ns, energy_nJ, area_mm2)
		# row = pointer.get('row')
		# col = pointer.get('col')
		# PRINT(str(row))
		# PRINT(str(col))

  # 19.3. Execute each architecture as specify in the input parameters	
	else:
		result_file.write("Architecture: cpu, cima")
		result_file.write("\n####################################################\n")
		result_file.close()
		for arch in ARCHITECURE_LIST: 
			PRINT_GOOD("===========================================================================")
			PRINT_GOOD("                            Architecture " + arch)
			PRINT_GOOD("===========================================================================")
			setting = set_param (arg_benchmark, arch, \
								arg_mcpat_template_cpu, arg_mcpat_template_cima, \
								arg_sinuca_conf_folder_cpu, arg_sinuca_conf_folder_cima, \
								arg_problem_size, arg_data_width_cpu, arg_data_width_cima, \
								arg_num_cores_cpu, arg_sync_conf)
			APP_FILE_NAME = setting.get('filename')
			arg_mcpat_template = setting.get('mcpat')
			arg_sinuca_conf_folder = setting.get('sinuca')
			performance = singleArchExecution(APP_FILE_NAME, arg_mcpat_template, result_file_name, arg_sinuca_conf_folder, arg_data_width_cima)
	
	#workbook_file.close()