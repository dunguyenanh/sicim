#!/bin/sh

#python 
#script_to_select_file_and_perform_estimation (1)
#mode (execute or generate only traces - check the script(1))
#benchmarks (name has to be defined in the script(1))
#architecture_select (cpu, cim, or all for both) --- in the script(1)
#configurations --- cpu --- cim (to generate mcpat file for power and area estimation)
#configurations --- cpu --- cim (for input of sinuca)
#problem sizes (not applicable to database application)
#datawidth --- cpu --- cim
#number of processor --- multicore: still need to be done
#cache configurations (only L1, L2 or L3) --- woL3 means that it has L1 and L2


#Execute encryption application (xor operations)// datawidth --- cpu 8 Bytes --- cim 1024 bytes // problem sizes from 32KB to 512KB
python script_SiCIM_tc_final.py "execute" "simple_otp_2" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "1KB" "8" "1024" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "simple_otp_2" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "2KB" "8" "1024" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "simple_otp_2" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "4KB" "8" "1024" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "simple_otp_2" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "8KB" "8" "1024" "1" "woL3"

#Execute encryption application (xor operations)// datawidth --- cpu 8 Bytes --- cim from 128 to 512 bytes // problem sizes from 32MB to 512MB
#python script_SiCIM_tc_final.py "execute" "simple_otp_2" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "32MB" "8" "512" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "simple_otp_2" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "32MB" "8" "256" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "simple_otp_2" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "32MB" "8" "128" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "simple_otp_2" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "32MB" "8" "64" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "simple_otp_2" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "32MB" "8" "32" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "simple_otp_2" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "32MB" "8" "16" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "simple_otp_2" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "32MB" "8" "8" "1" "woL3"

#Execute vector or operations// datawidth --- cpu 8 bytes --- cim 1024 bytes // problem sizes from 32KB to 512KB
python script_SiCIM_tc_final.py "execute" "vecsum" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "1KB" "8" "1024" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "vecsum" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "2KB" "8" "1024" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "vecsum" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "4KB" "8" "1024" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "vecsum" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "8KB" "8" "1024" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "vecsum" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "16KB" "8" "1024" "1" "woL3"

#Execute vector or operations// datawidth --- cpu 8 bytes --- cim size from 128 to 512 bytes // problem sizes from 32MB

#python script_SiCIM_tc_final.py "execute" "vecsum" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "32MB" "8" "512" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "vecsum" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "32MB" "8" "256" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "vecsum" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "32MB" "8" "128" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "vecsum" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "32MB" "8" "64" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "vecsum" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "32MB" "8" "32" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "vecsum" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "32MB" "8" "16" "1" "woL3"
#python script_SiCIM_tc_final.py "execute" "vecsum" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "32MB" "8" "8" "1" "woL3"


#Execute bitmap database (and operations) // datawidth --- cpu 8 bytes --- cim 1024 bytes // problem sizes 1GB // 0x unrolled 
python script_SiCIM_tc_final.py "execute" "tcph" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "1KB" "8" "1024" "1" "0x"
#Execute bitmap database (and operations) // datawidth --- cpu 8 bytes --- cim 512 bytes // problem sizes 1GB // 0x unrolled 
#python script_SiCIM_tc_final.py "execute" "tcph" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "1KB" "8" "8" "1" "0x"
#Execute bitmap database (and operations) // datawidth --- cpu 8 bytes --- cim 256 bytes // problem sizes 1GB // 0x unrolled 
#python script_SiCIM_tc_final.py "execute" "tcph" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "1KB" "8" "16" "1" "0x"
#python script_SiCIM_tc_final.py "execute" "tcph" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "1KB" "8" "128" "1" "0x"
#python script_SiCIM_tc_final.py "execute" "tcph" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "1KB" "8" "64" "1" "0x"
#python script_SiCIM_tc_final.py "execute" "tcph" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "1KB" "8" "32" "1" "0x"
#python script_SiCIM_tc_final.py "execute" "tcph" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "1KB" "8" "512" "1" "0x"
#python script_SiCIM_tc_final.py "execute" "tcph" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "1KB" "8" "256" "1" "0x"
#python script_SiCIM_tc_final.py "execute" "tcph" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "1KB" "8" "1024" "1" "0x"
#Execute bitmap database (and operations) // datawidth --- cpu 8 bytes --- cim 128 bytes // problem sizes 1GB // 0x unrolled 
#python script_SiCIM_tc_final.py "execute" "tcph" "all" "sandy_bridge_1cores_woL3.xml" "sandy_bridge_1cores_woL3.xml" "sandy_1core_cim_woL3/" "sandy_1core_cim_woL3/" "1KB" "8" "128" "1" "0x"


