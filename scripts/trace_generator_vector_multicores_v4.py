## Code Sample
# ~ 402e59:       f3 42 0f 6f 0c 0b       movdqu (%rbx,%r9,1),%xmm1
# ~ 402e5f:       48 83 c5 01             add    $0x1,%rbp
# ~ 402e63:       f3 43 0f 6f 04 0b       movdqu (%r11,%r9,1),%xmm0
# ~ 402e69:       66 0f fe c1             paddd  %xmm1,%xmm0
# ~ 402e6d:       f3 43 0f 7f 04 0a       movdqu %xmm0,(%r10,%r9,1)
# ~ 402e73:       49 83 c1 10             add    $0x10,%r9
# ~ 402e77:       49 39 ed                cmp    %rbp,%r13
# ~ 402e7a:       77 dd                   ja     402e59 <main._omp_fn.0+0xc9>

## 1MB => 262144
## 2MB => 524288
## 4MB => 1048576
## 8MB => 2097152
## 16MB => 4194304
## 32MB => 8388608
## 64MB => 16777216
## 1GB => 268435456

import sys
import subprocess
import os

if (len(sys.argv) != 9):
	print("Missing parameter!\n")
	sys.exit("Usage:\n"
		+"\t python trace_generator_vecsum_4.py + ARG_WORKING_DIR  ARG_BENCHMARK ARG_PROBLEM_SIZE ARG_REG_SIZE_CPU ARG_REG_SIZE_CIMA ARG_NUM_CORES_CPU ARG_CIM_ON ARG_CONFIG \n" )
else:
	BASEDIR = sys.argv[1]
	BENCHMARK = sys.argv[2]
	PROBLEM_SIZE = sys.argv[3]
	arg_reg_size_cpu = sys.argv[4]
	arg_reg_size_cima = sys.argv[5]
	arg_num_core_cpu = sys.argv[6]
	arg_cima_on = sys.argv[7]
	arg_config = sys.argv[8]
	
PROBLEM_SIZE_DICT = { '1KB': 512,\
					  '2KB': 1024,\
					  '4KB': 2048,\
					  '8KB': 4096,\
					  '16KB': 8192,\
					  '32KB': 16384,\
					  '64KB': 32768,\
					  '128KB': 65536,\
					  '256KB': 131072,\
					  '512KB': 262144,\
					  '1MB': 524288,\
					  '2MB': 1048576,\
					  '4MB': 2097152,\
					  '8MB': 4194304,\
					  '16MB': 8388608,\
					  '32MB': 16777216,\
					  '64MB': 33554432,\
					  '128MB': 67108864,\
					  '256MB': 134217728,\
					  '512MB': 268435456,\
					  '1GB': 536870912	}
#BASEDIR = "/home/hadunguyen/bulk/sinuca/sinuca-cim/examples/vecsum6/"
#BENCHMARK = "vecsum"
print (arg_cima_on)
for ELEMENTS in [PROBLEM_SIZE_DICT[PROBLEM_SIZE]] :
    #for THREADS in [1]:#, 2, 4, 8, 16]:#, 32, 64] :
	THREADS = 1
	ELEM_SIZE=2
	SIZE_MB=(ELEMENTS * ELEM_SIZE) / 1024 / 1024
	ELEM_PER_THREAD=int(ELEMENTS) // THREADS
	########################################################################
	##  CIMA Traces - Normal (8KB)
	########################################################################
	if (int(arg_cima_on) == 1):
		REG_SIZE = arg_reg_size_cima
		ELEM_PER_REG= int(REG_SIZE) // ELEM_SIZE
		# REG_SIZE=ELEM_PER_REG * ELEM_SIZE
		ARCH = "cima"
		static_trace = BASEDIR + ARCH + "_" + BENCHMARK + "_" + PROBLEM_SIZE + "_" + str(REG_SIZE) + "B_" + str(arg_config) + "." + "tid0.stat.out"
		print (static_trace)

		FILE = open(static_trace, 'w')
		FILE.write("# SiNUCA Trace\n"                                   )
		FILE.write("@1\n"                                               )
		FILE.write("ADD 1 1024 4 1 5 2 5 25 0 0 0 0 0 3 0 0 0\n"          )
		FILE.write("HIVE_LK 14 1028 4 1 12 1 102 10 0 0 0 0 3 0 0 1 -1 -1 -1\n"    ) ## LOCK

		FILE.write("CIM_ADR_LD 27 1032 6 2 7 12 1 101 7 12 1 0 0 3 0 0 1 -1 -1 0\n"   ) ## LOAD C0
		FILE.write("CIM_ADR_LD 27 1038 6 2 8 12 1 100 8 12 1 0 0 3 0 0 1 -1 -1 8\n"   ) ## LOAD C1
		#FILE.write("CIM_ADR_LD 27 1032 6 2 7 12 1 101 7 12 1 0 0 3 0 0 1 -1 -1 1\n"   ) ## LOAD C2
		#FILE.write("CIM_ADR_LD 27 1038 6 2 8 12 1 100 8 12 1 0 0 3 0 0 1 -1 -1 9\n"   ) ## LOAD C3
		
		FILE.write("CIM_BTW_OR 24 1044 4 1 12 1 102 0 0 0 0 0 3 0 0 1 0 8 8\n"   ) ## CMP
		#FILE.write("CIM_BTW_OR 24 1044 4 1 12 1 102 0 0 0 0 0 3 0 0 1 1 9 9\n"   ) ## CMP
		
		FILE.write("CIM_MEM_ST 29 1048 6 2 9 12 0 9 12 0 0 1 3 0 0 1 8 -1 -1\n"       ) ## STORE C1        
		#FILE.write("CIM_MEM_ST 29 1048 6 2 9 12 0 9 12 0 0 1 3 0 0 1 9 -1 -1\n"       ) ## STORE C4

		FILE.write("HIVE_UNLK 15 1054 4 1 12 1 102 10 0 0 0 0 3 0 0 1 -1 -1 -1\n"  ) ## UNLOCK

		FILE.write("ADD 1 1058 4 1 12 2 12 25 0 0 0 0 0 3 0 0 0\n"        )
		FILE.write("CMP 1 1060 3 2 16 5 1 25 0 0 0 0 0 3 0 0 0\n"         )
		FILE.write("JNBE 7 1063 2 2 26 25 1 26 0 0 0 0 0 4 0 0 0\n"       )
		FILE.write("# eof\n"                                            )
		FILE.close()

		for t in range(THREADS):
			dynamic_trace = BASEDIR + ARCH + "_" + BENCHMARK + "_" + PROBLEM_SIZE  + "_" + str(REG_SIZE) + "B_" + str(arg_config) + "." + "tid" + str(t) + ".dyn.out"
			print (dynamic_trace)

			FILE = open(dynamic_trace, 'w')
			FILE.write("# SiNUCA Trace\n")
			for i in range(0, ELEM_PER_THREAD, ELEM_PER_REG):
				FILE.write("1\n")
			FILE.write("# eof\n")
			FILE.close()

		ADDR_A=1024*1024*1024
		ADDR_B=1024*1024*2048
		#ADDR_C=1024*1024*3072
		#ADDR_D=1024*1024*4096
		ADDR_E=1024*1024*6144
		#ADDR_F=1024*1024*8192
		for t in range(THREADS):
			memory_trace = BASEDIR + ARCH + "_" + BENCHMARK + "_" + PROBLEM_SIZE  + "_" + str(REG_SIZE) + "B_" + str(arg_config) + "." + "tid" + str(t) + ".mem.out"
			print (memory_trace)

			FILE = open(memory_trace, 'w')
			FILE.write("# SiNUCA Trace\n")

			for i in range(0, ELEM_PER_THREAD, ELEM_PER_REG):
				FILE.write("R 1 " + str(ADDR_A) + " 1\n")
				FILE.write("R 1 " + str(ADDR_B) + " 1\n")
				#FILE.write("R 1 " + str(ADDR_C) + " 1\n")
				#FILE.write("R 1 " + str(ADDR_D) + " 1\n")
				FILE.write("W 1 " + str(ADDR_E) + " 1\n")
				#FILE.write("W 1 " + str(ADDR_F) + " 1\n")

				ADDR_A=ADDR_A + int(REG_SIZE)
				ADDR_B=ADDR_B + int(REG_SIZE)
				#ADDR_C=ADDR_C + int(REG_SIZE)
				#ADDR_D=ADDR_D + int(REG_SIZE)
				ADDR_E=ADDR_E + int(REG_SIZE)
				#ADDR_F=ADDR_F + int(REG_SIZE)
			FILE.write("# eof\n")
			FILE.close()
		print ("Compressing...")
		os.system("gzip " + BASEDIR + ARCH + "_" +  BENCHMARK + "_" + PROBLEM_SIZE + "_" + str(REG_SIZE) + "*." + "out")
	else:
		print ("No CIMA traces generated\n")
# ~
	########################################################################
	##  CPU Traces - Normal (8KB)
	########################################################################
	THREADS = int(arg_num_core_cpu)
	REG_SIZE = int(arg_reg_size_cpu)
	ELEM_PER_REG= int(REG_SIZE) // (ELEM_SIZE)
	ELEM_PER_THREAD=ELEMENTS // THREADS
	ARCH = "cpu"
	static_trace = BASEDIR + ARCH + "_" + BENCHMARK + "_" + PROBLEM_SIZE + "_" + str(REG_SIZE) + "B_" + str(THREADS) + "cores_" + str(arg_config) + "." + "tid0.stat.out"
	print (static_trace)

	FILE = open(static_trace, 'w')
	FILE.write("# SiNUCA Trace\n"                                   )
	FILE.write("@1\n"                                               )
	FILE.write("ADD 1 1024 4 1 5 2 5 25 0 0 0 0 0 3 0 0 0\n"          )

	FILE.write("MOV 8 1032 6 2 7 12 1 101 7 12 1 0 0 3 0 0 0\n"   ) ## LOAD C0
	FILE.write("MOV 8 1038 6 2 8 12 1 100 8 12 1 0 0 3 0 0 0\n"   ) ## LOAD C1
	#FILE.write("MOV 8 1032 6 2 7 12 1 101 7 12 1 0 0 3 0 0 0\n"   ) ## LOAD C2
	#FILE.write("MOV 8 1038 6 2 8 12 1 100 8 12 1 0 0 3 0 0 0\n"   ) ## LOAD C3
	
	FILE.write("ADD 1 1044 4 1 12 1 102 0 0 0 0 0 3 0 0 0\n"   ) ## CMP
	#FILE.write("ADD 1 1044 4 1 12 1 102 0 0 0 0 0 3 0 0 0\n"   ) ## CMP
	
	FILE.write("MOV 9 1048 6 2 9 12 0 9 12 0 0 1 3 0 0 0\n"       ) ## STORE C1        
	#FILE.write("MOV 9 1048 6 2 9 12 0 9 12 0 0 1 3 0 0 0\n"       ) ## STORE C4

	FILE.write("ADD 1 1058 4 1 12 2 12 25 0 0 0 0 0 3 0 0 0\n"        )
	FILE.write("CMP 1 1060 3 2 16 5 1 25 0 0 0 0 0 3 0 0 0\n"         )
	FILE.write("JNBE 7 1063 2 2 26 25 1 26 0 0 0 0 0 4 0 0 0\n"       )
	FILE.write("# eof\n"                                            )
	FILE.close()

	for t in range(THREADS):
		dynamic_trace = BASEDIR + ARCH + "_" + BENCHMARK + "_" + PROBLEM_SIZE + "_" + str(REG_SIZE) + "B_" + str(THREADS) + "cores_" + str(arg_config) + "." + "tid" + str(t) + ".dyn.out"
		print (dynamic_trace)

		FILE = open(dynamic_trace, 'w')
		FILE.write("# SiNUCA Trace\n")
		for i in range(0, ELEM_PER_THREAD, ELEM_PER_REG):
			FILE.write("1\n")
		FILE.write("# eof\n")
		FILE.close()

	ADDR_A=1024*1024*1024
	ADDR_B=1024*1024*2048
	#ADDR_C=1024*1024*3072
	#ADDR_D=1024*1024*4096
	ADDR_E=1024*1024*6144
	#ADDR_F=1024*1024*8192
	for t in range(THREADS):
		memory_trace = BASEDIR + ARCH + "_" + BENCHMARK + "_" + PROBLEM_SIZE + "_" + str(REG_SIZE) + "B_" + str(THREADS) + "cores_" + str(arg_config) + "." + "tid" + str(t) + ".mem.out"
		print (memory_trace)

		FILE = open(memory_trace, 'w')
		FILE.write("# SiNUCA Trace\n")

		for i in range(0, ELEM_PER_THREAD, ELEM_PER_REG):
			FILE.write("R 64 " + str(ADDR_A) + " 1\n")
			FILE.write("R 64 " + str(ADDR_B) + " 1\n")
			#FILE.write("R 1 " + str(ADDR_C) + " 1\n")
			#FILE.write("R 1 " + str(ADDR_D) + " 1\n")
			FILE.write("W 64 " + str(ADDR_E) + " 1\n")
			#FILE.write("W 1 " + str(ADDR_F) + " 1\n")

			ADDR_A=ADDR_A + int(REG_SIZE)
			ADDR_B=ADDR_B + int(REG_SIZE)
			#ADDR_C=ADDR_C + int(REG_SIZE)
			#ADDR_D=ADDR_D + int(REG_SIZE)
			ADDR_E=ADDR_E + int(REG_SIZE)
			#ADDR_F=ADDR_F + int(REG_SIZE)
			
		FILE.write("# eof\n")
		FILE.close()       
	print ("Compressing...")
	os.system("gzip " + BASEDIR + ARCH + "_" +  BENCHMARK + "_" + PROBLEM_SIZE + "_" + str(REG_SIZE) + "B_" + str(THREADS) + "cores" + "*." + "out")
	#subprocess.Popen("gzip *cmpimm." + str(SIZE_MB) + "MB." + str(THREADS) + "t*.out", shell=True)