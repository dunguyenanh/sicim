/*
 * Copyright (C) 2009~2017  Marco Antonio Zanata Alves
 *                          (mazalves at inf.ufrgs.br)
 *                          GPPD - Parallel and Distributed Processing Group
 *                          Universidade Federal do Rio Grande do Sul
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./sinuca.hpp"
#include <string>

sinuca_engine_t sinuca_engine;

// =============================================================================
static void display_use() {
    SINUCA_PRINTF("\n===============================================================================\n");
    SINUCA_PRINTF("SiNUCA simulates multi-core architectures with non-uniform cache architectures.\n\n");

    SINUCA_PRINTF("Usage: sinuca CONFIGURATION TRACE [OPTIONS] \n\n");
    SINUCA_PRINTF("\t CONFIGURATION is a configuration file which specify the architectural parameters.\n");
    SINUCA_PRINTF("\t TRACE is the base name for the three (static instruction, dynamic instruction, dynamic memory) trace files.\n");
    SINUCA_PRINTF("\t Example: ./sinuca --config CONFIGURATION --trace TRACE\n\n");

    SINUCA_PRINTF(" DESCRIPTION\n");
    SINUCA_PRINTF("\t -a or --affinity   \t THREADS       \t Inform a different affinity between the trace files and the cores.\n");
    SINUCA_PRINTF("\t -c or --config     \t FILE          \t Configuration file which describes the architecture. **Required\n");
    SINUCA_PRINTF("\t -g or --graph      \t FILE          \t Output graph file name to be used with GraphViz.\n");
    SINUCA_PRINTF("\t -h or --help       \t               \t Show this help.\n");
    SINUCA_PRINTF("\t -m or --map        \t FILE          \t Inform a user defined mapping between the memory addresses and NUMA nodes.\n");
    SINUCA_PRINTF("\t -r or --result     \t FILE          \t Output result file name. Default is \"stdout\".\n");
    SINUCA_PRINTF("\t -s or --stopat     \t INSTRUCTIONS  \t Instructions (opcodes) to be executed before stop the simulation. Default is trace size.\n");
    SINUCA_PRINTF("\t -t or --trace      \t FILE          \t Trace file base name. **Required\n");
    SINUCA_PRINTF("\t -w or --warmup     \t INSTRUCTIONS  \t Warm-up instructions (opcodes) before start statistics. Default is 0.\n");

    exit(EXIT_FAILURE);
};

// =============================================================================
static void process_argv(int argc, char **argv) {

    sinuca_engine.arg_trace_file_name = utils_t::template_allocate_array<char*>(MAX_CORES);

    uint32_t core_affinity = 0;
    struct stat buf;

    // Name, {no_argument, required_argument and optional_argument}, flag, value
    static struct option long_options[] = {
        {"affinity",    required_argument, 0, 'a'},
        {"config",      required_argument, 0, 'c'},
        {"graph",       required_argument, 0, 'g'},
        {"help",        no_argument, 0, 'h'},
        {"map",         required_argument, 0, 'm'},
        {"result",      required_argument, 0, 'r'},
        {"stopat",      required_argument, 0, 's'},
        {"trace",       required_argument, 0, 't'},
        {"warmup",      required_argument, 0, 'w'},
        {NULL,          0, NULL, 0}
    };

    // Count number of traces
    int opt;
    int option_index = 0;
    while ((opt = getopt_long_only(argc, argv, "a:c:g:hm:r:s:t:w:",
                 long_options, &option_index)) != -1) {
        switch (opt) {
        case 0:
            printf ("Option %s", long_options[option_index].name);
            if (optarg)
                printf (" with arg %s", optarg);
            printf ("\n");
            break;

        case 'a':
            sinuca_engine.arg_default_affinity = false;
            char *pch;
            pch = strtok(optarg, ",");
            core_affinity = 0;
            while (pch != NULL) {
                sinuca_engine.thread_affinity[core_affinity++] = atoi(pch);
                pch = strtok(NULL, ",");
            }
            break;

        case 'c':
            ERROR_ASSERT_PRINTF(sinuca_engine.arg_configuration_file_name == NULL, "Multiple config files are not supported.\n");
            sinuca_engine.arg_configuration_file_name = optarg;
            break;

        case 'g':
            sinuca_engine.arg_graph_file_name = optarg;
            if (stat(sinuca_engine.arg_graph_file_name, &buf) == 0) {
                SINUCA_PRINTF("Graph file already exist: %s\n\n", sinuca_engine.arg_graph_file_name)
                display_use();
            }
            break;

        case 'h':
            display_use();
            break;

        case 'm':
            sinuca_engine.arg_map_file_name = optarg;
            if (stat(sinuca_engine.arg_map_file_name, &buf) == -1) {
                SINUCA_PRINTF("Mapping file does not exist: %s\n\n", sinuca_engine.arg_map_file_name)
                display_use();
            }

            break;

        case 'r':
            ERROR_ASSERT_PRINTF(sinuca_engine.arg_result_file_name == NULL, "Multiple result files are not supported.\n");
            sinuca_engine.arg_result_file_name = optarg;
            if (stat(sinuca_engine.arg_result_file_name, &buf) == 0) {
                SINUCA_PRINTF("Result file already exist: %s\n\n", sinuca_engine.arg_result_file_name)
                display_use();
            }
            break;

        case 's':
            sinuca_engine.arg_stopat_instructions = atoi(optarg);
            if (atoi(optarg) <= 0) {
                SINUCA_PRINTF(">> Stop-at instructions should be greater than zero.\n\n")
                display_use();
            }
            break;

        case 't':
            sinuca_engine.arg_trace_file_name[sinuca_engine.total_trace_file_name] = optarg;
            sinuca_engine.total_trace_file_name++;
            break;

        case 'w':
            sinuca_engine.arg_warmup_instructions = atoi(optarg);
            if (atoi(optarg) < 0) {
                SINUCA_PRINTF(">> Warm-up instructions should be greater or equal than zero.\n\n")
                display_use();
            }
            break;

        case '?':
            break;

        default:
            SINUCA_PRINTF(">> getopt returned character code 0%o ??\n", opt);
        }
    }
    if (optind < argc) {
        SINUCA_PRINTF("Non-option ARGV-elements: ");
        while (optind < argc)
            SINUCA_PRINTF("%s ", argv[optind++]);
        SINUCA_PRINTF("\n");
    }

    if (sinuca_engine.arg_configuration_file_name == NULL) {
        SINUCA_PRINTF("Configuration file not defined.\n");
        display_use();
    }
    else if (sinuca_engine.total_trace_file_name == 0) {
        SINUCA_PRINTF("Trace file not defined.\n");
        display_use();
    }

    /// Check for all the trace files if they exist for thread 0
    char file_name[500];
    for (uint32_t i = 0; i < sinuca_engine.total_trace_file_name; i++) {
        file_name[0] = '\0';
        snprintf(file_name, sizeof(file_name), "%s.tid%d.stat.out.gz", sinuca_engine.arg_trace_file_name[i], 0);
        if (stat(file_name, &buf) == -1) {
            SINUCA_PRINTF("Static trace file does not exist: %s\n\n", file_name)
            display_use();
        }

        file_name[0] = '\0';
        snprintf(file_name, sizeof(file_name), "%s.tid%d.dyn.out.gz", sinuca_engine.arg_trace_file_name[i], 0);
        if (stat(file_name, &buf) == -1) {
            SINUCA_PRINTF("Dynamic trace file does not exist: %s\n\n", file_name)
            display_use();
        }

        file_name[0] = '\0';
        snprintf(file_name, sizeof(file_name), "%s.tid%d.mem.out.gz", sinuca_engine.arg_trace_file_name[i], 0);
        if (stat(file_name, &buf) == -1) {
            SINUCA_PRINTF("Memory trace file does not exist: %s\n\n", file_name)
            display_use();
        }
    }

    uint32_t configuration_file_size;
    configuration_file_size = strlen(sinuca_engine.arg_configuration_file_name) + 1;
    sinuca_engine.arg_configuration_path = utils_t::template_allocate_array<char>(configuration_file_size);
    utils_t::get_path(sinuca_engine.arg_configuration_path, sinuca_engine.arg_configuration_file_name);

    sinuca_engine.global_open_output_files();

    SINUCA_PRINTF("=======================  SiNUCA  =======================\n");
    SINUCA_PRINTF("CONFIGURATION FILE:  %s\n", sinuca_engine.arg_configuration_file_name != NULL ? sinuca_engine.arg_configuration_file_name : "MISSING");
    SINUCA_PRINTF("CONFIGURATION PATH:  %s\n", sinuca_engine.arg_configuration_path      != NULL ? sinuca_engine.arg_configuration_path      : "MISSING");
    SINUCA_PRINTF("TRACE FILES:         \n");
    for (uint32_t i = 0; i < sinuca_engine.total_trace_file_name; i++) {
        SINUCA_PRINTF("\t\t%s\n",sinuca_engine.arg_trace_file_name[i]);
    }
    SINUCA_PRINTF("RESULT FILE:         %s\n", sinuca_engine.arg_result_file_name        != NULL ? sinuca_engine.arg_result_file_name        : "MISSING");
    SINUCA_PRINTF("WARM-UP OPCODES:     %u\n", sinuca_engine.arg_warmup_instructions);
    SINUCA_PRINTF("STOP-AT OPCODES:     %u\n", sinuca_engine.arg_stopat_instructions);
    SINUCA_PRINTF("GRAPH FILE:          %s\n", sinuca_engine.arg_graph_file_name         != NULL ? sinuca_engine.arg_graph_file_name        : "MISSING");
    SINUCA_PRINTF("AFFINITY:            %s\n", sinuca_engine.arg_default_affinity ? "DEFAULT" : "USER DEFINED");

    for (uint32_t i = 0; i < core_affinity; i++) {
        SINUCA_PRINTF("\t Thread[%" PRIu32 "] -> Core[%" PRIu32 "]\n", i, sinuca_engine.thread_affinity[i]);
    }

    SINUCA_PRINTF("MAP FILE:            %s\n", sinuca_engine.arg_map_file_name        != NULL ? sinuca_engine.arg_map_file_name        : "MISSING");
};

// =============================================================================
static void premature_termination(int signo) {
    switch(signo) {
        case SIGABRT : SINUCA_PRINTF("SiNUCA received SIGABRT signal."); break;
        case SIGILL  : SINUCA_PRINTF("SiNUCA received SIGILL  signal."); break;
        case SIGINT  : SINUCA_PRINTF("SiNUCA received SIGINT  signal."); break;
        case SIGSEGV : SINUCA_PRINTF("SiNUCA received SIGSEGV signal."); break;
        case SIGTERM : SINUCA_PRINTF("SiNUCA received SIGTERM signal."); break;
    }

    sinuca_engine.premature_termination();
    exit(EXIT_FAILURE);
}

// =============================================================================
std::string simulation_status_to_string() {
    std::string final_report;
    char tmp_char[1000];

    uint64_t ActualLength = 0;
    uint64_t FullLength = 0;
    uint32_t active_cores = 0;

    /// Get seconds without micro-seconds
    gettimeofday(&sinuca_engine.stat_timer_end, NULL);
    double seconds_spent = sinuca_engine.stat_timer_end.tv_sec - sinuca_engine.stat_timer_start.tv_sec;

    /// Get global statistics from all the cores
    for (uint32_t cpu = 0 ; cpu < sinuca_engine.get_processor_array_size() ; cpu++) {
        ActualLength += sinuca_engine.trace_reader->get_trace_opcode_counter(cpu);
        FullLength += sinuca_engine.trace_reader->get_trace_opcode_max(cpu) + 1;
        active_cores += !sinuca_engine.is_processor_trace_eof[cpu];
    }
    // =====================================================================
    /// Computer the Global Percentage Completed
    double percentage_complete = 100.0 * (static_cast<double>(ActualLength) / static_cast<double>(FullLength));
    snprintf(tmp_char, sizeof(tmp_char), " Progress: %8.4lf%%", percentage_complete);
    final_report += tmp_char;

    snprintf(tmp_char, sizeof(tmp_char), " %s", utils_t::progress_pretty(ActualLength, FullLength).c_str());
    final_report += tmp_char;

    snprintf(tmp_char, sizeof(tmp_char), " Active Cores: %02" PRIu32 "", active_cores);
    final_report += tmp_char;

    /// Compute the Global Estimate Time to Complete (GETC)
    uint64_t seconds_remaining = (100*(seconds_spent / percentage_complete)) - seconds_spent;
    snprintf(tmp_char, sizeof(tmp_char), "     Global ETC(%02.0f:%02.0f:%02.0f)\n", floor(seconds_remaining / 3600.0),
                                                                floor(fmod(seconds_remaining, 3600.0) / 60.0),
                                                                fmod(seconds_remaining, 60.0));
    final_report += tmp_char;

    for (uint32_t cpu = 0 ; cpu < sinuca_engine.get_processor_array_size() ; cpu++) {
        ActualLength = sinuca_engine.trace_reader->get_trace_opcode_counter(cpu);
        FullLength = sinuca_engine.trace_reader->get_trace_opcode_max(cpu) + 1;

        snprintf(tmp_char, sizeof(tmp_char), "  > CPU %02d", cpu);
        final_report += tmp_char;

        snprintf(tmp_char, sizeof(tmp_char), " - Opcode[%10" PRIu64 "/%10" PRIu64 "]", ActualLength, FullLength);
        final_report += tmp_char;

        percentage_complete = 100.0 * (static_cast<double>(ActualLength) / static_cast<double>(FullLength));;
        snprintf(tmp_char, sizeof(tmp_char), " %8.4lf%%", percentage_complete);
        final_report += tmp_char;

        snprintf(tmp_char, sizeof(tmp_char), " %s", utils_t::progress_pretty(ActualLength, FullLength).c_str());
        final_report += tmp_char;

        snprintf(tmp_char, sizeof(tmp_char), " IPC(%5.3lf)", static_cast<double>(ActualLength) / static_cast<double>(sinuca_engine.get_global_cycle()));
        final_report += tmp_char;

        snprintf(tmp_char, sizeof(tmp_char), " [%9s]", get_enum_sync_char(sinuca_engine.processor_array[cpu]->get_sync_status()));
        final_report += tmp_char;

        snprintf(tmp_char, sizeof(tmp_char), " [%s]", sinuca_engine.is_processor_trace_eof[cpu] ? "OFF" : "ON");
        final_report += tmp_char;

        // =====================================================================
        /// Compute the Estimate Time to Complete (ETC)
        seconds_remaining = (100*(seconds_spent / percentage_complete)) - seconds_spent;
        snprintf(tmp_char, sizeof(tmp_char), " ETC(%02.0f:%02.0f:%02.0f)\n",
                                                floor(seconds_remaining / 3600.0),
                                                floor(fmod(seconds_remaining, 3600.0) / 60.0),
                                                fmod(seconds_remaining, 60.0));
        final_report += tmp_char;
    }
    return final_report;
};

// =============================================================================
int main(int argc, char **argv) {
    process_argv(argc, argv);

    // =========================================================================
    /// Register a user-defined signal handler.
    signal(SIGABRT, premature_termination);   ///< It is sent to a process to tell it to abort, i.e. to terminate.
    signal(SIGILL,  premature_termination);   ///< It is sent to a process when it attempts to execute an illegal instruction.
    signal(SIGINT,  premature_termination);   ///< It is sent to a process by its controlling terminal when a user interrupt the process.
    signal(SIGSEGV, premature_termination);   ///< It is sent to a process when it makes an invalid virtual memory reference, or seg. fault.
    signal(SIGTERM, premature_termination);   ///< It is sent to a process to request its termination.

    sinuca_engine.initialize();

    ERROR_ASSERT_PRINTF(sinuca_engine.get_processor_array_size() <= MAX_CORES, "Configuration has more processors than supported (MAX_CORES).\n")
    sinuca_engine.is_processor_trace_eof = utils_t::template_allocate_initialize_array<bool>(sinuca_engine.get_processor_array_size(), false);
    sinuca_engine.trace_reader->allocate(sinuca_engine.total_trace_file_name ,sinuca_engine.arg_trace_file_name, sinuca_engine.get_processor_array_size());

    sinuca_engine.global_reset_statistics();

    SINUCA_PRINTF("\n");
    SINUCA_PRINTF("=====================  Simulating  =====================\n");

    SINUCA_PRINTF("Warm-Up Start - Cycle: %-12" PRIu64 "\n", sinuca_engine.get_global_cycle() );

    /// Start CLOCK
    while (sinuca_engine.get_is_simulation_allocated() && sinuca_engine.alive()) {
        /// Spawn Warmup - is_warmup is set inside the trace_reader
        if (sinuca_engine.is_warmup == true) {
            SINUCA_PRINTF("Warm-Up End - Cycle: %-12" PRIu64 "\n", sinuca_engine.get_global_cycle() );

            sinuca_engine.global_reset_statistics();
            sinuca_engine.is_warmup = false;
        }

        /// Progress Information
        if ((sinuca_engine.get_global_cycle() % HEART_BEAT) == 0) {
            SINUCA_PRINTF("Heart-Beat - Cycle: %-12" PRIu64 "", sinuca_engine.get_global_cycle() );
            SINUCA_PRINTF("%s\n", simulation_status_to_string().c_str());
        }

        /// Spawn Periodic Check
        if ((sinuca_engine.get_global_cycle() % PERIODIC_CHECK) == 0) {
            sinuca_engine.global_periodic_check();
        }

        /// Spawn Clock Signal
        sinuca_engine.global_clock();
    }

    SINUCA_PRINTF("Evicting all cache lines... \n")

    /// Evict all the cache lines.
    bool all_evicted = false;
    while (all_evicted != true){
        /// Progress Information
        if ((sinuca_engine.get_global_cycle() % HEART_BEAT) == 0) {
            SINUCA_PRINTF("Heart-Beat - Cycle: %-12" PRIu64 "\n", sinuca_engine.get_global_cycle() );
            SINUCA_PRINTF("%s\n", simulation_status_to_string().c_str());
        }

        /// Spawn Periodic Check
        if ((sinuca_engine.get_global_cycle() % PERIODIC_CHECK) == 0) {
            sinuca_engine.global_periodic_check();
        }

        all_evicted = sinuca_engine.directory_controller->coherence_evict_all();

        /// Spawn Clock Signal
        sinuca_engine.global_clock();
    }

    SINUCA_PRINTF("!Finished! - Cycle: %-12" PRIu64 "", sinuca_engine.get_global_cycle() );
    SINUCA_PRINTF("%s\n", simulation_status_to_string().c_str());

    sinuca_engine.global_print_configuration();
    sinuca_engine.global_print_statistics();
    sinuca_engine.global_print_graph();

    return(EXIT_SUCCESS);
};

