/*
 * Copyright (C) 2009~2017  Marco Antonio Zanata Alves
 *                          (mazalves at inf.ufrgs.br)
 *                          GPPD - Parallel and Distributed Processing Group
 *                          Universidade Federal do Rio Grande do Sul
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../sinuca.hpp"

#ifdef MEMORY_CONTROLLER_DEBUG
    #define MEMORY_CONTROLLER_DEBUG_PRINTF(...) DEBUG_PRINTF(__VA_ARGS__);
#else
    #define MEMORY_CONTROLLER_DEBUG_PRINTF(...)
#endif

// ============================================================================
memory_controller_t::memory_controller_t() {
    this->set_type_component(COMPONENT_MEMORY_CONTROLLER);

    this->address_mask_type = MEMORY_CONTROLLER_MASK_ROW_BANK_COLROW_CHANNEL_COLBYTE;
    this->line_size = 0;

    this->controller_number = 0;
    this->total_controllers = 0;
    this->mshr_buffer_size = 0;
    this->channels_per_controller = 0;
    this->bank_per_channel = 0;
    this->bank_buffer_size = 0;
    this->bank_row_buffer_size = 0;
    this->bank_selection_policy = SELECTION_ROUND_ROBIN;

    this->request_priority_policy = REQUEST_PRIORITY_ROW_BUFFER_HITS_FIRST;
    this->write_priority_policy = WRITE_PRIORITY_DRAIN_WHEN_FULL;

    this->send_ready_cycle = NULL;
    this->recv_ready_cycle = NULL;

    this->channels = NULL;
    this->mshr_buffer = NULL;

    this->burst_length = 0;
    this->core_to_bus_clock_ratio = 0;

    this->timing_burst = 0;
    this->timing_al = 0;
    this->timing_cas = 0;
    this->timing_ccd = 0;
    this->timing_cwd = 0;
    this->timing_faw = 0;
    this->timing_ras = 0;
    this->timing_rc = 0;
    this->timing_rcd = 0;
    this->timing_rp = 0;
    this->timing_rrd = 0;
    this->timing_rtp = 0;
    this->timing_wr = 0;
    this->timing_wtr = 0;

    this->mshr_request_buffer_size = 0;
    this->mshr_prefetch_buffer_size = 0;
    this->mshr_write_buffer_size = 0;

    this->mshr_tokens_request = NULL;
    this->mshr_tokens_write = NULL;
    this->mshr_tokens_prefetch = NULL;

    // hmc
    this->hmc_size = 0;
    this->hmc_latency_roa = 0;
    this->hmc_latency_rowa = 0;

    // HIVE
    this->hive_operation_size = 0;
    this->hive_total_registers = 1;
    this->hive_ready_cycle = 0;

    this->hive_latency_int_alu = 0;
    this->hive_latency_int_mul = 0;
    this->hive_latency_int_div = 0;
	  
    this->hive_latency_fp_alu  = 0;
    this->hive_latency_fp_mul  = 0;
    this->hive_latency_fp_div  = 0;
	
	//CIM
	this->cim_latency_btw_or = 0;
	this->cim_latency_btw_and = 0;
	this->cim_latency_btw_xor = 0;
	this->cim_latency_eq_cmp = 0;
	this->cim_latency_adr_ld = 0;
	this->cim_latency_mem_ld = 0;
	this->cim_latency_mem_st = 0;

    this->hive_state = HIVE_STATE_UNLOCK;
    this->hive_id_owner = 0;
    this->hive_number = 0;

    this->hive_nano_buffer = NULL;
    this->hive_nano_buffer_size = 0;
    this->hive_nano_buffer_used = NULL;
    this->hive_nano_buffer_used_total = 0;
};

// ============================================================================
memory_controller_t::~memory_controller_t() {
    // De-Allocate memory to prevent memory leak
    utils_t::template_delete_array<uint64_t>(send_ready_cycle);
    utils_t::template_delete_array<uint64_t>(recv_ready_cycle);

    utils_t::template_delete_array<memory_channel_t>(channels);
    utils_t::template_delete_array<memory_package_t>(mshr_buffer);

    utils_t::template_delete_array<int32_t>(mshr_tokens_request);
    utils_t::template_delete_array<int32_t>(mshr_tokens_prefetch);
    utils_t::template_delete_array<int32_t>(mshr_tokens_write);

    // HIVE
    utils_t::template_delete_array<uint32_t>(hive_wait_registers);
    utils_t::template_delete_matrix<memory_package_t>(hive_nano_buffer, this->hive_nano_buffer_size);
    utils_t::template_delete_array<uint32_t>(hive_nano_buffer_used);
};

// ============================================================================
void memory_controller_t::allocate() {

    ERROR_ASSERT_PRINTF(mshr_request_buffer_size > 0, "mshr_request_buffer_size should be bigger than zero.\n");
    ERROR_ASSERT_PRINTF(mshr_prefetch_buffer_size > 0, "mshr_prefetch_buffer_size should be bigger than zero.\n");
    ERROR_ASSERT_PRINTF(mshr_write_buffer_size > 0, "mshr_write_buffer_size should be bigger than zero.\n");

    this->send_ready_cycle = utils_t::template_allocate_initialize_array<uint64_t>(this->get_max_ports(), 0);
    this->recv_ready_cycle = utils_t::template_allocate_initialize_array<uint64_t>(this->get_max_ports(), 0);

    /// MSHR = [    REQUEST    | PREFETCH | WRITE | EVICT ]
    this->mshr_buffer_size = this->mshr_request_buffer_size +
                                this->mshr_prefetch_buffer_size +
                                this->mshr_write_buffer_size;
    this->mshr_buffer = utils_t::template_allocate_array<memory_package_t>(this->get_mshr_buffer_size());
    this->mshr_born_ordered.reserve(this->mshr_buffer_size);

    this->mshr_tokens_request = utils_t::template_allocate_initialize_array<int32_t>(sinuca_engine.get_interconnection_interface_array_size(), -1);
    this->mshr_tokens_prefetch = utils_t::template_allocate_initialize_array<int32_t>(sinuca_engine.get_interconnection_interface_array_size(), -1);
    this->mshr_tokens_write = utils_t::template_allocate_initialize_array<int32_t>(sinuca_engine.get_interconnection_interface_array_size(), -1);

    this->set_masks();

    this->timing_burst = ceil((double)this->line_size / this->get_burst_length());

    this->channels = utils_t::template_allocate_array<memory_channel_t>(this->get_channels_per_controller());
    for (uint32_t i = 0; i < this->get_channels_per_controller(); i++) {
        char label[50] = "";
        sprintf(label, "%s_MEMORY_CHANNEL_%d", this->get_label(), i);
        this->channels[i].set_label(label);

        this->channels[i].set_memory_controller_id(this->get_id());

        this->channels[i].bank_per_channel = this->bank_per_channel;
        this->channels[i].bank_selection_policy = this->bank_selection_policy;
        this->channels[i].bank_buffer_size = this->bank_buffer_size;

        this->channels[i].page_policy = this->page_policy;

        this->channels[i].request_priority_policy = this->request_priority_policy;
        this->channels[i].write_priority_policy = this->write_priority_policy;

        /// Consider the latency in terms of processor cycles
        this->channels[i].timing_burst  = ceil((double)this->timing_burst   * this->core_to_bus_clock_ratio);
        this->channels[i].timing_al     = ceil((double)this->timing_al      * this->core_to_bus_clock_ratio);
        this->channels[i].timing_cas    = ceil((double)this->timing_cas     * this->core_to_bus_clock_ratio);
        this->channels[i].timing_ccd    = ceil((double)this->timing_ccd     * this->core_to_bus_clock_ratio);
        this->channels[i].timing_cwd    = ceil((double)this->timing_cwd     * this->core_to_bus_clock_ratio);
        this->channels[i].timing_faw    = ceil((double)this->timing_faw     * this->core_to_bus_clock_ratio);
        this->channels[i].timing_ras    = ceil((double)this->timing_ras     * this->core_to_bus_clock_ratio);
        this->channels[i].timing_rc     = ceil((double)this->timing_rc      * this->core_to_bus_clock_ratio);
        this->channels[i].timing_rcd    = ceil((double)this->timing_rcd     * this->core_to_bus_clock_ratio);
        this->channels[i].timing_rp     = ceil((double)this->timing_rp      * this->core_to_bus_clock_ratio);
        this->channels[i].timing_rrd    = ceil((double)this->timing_rrd     * this->core_to_bus_clock_ratio);
        this->channels[i].timing_rtp    = ceil((double)this->timing_rtp     * this->core_to_bus_clock_ratio);
        this->channels[i].timing_wr     = ceil((double)this->timing_wr      * this->core_to_bus_clock_ratio);
        this->channels[i].timing_wtr    = ceil((double)this->timing_wtr     * this->core_to_bus_clock_ratio);

        // HMC
        this->channels[i].hmc_size  = this->hmc_size ; // ceil(this->hmc_latency_roa  * this->core_to_bus_clock_ratio);
        this->channels[i].hmc_timing_burst = ceil( ((double)this->hmc_size / this->get_burst_length()) * this->core_to_bus_clock_ratio); // Transmission time using TSVs;

        this->channels[i].hmc_latency_roa  = this->hmc_latency_roa ; // ceil((double)this->hmc_latency_roa  * this->core_to_bus_clock_ratio);
        this->channels[i].hmc_latency_rowa = this->hmc_latency_rowa; // ceil((double)this->hmc_latency_rowa * this->core_to_bus_clock_ratio);

        // HIVE
        // ~ this->hive_latency_int_alu = ceil((double)this->hive_latency_int_alu * this->core_to_bus_clock_ratio);
        // ~ this->hive_latency_int_mul = ceil((double)this->hive_latency_int_mul * this->core_to_bus_clock_ratio);
        // ~ this->hive_latency_int_div = ceil((double)this->hive_latency_int_div * this->core_to_bus_clock_ratio);
        // ~ this->hive_latency_fp_alu  = ceil((double)this->hive_latency_fp_alu  * this->core_to_bus_clock_ratio);
        // ~ this->hive_latency_fp_mul  = ceil((double)this->hive_latency_fp_mul  * this->core_to_bus_clock_ratio);
        // ~ this->hive_latency_fp_div  = ceil((double)this->hive_latency_fp_div  * this->core_to_bus_clock_ratio);

        /// Copy the masks
        this->channels[i].not_column_bits_mask = this->not_column_bits_mask;

        this->channels[i].bank_bits_mask = this->bank_bits_mask;
        this->channels[i].bank_bits_shift = this->bank_bits_shift;

        /// Call the channel allocate()
        this->channels[i].allocate();
    }

    // HIVE
    MEMORY_CONTROLLER_DEBUG_PRINTF("total registers %d\n",this->get_hive_total_registers() );
    this->hive_wait_registers = utils_t::template_allocate_initialize_array<uint32_t>(this->get_hive_total_registers(), 0);
    if (this->get_hive_operation_size() == 0) {
        this->hive_nano_buffer_size = sinuca_engine.get_global_line_size();
    }
    else {
        this->hive_nano_buffer_size = ceil((double)this->get_hive_operation_size() / sinuca_engine.get_global_line_size());
    }
    MEMORY_CONTROLLER_DEBUG_PRINTF("nano_buffer_size %d\n",hive_nano_buffer_size );
    this->hive_nano_buffer = utils_t::template_allocate_matrix<memory_package_t>(this->bank_buffer_size * this->channels_per_controller ,this->hive_nano_buffer_size);
    this->hive_nano_buffer_used = utils_t::template_allocate_array<uint32_t>(this->bank_buffer_size * this->channels_per_controller);

    #ifdef MEMORY_CONTROLLER_DEBUG
        this->print_configuration();
    #endif
};

// ============================================================================
void memory_controller_t::set_tokens() {

    /// Find all LLC
    uint32_t higher_components = 0;
    for (uint32_t i = 0; i < sinuca_engine.get_cache_memory_array_size(); i++) {
        cache_memory_t *cache_memory = sinuca_engine.cache_memory_array[i];
        container_ptr_cache_memory_t *lower_level_cache = cache_memory->get_lower_level_cache();

        /// Found LLC
        if (lower_level_cache->empty()) {
            higher_components++;
            uint32_t id = cache_memory->get_id();

            this->mshr_tokens_request[id]   = this->higher_level_request_tokens;
            this->mshr_tokens_prefetch[id]  = this->higher_level_prefetch_tokens;
            this->mshr_tokens_write[id]     = this->higher_level_write_tokens;
        }
    }

    ERROR_ASSERT_PRINTF(this->higher_level_request_tokens * higher_components <= this->mshr_request_buffer_size,
                        "%s Allocating more REQUEST tokens than MSHR positions.\n", this->get_label())

    ERROR_ASSERT_PRINTF(this->higher_level_prefetch_tokens * higher_components <= this->mshr_prefetch_buffer_size,
                        "%s Allocating more PREFETCH tokens than MSHR positions.\n", this->get_label())

    ERROR_ASSERT_PRINTF(this->higher_level_write_tokens * higher_components <= this->mshr_write_buffer_size,
                        "%s Allocating more WRITE tokens than MSHR positions.\n", this->get_label())
};

// ============================================================================
void memory_controller_t::set_masks() {
    uint64_t i;

    ERROR_ASSERT_PRINTF(this->get_total_controllers() > this->get_controller_number(),
                        "Wrong number of memory_controllers (%u/%u).\n", this->get_controller_number(), this->get_total_controllers());
    ERROR_ASSERT_PRINTF(this->get_channels_per_controller() > 0,
                        "Wrong number of memory_channels (%u).\n", this->get_channels_per_controller());
    ERROR_ASSERT_PRINTF(this->get_bank_per_channel() > 0,
                        "Wrong number of memory_banks (%u).\n", this->get_bank_per_channel());

    this->controller_bits_mask = 0;
    this->colrow_bits_mask = 0;
    this->colbyte_bits_mask = 0;
    this->channel_bits_mask = 0;
    this->bank_bits_mask = 0;
    this->row_bits_mask = 0;


    switch (this->get_address_mask_type()) {
        case MEMORY_CONTROLLER_MASK_ROW_BANK_COLROW_COLBYTE:
            ERROR_ASSERT_PRINTF(this->get_total_controllers() == 1, "Wrong number of memory_controllers (%u).\n", this->get_total_controllers());
            ERROR_ASSERT_PRINTF(this->get_channels_per_controller() == 1, "Wrong number of memory_channels (%u).\n", this->get_channels_per_controller());

            this->controller_bits_shift = 0;
            this->channel_bits_shift = 0;

            this->colbyte_bits_shift = 0;
            this->colrow_bits_shift = utils_t::get_power_of_two(this->get_line_size());
            this->bank_bits_shift = utils_t::get_power_of_two(this->get_bank_row_buffer_size());
            this->row_bits_shift = bank_bits_shift + utils_t::get_power_of_two(this->get_bank_per_channel());

            /// COLBYTE MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_line_size()); i++) {
                this->colbyte_bits_mask |= 1 << (i + this->colbyte_bits_shift);
            }

            /// COLROW MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_bank_row_buffer_size()) - this->colrow_bits_shift; i++) {
                this->colrow_bits_mask |= 1 << (i + this->colrow_bits_shift);
            }

            this->not_column_bits_mask = ~(colbyte_bits_mask | colrow_bits_mask);

            /// BANK MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_bank_per_channel()); i++) {
                this->bank_bits_mask |= 1 << (i + bank_bits_shift);
            }

            /// ROW MASK
            for (i = row_bits_shift; i < utils_t::get_power_of_two((uint64_t)INT64_MAX+1); i++) {
                this->row_bits_mask |= 1 << i;
            }
        break;

        case MEMORY_CONTROLLER_MASK_ROW_BANK_CHANNEL_COLROW_COLBYTE:
            ERROR_ASSERT_PRINTF(this->get_total_controllers() == 1,
                                "Wrong number of memory_controllers (%u).\n", this->get_total_controllers());
            ERROR_ASSERT_PRINTF(this->get_channels_per_controller() > 1 &&
                                utils_t::check_if_power_of_two(this->get_channels_per_controller()),
                                "Wrong number of memory_channels (%u).\n", this->get_channels_per_controller());

            this->controller_bits_shift = 0;
            this->colbyte_bits_shift = 0;
            this->colrow_bits_shift = utils_t::get_power_of_two(this->get_line_size());
            this->channel_bits_shift = utils_t::get_power_of_two(this->get_bank_row_buffer_size());
            this->bank_bits_shift = this->channel_bits_shift + utils_t::get_power_of_two(this->get_channels_per_controller());
            this->row_bits_shift = this->bank_bits_shift + utils_t::get_power_of_two(this->get_bank_per_channel());

            /// COLBYTE MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_line_size()); i++) {
                this->colbyte_bits_mask |= 1 << (i + this->colbyte_bits_shift);
            }

            /// COLROW MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_bank_row_buffer_size() / this->get_line_size()); i++) {
                this->colrow_bits_mask |= 1 << (i + this->colrow_bits_shift);
            }

            this->not_column_bits_mask = ~(colbyte_bits_mask | colrow_bits_mask);

            /// CHANNEL MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_channels_per_controller()); i++) {
                this->channel_bits_mask |= 1 << (i + channel_bits_shift);
            }

            /// BANK MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_bank_per_channel()); i++) {
                this->bank_bits_mask |= 1 << (i + bank_bits_shift);
            }

            /// ROW MASK
            for (i = row_bits_shift; i < utils_t::get_power_of_two((uint64_t)INT64_MAX+1); i++) {
                this->row_bits_mask |= 1 << i;
            }
        break;


        case MEMORY_CONTROLLER_MASK_ROW_BANK_CHANNEL_CTRL_COLROW_COLBYTE:
            ERROR_ASSERT_PRINTF(this->get_total_controllers() > 1 &&
                                utils_t::check_if_power_of_two(this->get_total_controllers()),
                                "Wrong number of memory_controllers (%u).\n", this->get_total_controllers());
            ERROR_ASSERT_PRINTF(this->get_channels_per_controller() > 1 &&
                                utils_t::check_if_power_of_two(this->get_channels_per_controller()),
                                "Wrong number of memory_channels (%u).\n", this->get_channels_per_controller());

            this->colbyte_bits_shift = 0;
            this->colrow_bits_shift = utils_t::get_power_of_two(this->get_line_size());
            this->controller_bits_shift = utils_t::get_power_of_two(this->get_bank_row_buffer_size());
            this->channel_bits_shift = this->controller_bits_shift + utils_t::get_power_of_two(this->get_total_controllers());
            this->bank_bits_shift = this->channel_bits_shift + utils_t::get_power_of_two(this->get_channels_per_controller());
            this->row_bits_shift = this->bank_bits_shift + utils_t::get_power_of_two(this->get_bank_per_channel());

            /// COLBYTE MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_line_size()); i++) {
                this->colbyte_bits_mask |= 1 << (i + this->colbyte_bits_shift);
            }

            /// COLROW MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_bank_row_buffer_size()) - this->colrow_bits_shift; i++) {
                this->colrow_bits_mask |= 1 << (i + this->colrow_bits_shift);
            }

            this->not_column_bits_mask = ~(colbyte_bits_mask | colrow_bits_mask);

            /// CONTROLLER MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_total_controllers()); i++) {
                this->controller_bits_mask |= 1 << (i + controller_bits_shift);
            }

            /// CHANNEL MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_channels_per_controller()); i++) {
                this->channel_bits_mask |= 1 << (i + channel_bits_shift);
            }

            /// BANK MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_bank_per_channel()); i++) {
                this->bank_bits_mask |= 1 << (i + bank_bits_shift);
            }

            /// ROW MASK
            for (i = row_bits_shift; i < utils_t::get_power_of_two((uint64_t)INT64_MAX+1); i++) {
                this->row_bits_mask |= 1 << i;
            }
        break;

        case MEMORY_CONTROLLER_MASK_ROW_BANK_COLROW_CHANNEL_COLBYTE:
            ERROR_ASSERT_PRINTF(this->get_total_controllers() == 1,
                                "Wrong number of memory_controllers (%u).\n", this->get_total_controllers());
            ERROR_ASSERT_PRINTF(this->get_channels_per_controller() > 1 &&
                                utils_t::check_if_power_of_two(this->get_channels_per_controller()),
                                "Wrong number of memory_channels (%u).\n", this->get_channels_per_controller());

            this->controller_bits_shift = 0;
            this->colbyte_bits_shift = 0;
            this->channel_bits_shift = utils_t::get_power_of_two(this->get_line_size());
            this->colrow_bits_shift = this->channel_bits_shift + utils_t::get_power_of_two(this->get_channels_per_controller());
            this->bank_bits_shift = this->colrow_bits_shift + utils_t::get_power_of_two(this->get_bank_row_buffer_size() / this->get_line_size());
            this->row_bits_shift = this->bank_bits_shift + utils_t::get_power_of_two(this->get_bank_per_channel());

            /// COLBYTE MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_line_size()); i++) {
                this->colbyte_bits_mask |= 1 << (i + this->colbyte_bits_shift);
            }

            /// CHANNEL MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_channels_per_controller()); i++) {
                this->channel_bits_mask |= 1 << (i + channel_bits_shift);
            }

            /// COLROW MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_bank_row_buffer_size() / this->get_line_size()); i++) {
                this->colrow_bits_mask |= 1 << (i + this->colrow_bits_shift);
            }

            this->not_column_bits_mask = ~(colbyte_bits_mask | colrow_bits_mask);


            /// BANK MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_bank_per_channel()); i++) {
                this->bank_bits_mask |= 1 << (i + bank_bits_shift);
            }

            /// ROW MASK
            for (i = row_bits_shift; i < utils_t::get_power_of_two((uint64_t)INT64_MAX+1); i++) {
                this->row_bits_mask |= 1 << i;
            }
        break;


        case MEMORY_CONTROLLER_MASK_ROW_CTRL_BANK_COLROW_COLBYTE:
            ERROR_ASSERT_PRINTF(this->get_channels_per_controller() == 1,
                                "Wrong number of channels_per_controller (%u).\n", this->get_channels_per_controller());
            ERROR_ASSERT_PRINTF(this->get_total_controllers() > 1 &&
                                utils_t::check_if_power_of_two(this->get_total_controllers()),
                                "Wrong number of memory_channels (%u).\n", this->get_total_controllers());

            this->channel_bits_shift = 0;
            this->colbyte_bits_shift = 0;
            this->colrow_bits_shift = utils_t::get_power_of_two(this->get_line_size());
            this->bank_bits_shift =  this->colrow_bits_shift + utils_t::get_power_of_two(this->get_bank_row_buffer_size() / this->get_line_size());
            this->controller_bits_shift = this->bank_bits_shift + utils_t::get_power_of_two(this->get_bank_per_channel());
            this->row_bits_shift = this->controller_bits_shift + utils_t::get_power_of_two(this->get_total_controllers());

            /// COLBYTE MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_line_size()); i++) {
                this->colbyte_bits_mask |= 1 << (i + this->colbyte_bits_shift);
            }

            /// COLROW MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_bank_row_buffer_size() / this->get_line_size()); i++) {
                this->colrow_bits_mask |= 1 << (i + this->colrow_bits_shift);
            }
            this->not_column_bits_mask = ~(colbyte_bits_mask | colrow_bits_mask);

            /// BANK MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_bank_per_channel()); i++) {
                this->bank_bits_mask |= 1 << (i + bank_bits_shift);
            }

            /// CONTROLLER MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_total_controllers()); i++) {
                this->controller_bits_mask |= 1 << (i + controller_bits_shift);
            }

            /// ROW MASK
            for (i = row_bits_shift; i < utils_t::get_power_of_two((uint64_t)INT64_MAX+1); i++) {
                this->row_bits_mask |= 1 << i;
            }
        break;



        case MEMORY_CONTROLLER_MASK_ROW_BANK_COLROW_CTRL_CHANNEL_COLBYTE:
            ERROR_ASSERT_PRINTF(this->get_total_controllers() > 1 &&
                                utils_t::check_if_power_of_two(this->get_total_controllers()),
                                "Wrong number of memory_controllers (%u).\n", this->get_total_controllers());
            ERROR_ASSERT_PRINTF(this->get_channels_per_controller() > 1 &&
                                utils_t::check_if_power_of_two(this->get_channels_per_controller()),
                                "Wrong number of memory_channels (%u).\n", this->get_channels_per_controller());

            this->colbyte_bits_shift = 0;
            this->channel_bits_shift = utils_t::get_power_of_two(this->get_line_size());
            this->controller_bits_shift = this->channel_bits_shift + utils_t::get_power_of_two(this->get_channels_per_controller());
            this->colrow_bits_shift = this->controller_bits_shift + utils_t::get_power_of_two(this->get_total_controllers());
            this->bank_bits_shift = this->colrow_bits_shift + utils_t::get_power_of_two(this->get_bank_row_buffer_size() / this->get_line_size());
            this->row_bits_shift = this->bank_bits_shift + utils_t::get_power_of_two(this->get_bank_per_channel());

            /// COLBYTE MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_line_size()); i++) {
                this->colbyte_bits_mask |= 1 << (i + this->colbyte_bits_shift);
            }

            /// CHANNEL MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_channels_per_controller()); i++) {
                this->channel_bits_mask |= 1 << (i + channel_bits_shift);
            }

            /// CONTROLLER MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_total_controllers()); i++) {
                this->controller_bits_mask |= 1 << (i + controller_bits_shift);
            }


            /// COLROW MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_bank_row_buffer_size() / this->get_line_size()); i++) {
                this->colrow_bits_mask |= 1 << (i + this->colrow_bits_shift);
            }

            this->not_column_bits_mask = ~(colbyte_bits_mask | colrow_bits_mask);


            /// BANK MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_bank_per_channel()); i++) {
                this->bank_bits_mask |= 1 << (i + bank_bits_shift);
            }

            /// ROW MASK
            for (i = row_bits_shift; i < utils_t::get_power_of_two((uint64_t)INT64_MAX+1); i++) {
                this->row_bits_mask |= 1 << i;
            }
        break;


        case MEMORY_CONTROLLER_MASK_ROW_COLROW_BANK_CHANNEL_COLBYTE:
            ERROR_ASSERT_PRINTF(this->get_total_controllers() == 1,
                                "Wrong number of memory_controllers (%u).\n", this->get_total_controllers());
            ERROR_ASSERT_PRINTF(this->get_channels_per_controller() > 1 &&
                                utils_t::check_if_power_of_two(this->get_channels_per_controller()),
                                "Wrong number of memory_channels (%u).\n", this->get_channels_per_controller());

            this->controller_bits_shift = 0;
            this->colbyte_bits_shift = 0;
            this->channel_bits_shift = utils_t::get_power_of_two(this->get_line_size());
            this->bank_bits_shift    = this->channel_bits_shift + utils_t::get_power_of_two(this->get_channels_per_controller());
            this->colrow_bits_shift  = this->bank_bits_shift + utils_t::get_power_of_two(this->get_bank_per_channel());
            this->row_bits_shift     = this->colrow_bits_shift + utils_t::get_power_of_two(this->get_bank_row_buffer_size() / this->get_line_size());

            /// COLBYTE MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_line_size()); i++) {
                this->colbyte_bits_mask |= 1 << (i + this->colbyte_bits_shift);
            }

            /// CHANNEL MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_channels_per_controller()); i++) {
                this->channel_bits_mask |= 1 << (i + channel_bits_shift);
            }

            /// BANK MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_bank_per_channel()); i++) {
                this->bank_bits_mask |= 1 << (i + bank_bits_shift);
            }

            /// COLROW MASK
            for (i = 0; i < utils_t::get_power_of_two(this->get_bank_row_buffer_size() / this->get_line_size()); i++) {
                this->colrow_bits_mask |= 1 << (i + this->colrow_bits_shift);
            }

            this->not_column_bits_mask = ~(colbyte_bits_mask | colrow_bits_mask);


            /// ROW MASK
            for (i = row_bits_shift; i < utils_t::get_power_of_two((uint64_t)INT64_MAX+1); i++) {
                this->row_bits_mask |= 1 << i;
            }
        break;
    }

    MEMORY_CONTROLLER_DEBUG_PRINTF("not col %s\n", utils_t::address_to_binary(this->not_column_bits_mask).c_str());
    MEMORY_CONTROLLER_DEBUG_PRINTF("colbyte %s\n", utils_t::address_to_binary(this->colbyte_bits_mask).c_str());
    MEMORY_CONTROLLER_DEBUG_PRINTF("colrow  %s\n", utils_t::address_to_binary(this->colrow_bits_mask).c_str());
    MEMORY_CONTROLLER_DEBUG_PRINTF("bank    %s\n", utils_t::address_to_binary(this->bank_bits_mask).c_str());
    MEMORY_CONTROLLER_DEBUG_PRINTF("channel %s\n", utils_t::address_to_binary(this->channel_bits_mask).c_str());
    MEMORY_CONTROLLER_DEBUG_PRINTF("row     %s\n", utils_t::address_to_binary(this->row_bits_mask).c_str());
    MEMORY_CONTROLLER_DEBUG_PRINTF("ctrl    %s\n", utils_t::address_to_binary(this->controller_bits_mask).c_str());
};

// HIVE
// ============================================================================
uint32_t memory_controller_t::get_hive_latency(memory_operation_t operation) {
    switch(operation) {
        case MEMORY_OPERATION_HIVE_LOCK:     return 0; break;
        case MEMORY_OPERATION_HIVE_UNLOCK:   return 0; break;

        case MEMORY_OPERATION_HIVE_LOAD:     return 0; break;
        case MEMORY_OPERATION_HIVE_STORE:    return 0; break;

        case MEMORY_OPERATION_HIVE_NANO_LOAD:     return 0; break;
        case MEMORY_OPERATION_HIVE_NANO_STORE:    return 0; break;
        // ====================================================================
        case MEMORY_OPERATION_HIVE_INT_ALU:  return this->get_hive_latency_int_alu(); break;
        case MEMORY_OPERATION_HIVE_INT_MUL:  return this->get_hive_latency_int_mul(); break;
        case MEMORY_OPERATION_HIVE_INT_DIV:  return this->get_hive_latency_int_div(); break;
		    

        case MEMORY_OPERATION_HIVE_FP_ALU:   return this->get_hive_latency_fp_alu(); break;
        case MEMORY_OPERATION_HIVE_FP_MUL:   return this->get_hive_latency_fp_mul(); break;
        case MEMORY_OPERATION_HIVE_FP_DIV:   return this->get_hive_latency_fp_div(); break;
		
		//CIM
		case MEMORY_OPERATION_CIM_BTW_OR:  return this->get_cim_latency_btw_or(); break;
		case MEMORY_OPERATION_CIM_BTW_AND:  return this->get_cim_latency_btw_and(); break;
		case MEMORY_OPERATION_CIM_BTW_XOR:  return this->get_cim_latency_btw_xor(); break;
		case MEMORY_OPERATION_CIM_EQ_CMP:  return this->get_cim_latency_eq_cmp(); break;
		case MEMORY_OPERATION_CIM_ADR_LD:  return this->get_cim_latency_adr_ld(); break;
		case MEMORY_OPERATION_CIM_MEM_LD:  return this->get_cim_latency_mem_ld(); break;
		case MEMORY_OPERATION_CIM_MEM_ST:  return this->get_cim_latency_mem_st(); break;

        default: ERROR_PRINTF("Wrong operation type"); break;

    }
    return 0;
};


// ============================================================================
int32_t memory_controller_t::find_next_hive_operation() {

    int32_t slot = POSITION_FAIL;
    uint32_t i;

    // No HIVE
    if (this->hive_buffer.empty()) {
        return slot;
    }

    // HIVE is already running
    if (this->hive_state == HIVE_STATE_LOCK) {
        for (i = 0; i < this->hive_buffer.size(); i++) {
            if (this->hive_buffer[i]->id_owner == this->hive_id_owner &&
                this->hive_buffer[i]->hive_number == this->hive_number + 1) {
                slot = i;
                break;
            }
        }
        return slot;
    }

    // HIVE - Start a new HIVE operation
    for (i = 0; i < this->hive_buffer.size(); i++) {
        if (this->hive_buffer[i]->memory_operation == MEMORY_OPERATION_HIVE_LOCK) {
            slot = i;
            break;
        }
    }
    return slot;
};


// ============================================================================
void memory_controller_t::clock(uint32_t subcycle) {
    if (subcycle != 0) return;
    MEMORY_CONTROLLER_DEBUG_PRINTF("==================== ID(%u) ", this->get_id());
    MEMORY_CONTROLLER_DEBUG_PRINTF("====================\n");
    MEMORY_CONTROLLER_DEBUG_PRINTF("cycle() \n");


    // =================================================================
    /// MSHR_BUFFER - READY
    // =================================================================
    for (uint32_t i = 0; i < this->mshr_born_ordered.size(); i++){
        if (mshr_born_ordered[i]->state == PACKAGE_STATE_READY &&
        this->mshr_born_ordered[i]->ready_cycle <= sinuca_engine.get_global_cycle()) {
            /// PACKAGE READY !
            this->memory_stats(this->mshr_born_ordered[i]);
            this->push_token_credit(this->mshr_born_ordered[i]->id_dst, this->mshr_born_ordered[i]->memory_operation);
            this->mshr_born_ordered[i]->package_clean();
            this->mshr_born_ordered.erase(this->mshr_born_ordered.begin() + i);
        }
    }

    // HIVE - NANO LOAD/STORE
    for (uint32_t i = 0; i < this->bank_buffer_size * this->channels_per_controller; i++){
        if (this->hive_nano_buffer_used[i] == 0) {
            continue;
        }
        for (uint32_t j = 0; j < this->hive_nano_buffer_size; j++){
            if (hive_nano_buffer[i][j].state == PACKAGE_STATE_READY &&
            this->hive_nano_buffer[i][j].ready_cycle <= sinuca_engine.get_global_cycle()) {
                if (this->hive_nano_buffer[i][j].memory_operation == MEMORY_OPERATION_HIVE_NANO_LOAD) {
                    MEMORY_CONTROLLER_DEBUG_PRINTF("Solving HIVE_NANO_LOAD %s\n", this->hive_nano_buffer[i][j].content_to_string().c_str());
                    this->add_stat_hive_nano_load_completed(this->hive_nano_buffer[i][j].born_cycle);
                    ERROR_ASSERT_PRINTF(hive_nano_buffer[i][j].hive_write != -1, "Receiving an NANO answer that does not write a register");
                    this->hive_wait_registers[hive_nano_buffer[i][j].hive_write]--;
                    ERROR_ASSERT_PRINTF(this->hive_nano_buffer_used[i] > 0, "Decreasing the used nano buffer below zero.\n")
                    this->hive_nano_buffer_used[i]--;
                }
                else if (this->hive_nano_buffer[i][j].memory_operation == MEMORY_OPERATION_HIVE_NANO_STORE) {
                    MEMORY_CONTROLLER_DEBUG_PRINTF("Solving HIVE_NANO_STORE %s\n", this->hive_nano_buffer[i][j].content_to_string().c_str());
                    this->add_stat_hive_nano_store_completed(this->hive_nano_buffer[i][j].born_cycle);
                    ERROR_ASSERT_PRINTF(this->hive_nano_buffer_used[i] > 0, "Decreasing the used nano buffer below zero.\n")
                    this->hive_nano_buffer_used[i]--;
                }
                this->hive_nano_buffer[i][j].package_clean();
            }
        }
        // One more free line
        if (this->hive_nano_buffer_used[i] == 0) {
            ERROR_ASSERT_PRINTF(this->hive_nano_buffer_used_total > 0, "Decreasing the used nano buffer below zero.\n")
            hive_nano_buffer_used_total--;
        }
    }

    // =================================================================
    // HIVE_NANO_BUFFER - UNTREATED (Local / Remote)
    // =================================================================
    for (uint32_t i = 0; i < this->bank_buffer_size * this->channels_per_controller; i++){
        if (this->hive_nano_buffer_used[i] == 0) {
            continue;
        }
        for (uint32_t j = 0; j < this->hive_nano_buffer_size; j++){
            if (hive_nano_buffer[i][j].state == PACKAGE_STATE_UNTREATED &&
            this->hive_nano_buffer[i][j].ready_cycle <= sinuca_engine.get_global_cycle()) {
                uint32_t controller = this->get_controller(this->hive_nano_buffer[i][j].memory_address);
                uint32_t channel = this->get_channel(this->hive_nano_buffer[i][j].memory_address);
                hive_nano_buffer[i][j].id_owner = this->get_id();

                // LOCAL REQUESTS
                if (controller == this->controller_number) {
                    MEMORY_CONTROLLER_DEBUG_PRINTF("Treating Local HIVE_NANO_LOAD %s\n", this->hive_nano_buffer[i][j].content_to_string().c_str());
                    ERROR_ASSERT_PRINTF(this->hive_nano_buffer[i][j].is_answer == false, "Packages being treated should not be answer.")

                    this->hive_nano_buffer[i][j].state = this->channels[channel].treat_memory_request(&this->hive_nano_buffer[i][j]);
                    ERROR_ASSERT_PRINTF(this->hive_nano_buffer[i][j].state != PACKAGE_STATE_FREE, "Must not receive back a FREE, should receive READY/TRANSMIT + Latency")
                }
                // REMOTE REQUESTS
                else {
                    /// Prepare for answer later
                    MEMORY_CONTROLLER_DEBUG_PRINTF("Treating Remote HIVE_NANO_LOAD %s\n", this->hive_nano_buffer[i][j].content_to_string().c_str());
                    this->hive_nano_buffer[i][j].package_set_src_dst(this->get_id(), sinuca_engine.memory_controller_array[controller]->get_id());
                    MEMORY_CONTROLLER_DEBUG_PRINTF("\t Send RQST this->hive_nano_buffer[%d] %s\n", i, this->hive_nano_buffer[i][j].content_to_string().c_str());
                    int32_t transmission_latency = send_package(&hive_nano_buffer[i][j]);
                    if (transmission_latency != POSITION_FAIL) {
                        this->hive_nano_buffer[i][j].package_clean();
                        this->hive_nano_buffer_used[i]--;
                    }
                }
            }
        }
        // One more free line (in case of remote accesses)
        if (this->hive_nano_buffer_used[i] == 0) {
            ERROR_ASSERT_PRINTF(this->hive_nano_buffer_used_total > 0, "Decreasing the used nano buffer below zero.\n")
            hive_nano_buffer_used_total--;
        }
    }

    // =================================================================
    // HIVE_BUFFER - TRANSMISSION
    // =================================================================
    for (uint32_t i = 0; i < this->hive_buffer.size(); i++){
        if (hive_buffer[i]->state == PACKAGE_STATE_TRANSMIT &&
        this->hive_buffer[i]->ready_cycle <= sinuca_engine.get_global_cycle()) {

            ERROR_ASSERT_PRINTF(this->hive_buffer[i]->is_answer == true, "Packages being transmited should be answer.")
            MEMORY_CONTROLLER_DEBUG_PRINTF("\t Send ANSWER this->hive_buffer[%d] %s\n", i, this->hive_buffer[i]->content_to_string().c_str());
            int32_t transmission_latency = send_package(hive_buffer[i]);
            if (transmission_latency != POSITION_FAIL) {
                /// PACKAGE READY !
                memory_stats(this->hive_buffer[i]);
                this->push_token_credit(this->hive_buffer[i]->id_dst, this->hive_buffer[i]->memory_operation);
                this->hive_buffer[i]->package_clean();
                this->hive_buffer.erase(this->hive_buffer.begin() + i);
            }
            break;
        }
    }

    // =================================================================
    /// MSHR_BUFFER - TRANSMISSION
    // =================================================================
    for (uint32_t i = 0; i < this->mshr_born_ordered.size(); i++){
        if (mshr_born_ordered[i]->state == PACKAGE_STATE_TRANSMIT &&
        this->mshr_born_ordered[i]->ready_cycle <= sinuca_engine.get_global_cycle()) {

            ERROR_ASSERT_PRINTF(this->mshr_born_ordered[i]->is_answer == true, "Packages being transmited should be answer.")
            MEMORY_CONTROLLER_DEBUG_PRINTF("\t Send ANSWER this->mshr_born_ordered[%d] %s\n", i, this->mshr_born_ordered[i]->content_to_string().c_str());
            int32_t transmission_latency = send_package(mshr_born_ordered[i]);
            if (transmission_latency != POSITION_FAIL) {
                /// PACKAGE READY !
                memory_stats(this->mshr_born_ordered[i]);
                this->push_token_credit(this->mshr_born_ordered[i]->id_dst, this->mshr_born_ordered[i]->memory_operation);
                this->mshr_born_ordered[i]->package_clean();
                this->mshr_born_ordered.erase(this->mshr_born_ordered.begin() + i);
            }
            break;
        }
    }

    // =================================================================
    // HIVE_BUFFER - UNTREATED
    // =================================================================
    // READY to execute the next
    if (this->hive_ready_cycle <= sinuca_engine.get_global_cycle()) {
        memory_package_t *package = NULL;
        bool completed = true;
        this->hive_buffer_actual_position = find_next_hive_operation();
        if (this->hive_buffer_actual_position != POSITION_FAIL) {
            package = this->hive_buffer[this->hive_buffer_actual_position];
        }
        if (package != NULL) {
            switch (package->memory_operation) {

                // HIVE =========================================================
                case MEMORY_OPERATION_HIVE_LOCK:
                case MEMORY_OPERATION_HIVE_UNLOCK:
                    package->memory_size = 1;
                    package->is_answer = true;
                    package->package_transmit(0);
                    this->hive_ready_cycle = sinuca_engine.get_global_cycle();
                break;

                case MEMORY_OPERATION_HIVE_INT_ALU:
                case MEMORY_OPERATION_HIVE_INT_MUL:
                case MEMORY_OPERATION_HIVE_INT_DIV:

                case MEMORY_OPERATION_HIVE_FP_ALU :
                case MEMORY_OPERATION_HIVE_FP_MUL :
                case MEMORY_OPERATION_HIVE_FP_DIV :
				
				//CIM
				case MEMORY_OPERATION_CIM_BTW_OR:
				case MEMORY_OPERATION_CIM_BTW_AND:
				case MEMORY_OPERATION_CIM_BTW_XOR:
				case MEMORY_OPERATION_CIM_EQ_CMP:
				case MEMORY_OPERATION_CIM_ADR_LD:
				case MEMORY_OPERATION_CIM_MEM_LD:
				case MEMORY_OPERATION_CIM_MEM_ST:

                    // Check if all the registers are ready.
                    if (package->hive_read1 != -1 && this->hive_wait_registers[package->hive_read1] != 0 ) {
                        completed = false;
                        break;
                    }
                    if (package->hive_read2 != -1 && this->hive_wait_registers[package->hive_read2] != 0 ) {
                        completed = false;
                        break;
                    }
                    if (package->hive_write != -1 && this->hive_wait_registers[package->hive_write] != 0 ) {
                        ERROR_PRINTF("Should not write in a non_ready register.\n")
                    }
                    MEMORY_CONTROLLER_DEBUG_PRINTF("Untreated HIVE_OP\n");
                    package->memory_size = 1;
                    package->is_answer = true;
                    package->package_transmit(get_hive_latency(package->memory_operation));
                    // Control the FU's usage, that's why no register dep is created.
                    this->hive_ready_cycle = sinuca_engine.get_global_cycle() + get_hive_latency(package->memory_operation);
                break;

                case MEMORY_OPERATION_HIVE_LOAD:
                    if (this->hive_nano_buffer_used_total < this->bank_buffer_size * this->channels_per_controller){
                        MEMORY_CONTROLLER_DEBUG_PRINTF("Untreated HIVE_LOAD\n");
                        for (uint32_t i = 0; i < this->bank_buffer_size * this->channels_per_controller; i++){
                            if (this->hive_nano_buffer_used[i] == 0) {

                                this->hive_nano_buffer_used_total++;
                                this->hive_nano_buffer_used[i] = hive_nano_buffer_size;

                                ERROR_ASSERT_PRINTF(package->hive_write != -1, "LOAD into the register -1.\n")
                                ERROR_ASSERT_PRINTF(this->hive_wait_registers[package->hive_write] == 0, "Should not write in a non_ready register.\n")
                                this->hive_wait_registers[package->hive_write] = hive_nano_buffer_size;
                                for (uint32_t j=0; j < hive_nano_buffer_size; j++){
                                    this->hive_nano_buffer[i][j] = *package;
                                    this->hive_nano_buffer[i][j].memory_operation = MEMORY_OPERATION_HIVE_NANO_LOAD;
                                    this->hive_nano_buffer[i][j].memory_address += j * sinuca_engine.get_global_line_size();
                                }

                                package->memory_size = 1;
                                package->is_answer = true;
                                package->package_transmit(get_hive_latency(package->memory_operation));
                                this->hive_ready_cycle = sinuca_engine.get_global_cycle() + get_hive_latency(package->memory_operation);
                                break;
                            }
                        }
                    }
                    else {
                        completed = false;
                        break;
                    }
                break;

                case MEMORY_OPERATION_HIVE_STORE:
                    if (this->hive_nano_buffer_used_total < this->bank_buffer_size * this->channels_per_controller){
                        MEMORY_CONTROLLER_DEBUG_PRINTF("Untreated HIVE_STORE\n");
                        for (uint32_t i = 0; i < this->bank_buffer_size * this->channels_per_controller; i++){
                            if (this->hive_nano_buffer_used[i] == 0) {

                                this->hive_nano_buffer_used_total++;
                                this->hive_nano_buffer_used[i] = hive_nano_buffer_size;

                                for (uint32_t j=0; j < hive_nano_buffer_size; j++){
                                    this->hive_nano_buffer[i][j] = *package;
                                    this->hive_nano_buffer[i][j].memory_operation = MEMORY_OPERATION_HIVE_NANO_STORE;
                                    this->hive_nano_buffer[i][j].memory_address += j * sinuca_engine.get_global_line_size();
                                }

                                package->memory_size = 1;
                                package->is_answer = true;
                                package->package_transmit(get_hive_latency(package->memory_operation));
                                this->hive_ready_cycle = sinuca_engine.get_global_cycle() + get_hive_latency(package->memory_operation);
                                break;
                            }
                        }
                    }
                    else {
                        completed = false;
                        break;
                    }
                break;


                default:
                    ERROR_PRINTF("Should not receive a normal package here!\n")
                break;
            }

            if (completed) {
                if(package->memory_operation == MEMORY_OPERATION_HIVE_UNLOCK) {
                    MEMORY_CONTROLLER_DEBUG_PRINTF("Untreated HIVE_UNLOCK\n");
                    this->hive_state = HIVE_STATE_UNLOCK;
                    this->hive_id_owner = 0;
                    this->hive_number = 0;
                }
                else {
                    MEMORY_CONTROLLER_DEBUG_PRINTF("Untreated HIVE_LOCK\n");
                    this->hive_state = HIVE_STATE_LOCK;
                    this->hive_id_owner = package->id_owner;
                    this->hive_number = package->hive_number;
                }

                // Advance the HIVE
                this->hive_buffer_actual_position = POSITION_FAIL;
            }
        }
    }

    // =================================================================
    /// MSHR_BUFFER - UNTREATED
    // =================================================================
    for (uint32_t i = 0; i < this->mshr_born_ordered.size(); i++){
        if (mshr_born_ordered[i]->state == PACKAGE_STATE_UNTREATED &&
        this->mshr_born_ordered[i]->ready_cycle <= sinuca_engine.get_global_cycle()) {

            ERROR_ASSERT_PRINTF(this->mshr_born_ordered[i]->is_answer == false, "Packages being treated should not be answer.")
            uint32_t channel = this->get_channel(this->mshr_born_ordered[i]->memory_address);

            this->mshr_born_ordered[i]->state = this->channels[channel].treat_memory_request(this->mshr_born_ordered[i]);
            ERROR_ASSERT_PRINTF(this->mshr_born_ordered[i]->state != PACKAGE_STATE_FREE, "Must not receive back a FREE, should receive READY + Latency")
            /// Could not treat, then restart born_cycle (change priority)
            if (this->mshr_born_ordered[i]->state == PACKAGE_STATE_UNTREATED) {
                this->mshr_born_ordered[i]->born_cycle = sinuca_engine.get_global_cycle();

                memory_package_t *package = this->mshr_born_ordered[i];
                this->mshr_born_ordered.erase(this->mshr_born_ordered.begin() + i);
                this->insert_mshr_born_ordered(package);
            }
            break;
        }
    }

    for (uint32_t i = 0; i < this->channels_per_controller; i++) {
        this->channels[i].clock(subcycle);
    }

};

// ============================================================================
void memory_controller_t::insert_mshr_born_ordered(memory_package_t* package){
    /// this->mshr_born_ordered            = [OLDER --------> NEWER]
    /// this->mshr_born_ordered.born_cycle = [SMALLER -----> BIGGER]

    /// Most of the insertions are made in the end !!!
    for (int32_t i = this->mshr_born_ordered.size() - 1; i >= 0 ; i--){
        if (this->mshr_born_ordered[i]->born_cycle <= package->born_cycle) {
            this->mshr_born_ordered.insert(this->mshr_born_ordered.begin() + i + 1, package);
            return;
        }
    }
    /// Could not find a older package to insert or it is just empty
    this->mshr_born_ordered.insert(this->mshr_born_ordered.begin(), package);

    /// Check the MSHR BORN ORDERED
    #ifdef MEMORY_CONTROLLER_DEBUG
        uint64_t test_order = 0;
        for (uint32_t i = 0; i < this->mshr_born_ordered.size(); i++){
            if (test_order > this->mshr_born_ordered[i]->born_cycle) {
                for (uint32_t j = 0; j < this->mshr_born_ordered.size(); j++){
                    MEMORY_CONTROLLER_DEBUG_PRINTF("%" PRIu64 " ", this->mshr_born_ordered[j]->born_cycle);
                }
                ERROR_ASSERT_PRINTF(test_order > this->mshr_born_ordered[i]->born_cycle, "Wrong order when inserting (%" PRIu64 ")\n", package->born_cycle);
            }
            test_order = this->mshr_born_ordered[i]->born_cycle;
        }
    #endif
};

// ============================================================================
int32_t memory_controller_t::allocate_request(memory_package_t* package){

    int32_t slot = memory_package_t::find_free(this->mshr_buffer, this->mshr_request_buffer_size);
    ERROR_ASSERT_PRINTF(slot != POSITION_FAIL, "Receiving a REQUEST package, but MSHR is full\n")
    if (slot != POSITION_FAIL) {
        MEMORY_CONTROLLER_DEBUG_PRINTF("\t NEW REQUEST\n");
        this->mshr_buffer[slot] = *package;
        // ~ this->insert_mshr_born_ordered(&this->mshr_buffer[slot]);    /// Insert into a parallel and well organized MSHR structure

        // HIVE
        if (package->is_hive &&
        package->memory_operation != MEMORY_OPERATION_HIVE_NANO_LOAD &&
        package->memory_operation != MEMORY_OPERATION_HIVE_NANO_STORE) {
            this->hive_buffer.push_back(&this->mshr_buffer[slot]);
        }
        else {
            this->insert_mshr_born_ordered(&this->mshr_buffer[slot]);    /// Insert into a parallel and well organized MSHR structure
        }
    }
    return slot;
};

// ============================================================================
int32_t memory_controller_t::allocate_prefetch(memory_package_t* package){
    int32_t slot = memory_package_t::find_free(this->mshr_buffer + this->mshr_request_buffer_size ,
                                                this->mshr_prefetch_buffer_size);
    ERROR_ASSERT_PRINTF(slot != POSITION_FAIL, "Receiving a PREFETCH package, but MSHR is full\n")
    if (slot != POSITION_FAIL) {
        slot += this->mshr_request_buffer_size;
        MEMORY_CONTROLLER_DEBUG_PRINTF("\t NEW PREFETCH\n");
        this->mshr_buffer[slot] = *package;
        this->insert_mshr_born_ordered(&this->mshr_buffer[slot]);    /// Insert into a parallel and well organized MSHR structure
    }
    return slot;
};

// ============================================================================
int32_t memory_controller_t::allocate_write(memory_package_t* package){

    int32_t slot = memory_package_t::find_free(this->mshr_buffer + this->mshr_request_buffer_size + this->mshr_prefetch_buffer_size, this->mshr_write_buffer_size);
    ERROR_ASSERT_PRINTF(slot != POSITION_FAIL, "Receiving a WRITEBACK package, but MSHR is full\n")
    if (slot != POSITION_FAIL) {
        slot += this->mshr_request_buffer_size + this->mshr_prefetch_buffer_size;
        MEMORY_CONTROLLER_DEBUG_PRINTF("\t NEW WRITEBACK\n");
        this->mshr_buffer[slot] = *package;
        this->insert_mshr_born_ordered(&this->mshr_buffer[slot]);    /// Insert into a parallel and well organized MSHR structure
    }
    return slot;
};



// ============================================================================
int32_t memory_controller_t::send_package(memory_package_t *package) {
    MEMORY_CONTROLLER_DEBUG_PRINTF("send_package() package:%s\n", package->content_to_string().c_str());
    // ~ ERROR_ASSERT_PRINTF(package->memory_address != 0, "Wrong memory address.\n%s\n", package->content_to_string().c_str());
    ERROR_ASSERT_PRINTF(package->memory_operation != MEMORY_OPERATION_WRITEBACK && package->memory_operation != MEMORY_OPERATION_WRITE, "Main memory must never send answer for WRITE.\n");

    sinuca_engine.interconnection_controller->find_package_route(package);
    ERROR_ASSERT_PRINTF(package->hop_count != POSITION_FAIL, "Achieved the end of the route\n");
    uint32_t output_port = package->hops[package->hop_count];  /// Where to send the package ?
    ERROR_ASSERT_PRINTF(output_port < this->get_max_ports(), "Output Port does not exist\n");

    if (this->send_ready_cycle[output_port] <= sinuca_engine.get_global_cycle()) {

        // HIVE
        if (package->is_hive &&
        !package->is_answer &&
        (package->memory_operation == MEMORY_OPERATION_HIVE_NANO_LOAD ||
        package->memory_operation == MEMORY_OPERATION_HIVE_NANO_STORE)) {
            ERROR_PRINTF("send_package for: %s.\n", get_enum_memory_operation_char(package->memory_operation));
            /// Check if DESTINATION has FREE SPACE available.
            // ~ if (sinuca_engine.interconnection_interface_array[package->id_dst]->check_token_list(package) == false) {
                // ~ MEMORY_CONTROLLER_DEBUG_PRINTF("\tSEND FAIL (NO TOKENS)\n");
                // ~ return POSITION_FAIL;
            // ~ }
        }

        package->hop_count--;  /// Consume its own port

        uint32_t transmission_latency = sinuca_engine.interconnection_controller->find_package_route_latency(package, this, this->get_interface_output_component(output_port));
        bool sent = this->get_interface_output_component(output_port)->receive_package(package, this->get_ports_output_component(output_port), transmission_latency);
        if (sent) {
            MEMORY_CONTROLLER_DEBUG_PRINTF("\tSEND OK (latency:%u)\n", transmission_latency);
            this->send_ready_cycle[output_port] = sinuca_engine.get_global_cycle() + transmission_latency;
            return transmission_latency;
        }
        else {
            MEMORY_CONTROLLER_DEBUG_PRINTF("\tSEND FAIL\n");
            package->hop_count++;  /// Do not Consume its own port
            return POSITION_FAIL;
        }
    }
    MEMORY_CONTROLLER_DEBUG_PRINTF("\tSEND FAIL (BUSY)\n");
    return POSITION_FAIL;
};

// ============================================================================
bool memory_controller_t::receive_package(memory_package_t *package, uint32_t input_port, uint32_t transmission_latency) {
    MEMORY_CONTROLLER_DEBUG_PRINTF("receive_package() port:%u, package:%s\n", input_port, package->content_to_string().c_str());

    // ~ ERROR_ASSERT_PRINTF(package->memory_address != 0, "Wrong memory address.\n%s\n", package->content_to_string().c_str());
    ERROR_ASSERT_PRINTF(package->id_dst == this->get_id() && package->hop_count == POSITION_FAIL, "Received some package for a different id_dst.\n");
    ERROR_ASSERT_PRINTF(input_port < this->get_max_ports(), "Received a wrong input_port\n");
    // ~ ERROR_ASSERT_PRINTF(package->is_answer == false, "Only requests are expected.\n");

    int32_t slot = POSITION_FAIL;
    if (this->recv_ready_cycle[input_port] <= sinuca_engine.get_global_cycle()) {

        /// HIVE ANSWER
        if (package->is_answer) {
            ERROR_ASSERT_PRINTF(package->hive_write != -1, "Receiving an NANO answer that does not write a register");
            this->hive_wait_registers[package->hive_write]--;
            this->recv_ready_cycle[input_port] = transmission_latency + sinuca_engine.get_global_cycle();  /// Ready to receive from HIGHER_PORT
            // ~ this->remove_token_list(package);
            return OK;
        }

        switch (package->memory_operation) {
            case MEMORY_OPERATION_READ:
            case MEMORY_OPERATION_INST:

            // HMC -> READ buffer
            case MEMORY_OPERATION_HMC_ROA:
            case MEMORY_OPERATION_HMC_ROWA:

            // HIVE -> READ buffer
            case MEMORY_OPERATION_HIVE_LOCK:
            case MEMORY_OPERATION_HIVE_UNLOCK:
            case MEMORY_OPERATION_HIVE_LOAD:
            case MEMORY_OPERATION_HIVE_STORE:
            case MEMORY_OPERATION_HIVE_NANO_LOAD:
            case MEMORY_OPERATION_HIVE_NANO_STORE:
            case MEMORY_OPERATION_HIVE_INT_ALU:
            case MEMORY_OPERATION_HIVE_INT_MUL:
            case MEMORY_OPERATION_HIVE_INT_DIV:

            case MEMORY_OPERATION_HIVE_FP_ALU :
            case MEMORY_OPERATION_HIVE_FP_MUL :
            case MEMORY_OPERATION_HIVE_FP_DIV :
			
			//CIM
			case MEMORY_OPERATION_CIM_BTW_OR:
			case MEMORY_OPERATION_CIM_BTW_AND:
			case MEMORY_OPERATION_CIM_BTW_XOR:
			case MEMORY_OPERATION_CIM_EQ_CMP:
			case MEMORY_OPERATION_CIM_ADR_LD:
			case MEMORY_OPERATION_CIM_MEM_LD:
			case MEMORY_OPERATION_CIM_MEM_ST:
                slot = this->allocate_request(package);
            break;

            case MEMORY_OPERATION_PREFETCH:
                slot = this->allocate_prefetch(package);
            break;

            case MEMORY_OPERATION_WRITEBACK:
            case MEMORY_OPERATION_WRITE:
                slot = this->allocate_write(package);
            break;
        }

        if (slot == POSITION_FAIL) {
            return FAIL;
        }
        MEMORY_CONTROLLER_DEBUG_PRINTF("\t RECEIVED REQUEST\n");

        this->mshr_buffer[slot].package_untreated(1);
        /// Prepare for answer later
        this->mshr_buffer[slot].package_set_src_dst(this->get_id(), package->id_src);
        this->recv_ready_cycle[input_port] = transmission_latency + sinuca_engine.get_global_cycle();  /// Ready to receive from HIGHER_PORT
        return OK;

    }
    else {
        MEMORY_CONTROLLER_DEBUG_PRINTF("\tRECV FAIL (BUSY)\n");
        return FAIL;
    }
    ERROR_PRINTF("Memory receiving %s.\n", get_enum_memory_operation_char(package->memory_operation))
    return FAIL;
};


// ============================================================================
/// Token Controller Methods
// ============================================================================
bool memory_controller_t::pop_token_credit(uint32_t src_id, memory_operation_t memory_operation) {

    MEMORY_CONTROLLER_DEBUG_PRINTF("Popping a token to id (%" PRIu32 ")\n", src_id)

    switch (memory_operation) {
        case MEMORY_OPERATION_READ:
        case MEMORY_OPERATION_INST:

        // HMC -> READ buffer
        case MEMORY_OPERATION_HMC_ROA:
        case MEMORY_OPERATION_HMC_ROWA:

        // HIVE -> READ buffer
        case MEMORY_OPERATION_HIVE_LOCK:
        case MEMORY_OPERATION_HIVE_UNLOCK:
        case MEMORY_OPERATION_HIVE_LOAD:
        case MEMORY_OPERATION_HIVE_STORE:
        case MEMORY_OPERATION_HIVE_NANO_LOAD:
        case MEMORY_OPERATION_HIVE_NANO_STORE:
        case MEMORY_OPERATION_HIVE_INT_ALU:
        case MEMORY_OPERATION_HIVE_INT_MUL:
        case MEMORY_OPERATION_HIVE_INT_DIV:

        case MEMORY_OPERATION_HIVE_FP_ALU :
        case MEMORY_OPERATION_HIVE_FP_MUL :
        case MEMORY_OPERATION_HIVE_FP_DIV :

		//CIM
		case MEMORY_OPERATION_CIM_BTW_OR:
		case MEMORY_OPERATION_CIM_BTW_AND:
		case MEMORY_OPERATION_CIM_BTW_XOR:
		case MEMORY_OPERATION_CIM_EQ_CMP:
		case MEMORY_OPERATION_CIM_ADR_LD:
		case MEMORY_OPERATION_CIM_MEM_LD:
		case MEMORY_OPERATION_CIM_MEM_ST:
            if (this->mshr_tokens_request[src_id] > 0) {
                this->mshr_tokens_request[src_id]--;
                MEMORY_CONTROLLER_DEBUG_PRINTF("Found a REQUEST token. (%" PRIu32 " left)\n", this->mshr_tokens_request[src_id]);
                return OK;
            }
            else {
                MEMORY_CONTROLLER_DEBUG_PRINTF("Must wait, no REQUEST tokens left\n");
                this->add_stat_full_mshr_request_buffer();
                return FAIL;
            }
        break;

        case MEMORY_OPERATION_PREFETCH:
            if (this->mshr_tokens_prefetch[src_id] > 0) {
                this->mshr_tokens_prefetch[src_id]--;
                MEMORY_CONTROLLER_DEBUG_PRINTF("Found a PREFETCH token. (%" PRIu32 " left)\n", this->mshr_tokens_prefetch[src_id]);
                return OK;
            }
            else {
                MEMORY_CONTROLLER_DEBUG_PRINTF("Must wait, no PREFETCH tokens left\n");
                this->add_stat_full_mshr_prefetch_buffer();
                return FAIL;
            }
        break;

        case MEMORY_OPERATION_WRITE:
        case MEMORY_OPERATION_WRITEBACK:
            if (this->mshr_tokens_write[src_id] > 0) {
                this->mshr_tokens_write[src_id]--;
                MEMORY_CONTROLLER_DEBUG_PRINTF("Found a WRITE token. (%" PRIu32 " left)\n", this->mshr_tokens_write[src_id]);
                return OK;
            }
            else {
                MEMORY_CONTROLLER_DEBUG_PRINTF("Must wait, no WRITE tokens left\n");
                this->add_stat_full_mshr_write_buffer();
                return FAIL;
            }
        break;
    }

    ERROR_PRINTF("Could not treat the token\n")
    return FAIL;
};

// ============================================================================
void memory_controller_t::push_token_credit(uint32_t src_id, memory_operation_t memory_operation) {

    MEMORY_CONTROLLER_DEBUG_PRINTF("Pushing a token to id %" PRIu32 "\n", src_id)
    ERROR_ASSERT_PRINTF(this->get_id() != src_id, "Should not be pushing a token to this.\n")

    switch (memory_operation) {
        case MEMORY_OPERATION_READ:
        case MEMORY_OPERATION_INST:

        // HMC -> READ buffer
        case MEMORY_OPERATION_HMC_ROA:
        case MEMORY_OPERATION_HMC_ROWA:

        // HIVE
        case MEMORY_OPERATION_HIVE_LOCK:
        case MEMORY_OPERATION_HIVE_UNLOCK:
        case MEMORY_OPERATION_HIVE_LOAD:
        case MEMORY_OPERATION_HIVE_STORE:
        case MEMORY_OPERATION_HIVE_INT_ALU:
        case MEMORY_OPERATION_HIVE_INT_MUL:
        case MEMORY_OPERATION_HIVE_INT_DIV:

        case MEMORY_OPERATION_HIVE_FP_ALU :
        case MEMORY_OPERATION_HIVE_FP_MUL :
        case MEMORY_OPERATION_HIVE_FP_DIV :

		//CIM
		case MEMORY_OPERATION_CIM_BTW_OR:
		case MEMORY_OPERATION_CIM_BTW_AND:
		case MEMORY_OPERATION_CIM_BTW_XOR:
		case MEMORY_OPERATION_CIM_EQ_CMP:
		case MEMORY_OPERATION_CIM_ADR_LD:
		case MEMORY_OPERATION_CIM_MEM_LD:
		case MEMORY_OPERATION_CIM_MEM_ST:
            ERROR_ASSERT_PRINTF(this->mshr_tokens_request[src_id] <= (int32_t)this->higher_level_request_tokens &&
                                this->mshr_tokens_request[src_id] >= 0,
                                "Found an component with more tokens than permitted\n");
            this->mshr_tokens_request[src_id]++;
        break;

        // =============================================================
        // Receiving a wrong HIVE
        case MEMORY_OPERATION_HIVE_NANO_LOAD:
        case MEMORY_OPERATION_HIVE_NANO_STORE:
            ERROR_PRINTF("push_token_credit for: %s.\n", get_enum_memory_operation_char(memory_operation));
        break;


        case MEMORY_OPERATION_PREFETCH:
            ERROR_ASSERT_PRINTF(this->mshr_tokens_prefetch[src_id] <= (int32_t)this->higher_level_prefetch_tokens &&
                                this->mshr_tokens_prefetch[src_id] >= 0,
                                "Found an component with more tokens than permitted\n");
            this->mshr_tokens_prefetch[src_id]++;
        break;

        case MEMORY_OPERATION_WRITE:
        case MEMORY_OPERATION_WRITEBACK:
            ERROR_ASSERT_PRINTF(this->mshr_tokens_write[src_id] <= (int32_t)this->higher_level_write_tokens &&
                                this->mshr_tokens_write[src_id] >= 0,
                                "Found an component with more tokens than permitted\n");
            this->mshr_tokens_write[src_id]++;
        break;
    }

    return;
};


// ============================================================================
void memory_controller_t::memory_stats(memory_package_t *package) {

    this->add_stat_accesses();

    switch (package->memory_operation) {
        case MEMORY_OPERATION_READ:
            this->add_stat_read_completed(package->born_cycle);
        break;

        case MEMORY_OPERATION_INST:
            this->add_stat_instruction_completed(package->born_cycle);
        break;

        case MEMORY_OPERATION_PREFETCH:
            this->add_stat_prefetch_completed(package->born_cycle);
        break;

        case MEMORY_OPERATION_WRITEBACK:
            this->add_stat_writeback_completed(package->born_cycle);
        break;

        case MEMORY_OPERATION_WRITE:
            this->add_stat_write_completed(package->born_cycle);
        break;

        // HMC
        case MEMORY_OPERATION_HMC_ROA:
            this->add_stat_hmc_roa_completed(package->born_cycle);
        break;

        case MEMORY_OPERATION_HMC_ROWA:
            this->add_stat_hmc_rowa_completed(package->born_cycle);
        break;

        // HIVE - NANO LOAD/STORE (Requests from a different MC)
        case MEMORY_OPERATION_HIVE_NANO_LOAD:
        case MEMORY_OPERATION_HIVE_NANO_STORE:

        break;

        // HIVE
        case MEMORY_OPERATION_HIVE_LOCK:
            this->add_stat_hive_lock_completed(package->born_cycle);
        break;

        case MEMORY_OPERATION_HIVE_UNLOCK:
            this->add_stat_hive_unlock_completed(package->born_cycle);
        break;

        case MEMORY_OPERATION_HIVE_LOAD:
            this->add_stat_hive_load_completed(package->born_cycle);
        break;

        case MEMORY_OPERATION_HIVE_STORE:
            this->add_stat_hive_store_completed(package->born_cycle);
        break;

        case MEMORY_OPERATION_HIVE_INT_ALU:
            this->add_stat_hive_int_alu_completed(package->born_cycle);
        break;

        case MEMORY_OPERATION_HIVE_INT_MUL:
            this->add_stat_hive_int_mul_completed(package->born_cycle);
        break;

        case MEMORY_OPERATION_HIVE_INT_DIV:
            this->add_stat_hive_int_div_completed(package->born_cycle);
        break;
	
        case MEMORY_OPERATION_HIVE_FP_ALU:
            this->add_stat_hive_fp_alu_completed(package->born_cycle);
        break;

        case MEMORY_OPERATION_HIVE_FP_MUL:
            this->add_stat_hive_fp_mul_completed(package->born_cycle);
        break;

        case MEMORY_OPERATION_HIVE_FP_DIV:
            this->add_stat_hive_fp_div_completed(package->born_cycle);
        break;
		
		case MEMORY_OPERATION_CIM_BTW_OR:
            this->add_stat_cim_btw_or_completed(package->born_cycle);
        break;

		case MEMORY_OPERATION_CIM_BTW_AND:
            this->add_stat_cim_btw_and_completed(package->born_cycle);
        break;

		case MEMORY_OPERATION_CIM_BTW_XOR:
            this->add_stat_cim_btw_xor_completed(package->born_cycle);
        break;

		case MEMORY_OPERATION_CIM_EQ_CMP:
            this->add_stat_cim_eq_cmp_completed(package->born_cycle);
        break;
		
		case MEMORY_OPERATION_CIM_ADR_LD:
            this->add_stat_cim_adr_ld_completed(package->born_cycle);
        break;
		
		case MEMORY_OPERATION_CIM_MEM_LD:
            this->add_stat_cim_mem_ld_completed(package->born_cycle);
        break;

		case MEMORY_OPERATION_CIM_MEM_ST:
            this->add_stat_cim_mem_st_completed(package->born_cycle);
        break;
    }

};


// ============================================================================
void memory_controller_t::print_structures() {
    SINUCA_PRINTF("%s MSHR_BUFFER:\n%s", this->get_label(), memory_package_t::print_all(this->mshr_buffer, this->mshr_buffer_size).c_str())

    SINUCA_PRINTF("%s MSHR_TOKENS_REQUEST:\n", this->get_label())
    for (uint32_t i = 0; i < sinuca_engine.get_interconnection_interface_array_size(); i++) {
        uint32_t src_id = sinuca_engine.interconnection_interface_array[i]->get_id();
        if (this->mshr_tokens_request[src_id] != -1) {
            SINUCA_PRINTF("%s => %d tokens\n", sinuca_engine.interconnection_interface_array[i]->get_label(), this->mshr_tokens_request[src_id])
        }
    }

    SINUCA_PRINTF("%s MSHR_TOKENS_PREFETCH:\n", this->get_label())
    for (uint32_t i = 0; i < sinuca_engine.get_interconnection_interface_array_size(); i++) {
        uint32_t src_id = sinuca_engine.interconnection_interface_array[i]->get_id();
        if (this->mshr_tokens_prefetch[src_id] != -1) {
            SINUCA_PRINTF("%s => %d tokens\n", sinuca_engine.interconnection_interface_array[i]->get_label(), this->mshr_tokens_prefetch[src_id])
        }
    }

    SINUCA_PRINTF("%s MSHR_TOKENS_WRITE:\n", this->get_label())
    for (uint32_t i = 0; i < sinuca_engine.get_interconnection_interface_array_size(); i++) {
        uint32_t src_id = sinuca_engine.interconnection_interface_array[i]->get_id();
        if (this->mshr_tokens_write[src_id] != -1) {
            SINUCA_PRINTF("%s => %d tokens\n", sinuca_engine.interconnection_interface_array[i]->get_label(), this->mshr_tokens_write[src_id])
        }
    }

    // HIVE
    for (uint32_t j = 0; j < this->hive_total_registers; j++) {
        SINUCA_PRINTF("REGISTER[%s] %s\n", utils_t::uint32_to_string(j).c_str(),
                                            utils_t::uint32_to_string(this->hive_wait_registers[j]).c_str());
    }


    SINUCA_PRINTF("hive_state:%s\n", get_enum_hive_state_t_char(this->hive_state));
    SINUCA_PRINTF("hive_id_owner:%s\n", utils_t::uint64_to_string(this->hive_id_owner).c_str());
    SINUCA_PRINTF("hive_number:%s\n", utils_t::uint64_to_string(this->hive_number).c_str());

    for (uint32_t j = 0; j < this->hive_buffer.size(); j++) {
        SINUCA_PRINTF("%s HIVE_BUFFER[%s] %s\n", this->get_label(),
                                                    utils_t::uint32_to_string(j).c_str(),
                                                    this->hive_buffer[j]->content_to_string().c_str());
    }

    SINUCA_PRINTF("hive_nano_buffer_size:%s\n", utils_t::uint32_to_string(this->hive_nano_buffer_size).c_str());

    SINUCA_PRINTF("hive_nano_buffer_used_total: %s\n", utils_t::uint32_to_string(this->hive_nano_buffer_used_total).c_str());

    SINUCA_PRINTF("hive_nano_buffer_used: ");
    for (uint32_t j = 0; j < this->bank_buffer_size * this->channels_per_controller; j++) {
        SINUCA_PRINTF(" %s", utils_t::uint32_to_string(this->hive_nano_buffer_used[j]).c_str());
    }
    SINUCA_PRINTF("\n");


    for (uint32_t i = 0; i < this->bank_buffer_size * this->channels_per_controller; i++) {
        if (hive_nano_buffer_used[i] == 0) {
            continue;
        }
        for (uint32_t j = 0; j < this->hive_nano_buffer_size; j++) {
            SINUCA_PRINTF("%s HIVE_NANO_BUFFER[%s][%s] %s\n", this->get_label(),
                                                        utils_t::uint32_to_string(i).c_str(),
                                                        utils_t::uint32_to_string(j).c_str(),
                                                        this->hive_nano_buffer[i][j].content_to_string().c_str());
        }
        SINUCA_PRINTF("\n");
    }

    for (uint32_t i = 0; i < this->channels_per_controller; i++) {
        this->channels[i].print_structures();
    }

};

// =============================================================================
void memory_controller_t::panic() {
    this->print_structures();
};

// ============================================================================
void memory_controller_t::periodic_check(){
    #ifdef MEMORY_CONTROLLER_DEBUG
        this->print_structures();
    #endif
    /// Check Requests
    ERROR_ASSERT_PRINTF(memory_package_t::check_age(this->mshr_buffer,
                                                    this->mshr_request_buffer_size) == OK, "Check_age failed.\n");
    /// Check Prefetchs
    ERROR_ASSERT_PRINTF(memory_package_t::check_age(this->mshr_buffer +
                                                    this->mshr_request_buffer_size,
                                                    this->mshr_prefetch_buffer_size) == OK, "Check_age failed.\n");
    /// Check Writes
    // ~ ERROR_ASSERT_PRINTF(memory_package_t::check_age(this->mshr_buffer +
                                                    // ~ this->mshr_request_buffer_size +
                                                    // ~ this->mshr_prefetch_buffer_size,
                                                    // ~ this->mshr_write_buffer_size) == OK, "Check_age failed.\n");

    switch (this->write_priority_policy) {
        case WRITE_PRIORITY_DRAIN_WHEN_FULL:
        break;

        case WRITE_PRIORITY_SERVICE_AT_NO_READ:
            ERROR_ASSERT_PRINTF(memory_package_t::check_age(this->mshr_buffer, this->mshr_buffer_size) == OK, "Check_age failed.\n");

            for (uint32_t i = 0; i < this->channels_per_controller; i++) {
                this->channels[i].periodic_check();
            }
        break;
    }
};

// ============================================================================
/// STATISTICS
// ============================================================================
void memory_controller_t::reset_statistics() {
    this->stat_accesses = 0;

    this->set_stat_instruction_completed(0);
    this->set_stat_read_completed(0);
    this->set_stat_prefetch_completed(0);
    this->set_stat_write_completed(0);
    this->set_stat_writeback_completed(0);

    this->stat_min_instruction_wait_time = MAX_ALIVE_TIME;
    this->stat_max_instruction_wait_time = 0;
    this->stat_accumulated_instruction_wait_time = 0;

    this->stat_min_read_wait_time = MAX_ALIVE_TIME;
    this->stat_max_read_wait_time = 0;
    this->stat_accumulated_read_wait_time = 0;

    this->stat_min_prefetch_wait_time = MAX_ALIVE_TIME;
    this->stat_max_prefetch_wait_time = 0;
    this->stat_accumulated_prefetch_wait_time = 0;

    this->stat_min_write_wait_time = MAX_ALIVE_TIME;
    this->stat_max_write_wait_time = 0;
    this->stat_accumulated_write_wait_time = 0;

    this->stat_min_writeback_wait_time = MAX_ALIVE_TIME;
    this->stat_max_writeback_wait_time = 0;
    this->stat_accumulated_writeback_wait_time = 0;

    // HMC
    this->set_stat_hmc_roa_completed(0);
    this->set_stat_hmc_rowa_completed(0);

    this->stat_min_hmc_roa_wait_time = MAX_ALIVE_TIME;
    this->stat_max_hmc_roa_wait_time = 0;
    this->stat_accumulated_hmc_roa_wait_time = 0;

    this->stat_min_hmc_rowa_wait_time = MAX_ALIVE_TIME;
    this->stat_max_hmc_rowa_wait_time = 0;
    this->stat_accumulated_hmc_rowa_wait_time = 0;

    this->stat_full_mshr_request_buffer = 0;
    this->stat_full_mshr_write_buffer = 0;
    this->stat_full_mshr_prefetch_buffer = 0;

    // HIVE
    this->set_stat_hive_lock_completed(0);
    this->set_stat_hive_unlock_completed(0);
    this->set_stat_hive_load_completed(0);
    this->set_stat_hive_store_completed(0);
    this->set_stat_hive_nano_load_completed(0);
    this->set_stat_hive_nano_store_completed(0);
    this->set_stat_hive_int_alu_completed(0);
    this->set_stat_hive_int_mul_completed(0);
    this->set_stat_hive_int_div_completed(0);

    this->set_stat_hive_fp_alu_completed(0);
    this->set_stat_hive_fp_mul_completed(0);
    this->set_stat_hive_fp_div_completed(0);
	
	// CIM
    this->set_stat_cim_btw_or_completed(0);
    this->set_stat_cim_btw_and_completed(0);
	this->set_stat_cim_btw_xor_completed(0);
	this->set_stat_cim_eq_cmp_completed(0);
    this->set_stat_cim_adr_ld_completed(0);
	this->set_stat_cim_mem_ld_completed(0);
	this->set_stat_cim_mem_st_completed(0);
	
    this->stat_min_hive_lock_wait_time = MAX_ALIVE_TIME;
    this->stat_max_hive_lock_wait_time = 0;
    this->stat_accumulated_hive_lock_wait_time = 0;

    this->stat_min_hive_unlock_wait_time = MAX_ALIVE_TIME;
    this->stat_max_hive_unlock_wait_time = 0;
    this->stat_accumulated_hive_unlock_wait_time = 0;

    this->stat_min_hive_load_wait_time = MAX_ALIVE_TIME;
    this->stat_max_hive_load_wait_time = 0;
    this->stat_accumulated_hive_load_wait_time = 0;

    this->stat_min_hive_store_wait_time = MAX_ALIVE_TIME;
    this->stat_max_hive_store_wait_time = 0;
    this->stat_accumulated_hive_store_wait_time = 0;

    this->stat_min_hive_nano_load_wait_time = MAX_ALIVE_TIME;
    this->stat_max_hive_nano_load_wait_time = 0;
    this->stat_accumulated_hive_nano_load_wait_time = 0;

    this->stat_min_hive_nano_store_wait_time = MAX_ALIVE_TIME;
    this->stat_max_hive_nano_store_wait_time = 0;
    this->stat_accumulated_hive_nano_store_wait_time = 0;

    this->stat_min_hive_int_alu_wait_time = MAX_ALIVE_TIME;
    this->stat_max_hive_int_alu_wait_time = 0;
    this->stat_accumulated_hive_int_alu_wait_time = 0;

    this->stat_min_hive_int_mul_wait_time = MAX_ALIVE_TIME;
    this->stat_max_hive_int_mul_wait_time = 0;
    this->stat_accumulated_hive_int_mul_wait_time = 0;

    this->stat_min_hive_int_div_wait_time = MAX_ALIVE_TIME;
    this->stat_max_hive_int_div_wait_time = 0;
    this->stat_accumulated_hive_int_div_wait_time = 0;

    this->stat_min_hive_fp_alu_wait_time = MAX_ALIVE_TIME;
    this->stat_max_hive_fp_alu_wait_time = 0;
    this->stat_accumulated_hive_fp_alu_wait_time = 0;

    this->stat_min_hive_fp_mul_wait_time = MAX_ALIVE_TIME;
    this->stat_max_hive_fp_mul_wait_time = 0;
    this->stat_accumulated_hive_fp_mul_wait_time = 0;

    this->stat_min_hive_fp_div_wait_time = MAX_ALIVE_TIME;
    this->stat_max_hive_fp_div_wait_time = 0;
    this->stat_accumulated_hive_fp_div_wait_time = 0;
	
	// CIM
  	this->stat_min_cim_btw_or_wait_time = MAX_ALIVE_TIME;
    this->stat_max_cim_btw_or_wait_time = 0;
    this->stat_accumulated_cim_btw_or_wait_time = 0;

  	this->stat_min_cim_btw_and_wait_time = MAX_ALIVE_TIME;
    this->stat_max_cim_btw_and_wait_time = 0;
    this->stat_accumulated_cim_btw_and_wait_time = 0;

  	this->stat_min_cim_btw_xor_wait_time = MAX_ALIVE_TIME;
    this->stat_max_cim_btw_xor_wait_time = 0;
    this->stat_accumulated_cim_btw_xor_wait_time = 0;

  	this->stat_min_cim_eq_cmp_wait_time = MAX_ALIVE_TIME;
    this->stat_max_cim_eq_cmp_wait_time = 0;
    this->stat_accumulated_cim_eq_cmp_wait_time = 0;
	
  	this->stat_min_cim_adr_ld_wait_time = MAX_ALIVE_TIME;
    this->stat_max_cim_adr_ld_wait_time = 0;
    this->stat_accumulated_cim_adr_ld_wait_time = 0;

  	this->stat_min_cim_mem_ld_wait_time = MAX_ALIVE_TIME;
    this->stat_max_cim_mem_ld_wait_time = 0;
    this->stat_accumulated_cim_mem_ld_wait_time = 0;

  	this->stat_min_cim_mem_st_wait_time = MAX_ALIVE_TIME;
    this->stat_max_cim_mem_st_wait_time = 0;
    this->stat_accumulated_cim_mem_st_wait_time = 0;
	
    for (uint32_t i = 0; i < this->channels_per_controller; i++) {
        this->channels[i].reset_statistics();
    }
};

// ============================================================================
void memory_controller_t::print_statistics() {
    char title[100] = "";
    snprintf(title, sizeof(title), "Configuration of %s", this->get_label());
    sinuca_engine.write_statistics_big_separator();
    sinuca_engine.write_statistics_comments(title);
    sinuca_engine.write_statistics_big_separator();

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_accesses", stat_accesses);

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_instruction_completed", stat_instruction_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_read_completed", stat_read_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_prefetch_completed", stat_prefetch_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_write_completed", stat_write_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_writeback_completed", stat_writeback_completed);

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_sum_read",
                                                                                    stat_instruction_completed +
                                                                                    stat_read_completed +
                                                                                    stat_prefetch_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_sum_write",
                                                                                    stat_write_completed +
                                                                                    stat_writeback_completed);

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_sum_read_hive",
                                                                                    stat_instruction_completed +
                                                                                    stat_read_completed +
                                                                                    stat_prefetch_completed +
                                                                                    stat_hive_load_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_sum_write_hive",
                                                                                    stat_write_completed +
                                                                                    stat_writeback_completed +
                                                                                    stat_hive_store_completed);

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_instruction_wait_time", stat_min_instruction_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_read_wait_time", stat_min_read_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_prefetch_wait_time", stat_min_prefetch_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_write_wait_time", stat_min_write_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_writeback_wait_time", stat_min_writeback_wait_time);

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_instruction_wait_time", stat_max_instruction_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_read_wait_time", stat_max_read_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_prefetch_wait_time", stat_max_prefetch_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_write_wait_time", stat_max_write_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_writeback_wait_time", stat_max_writeback_wait_time);

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_instruction_wait_time", stat_accumulated_instruction_wait_time, stat_instruction_completed);
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_read_wait_time", stat_accumulated_read_wait_time, stat_read_completed);
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_prefetch_wait_time", stat_accumulated_prefetch_wait_time, stat_prefetch_completed);
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_write_wait_time", stat_accumulated_write_wait_time, stat_write_completed);
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_writeback_wait_time", stat_accumulated_writeback_wait_time, stat_writeback_completed);

    // HMC
    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_hmc_roa_completed", stat_hmc_roa_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_hmc_rowa_completed", stat_hmc_rowa_completed);

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_hmc_roa_wait_time", stat_min_hmc_roa_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_hmc_roa_wait_time", stat_max_hmc_roa_wait_time);

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_hmc_rowa_wait_time", stat_min_hmc_rowa_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_hmc_rowa_wait_time", stat_max_hmc_rowa_wait_time);

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_hmc_roa_wait_time", stat_accumulated_hmc_roa_wait_time, stat_hmc_roa_completed);
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_hmc_rowa_wait_time", stat_accumulated_hmc_rowa_wait_time, stat_hmc_rowa_completed);

    // HIVE
sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_hive_lock_completed", stat_hive_lock_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_hive_unlock_completed", stat_hive_unlock_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_hive_load_completed", stat_hive_load_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_hive_store_completed", stat_hive_store_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_hive_nano_load_completed", stat_hive_nano_load_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_hive_nano_store_completed", stat_hive_nano_store_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_hive_int_alu_completed", stat_hive_int_alu_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_hive_int_mul_completed", stat_hive_int_mul_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_hive_int_div_completed", stat_hive_int_div_completed);
    
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_hive_fp_alu_completed", stat_hive_fp_alu_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_hive_fp_mul_completed", stat_hive_fp_mul_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_hive_fp_div_completed", stat_hive_fp_div_completed);
	// CIM
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_cim_btw_or_completed", stat_cim_btw_or_completed);
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_cim_btw_and_completed", stat_cim_btw_and_completed);
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_cim_btw_xor_completed", stat_cim_btw_xor_completed);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_cim_eq_cmp_completed", stat_cim_eq_cmp_completed);
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_cim_adr_ld_completed", stat_cim_adr_ld_completed);
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_cim_mem_ld_completed", stat_cim_mem_ld_completed);
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_cim_mem_st_completed", stat_cim_mem_st_completed);
sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_hive_lock_wait_time", stat_min_hive_lock_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_hive_lock_wait_time", stat_max_hive_lock_wait_time);

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_hive_unlock_wait_time", stat_min_hive_unlock_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_hive_unlock_wait_time", stat_max_hive_unlock_wait_time);

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_hive_load_wait_time", stat_min_hive_load_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_hive_load_wait_time", stat_max_hive_load_wait_time);

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_hive_store_wait_time", stat_min_hive_store_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_hive_store_wait_time", stat_max_hive_store_wait_time);

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_hive_nano_load_wait_time", stat_min_hive_nano_load_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_hive_nano_load_wait_time", stat_max_hive_nano_load_wait_time);

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_hive_nano_store_wait_time", stat_min_hive_nano_store_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_hive_nano_store_wait_time", stat_max_hive_nano_store_wait_time);

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_hive_int_alu_wait_time", stat_min_hive_int_alu_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_hive_int_alu_wait_time", stat_max_hive_int_alu_wait_time);

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_hive_int_mul_wait_time", stat_min_hive_int_mul_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_hive_int_mul_wait_time", stat_max_hive_int_mul_wait_time);

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_hive_int_div_wait_time", stat_min_hive_int_div_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_hive_int_div_wait_time", stat_max_hive_int_div_wait_time);
   
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_hive_fp_alu_wait_time", stat_min_hive_fp_alu_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_hive_fp_alu_wait_time", stat_max_hive_fp_alu_wait_time);

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_hive_fp_mul_wait_time", stat_min_hive_fp_mul_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_hive_fp_mul_wait_time", stat_max_hive_fp_mul_wait_time);

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_hive_fp_div_wait_time", stat_min_hive_fp_div_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_hive_fp_div_wait_time", stat_max_hive_fp_div_wait_time);

	//CIM
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_cim_btw_or_wait_time", stat_min_cim_btw_or_wait_time);
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_cim_btw_and_wait_time", stat_min_cim_btw_and_wait_time);
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_cim_btw_xor_wait_time", stat_min_cim_btw_xor_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_min_cim_eq_cmp_wait_time", stat_min_cim_eq_cmp_wait_time);
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_cim_adr_ld_wait_time", stat_max_cim_adr_ld_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_cim_mem_ld_wait_time", stat_max_cim_mem_ld_wait_time);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_max_cim_mem_st_wait_time", stat_max_cim_mem_st_wait_time);
	
    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_hive_lock_wait_time", stat_accumulated_hive_lock_wait_time, stat_hive_lock_completed);
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_hive_unlock_wait_time", stat_accumulated_hive_unlock_wait_time, stat_hive_unlock_completed);
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_hive_load_wait_time", stat_accumulated_hive_load_wait_time, stat_hive_load_completed);
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_hive_store_wait_time", stat_accumulated_hive_store_wait_time, stat_hive_store_completed);

    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_hive_nano_load_wait_time", stat_accumulated_hive_nano_load_wait_time, stat_hive_nano_load_completed);
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_hive_nano_store_wait_time", stat_accumulated_hive_nano_store_wait_time, stat_hive_nano_store_completed);

    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_hive_int_alu_wait_time", stat_accumulated_hive_int_alu_wait_time, stat_hive_int_alu_completed);
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_hive_int_mul_wait_time", stat_accumulated_hive_int_mul_wait_time, stat_hive_int_mul_completed);
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_hive_int_div_wait_time", stat_accumulated_hive_int_div_wait_time, stat_hive_int_div_completed);
    
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_hive_fp_alu_wait_time", stat_accumulated_hive_fp_alu_wait_time, stat_hive_fp_alu_completed);
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_hive_fp_mul_wait_time", stat_accumulated_hive_fp_mul_wait_time, stat_hive_fp_mul_completed);
    sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_hive_fp_div_wait_time", stat_accumulated_hive_fp_div_wait_time, stat_hive_fp_div_completed);
	
	// CIM
	sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_cim_btw_or_wait_time", stat_accumulated_cim_btw_or_wait_time, stat_cim_btw_or_completed);
	sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_cim_btw_and_wait_time", stat_accumulated_cim_btw_and_wait_time, stat_cim_btw_and_completed);
	sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_cim_btw_xor_wait_time", stat_accumulated_cim_btw_xor_wait_time, stat_cim_btw_xor_completed);
	sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_cim_eq_cmp_wait_time", stat_accumulated_cim_eq_cmp_wait_time, stat_cim_eq_cmp_completed);
	sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_cim_adr_ld_wait_time", stat_accumulated_cim_adr_ld_wait_time, stat_cim_adr_ld_completed);
	sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_cim_mem_ld_wait_time", stat_accumulated_cim_mem_ld_wait_time, stat_cim_mem_ld_completed);
	sinuca_engine.write_statistics_value_ratio(get_type_component_label(), get_label(), "stat_accumulated_cim_mem_st_wait_time", stat_accumulated_cim_mem_st_wait_time, stat_cim_mem_st_completed);
	
    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_full_mshr_request_buffer", stat_full_mshr_request_buffer);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_full_mshr_write_buffer", stat_full_mshr_write_buffer);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_full_mshr_prefetch_buffer", stat_full_mshr_prefetch_buffer);

    for (uint32_t i = 0; i < this->channels_per_controller; i++) {
        this->channels[i].print_statistics();
    }
};

// ============================================================================
void memory_controller_t::print_configuration() {
    char title[100] = "";
    snprintf(title, sizeof(title), "Configuration of %s", this->get_label());
    sinuca_engine.write_statistics_big_separator();
    sinuca_engine.write_statistics_comments(title);
    sinuca_engine.write_statistics_big_separator();

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "interconnection_latency", this->get_interconnection_latency());
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "interconnection_width", this->get_interconnection_width());

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "line_size", line_size);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "address_mask_type", get_enum_memory_controller_mask_char(address_mask_type));

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "controller_number", controller_number);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "total_controllers", total_controllers);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "channels_per_controller", channels_per_controller);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "bank_per_channel", bank_per_channel);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "bank_buffer_size", bank_buffer_size);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "bank_row_buffer_size", bank_row_buffer_size);

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "bank_selection_policy", get_enum_selection_char(bank_selection_policy));
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "request_priority_policy", get_enum_request_priority_char(request_priority_policy));
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "write_priority_policy", get_enum_write_priority_char(write_priority_policy));

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "mshr_buffer_size", mshr_buffer_size);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "mshr_request_buffer_size", mshr_request_buffer_size);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "mshr_write_buffer_size", mshr_write_buffer_size);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "mshr_prefetch_buffer_size", mshr_prefetch_buffer_size);

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "higher_level_request_tokens", higher_level_request_tokens);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "higher_level_prefetch_tokens", higher_level_prefetch_tokens);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "higher_level_write_tokens", higher_level_write_tokens);


    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "burst_length", burst_length);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "core_to_bus_clock_ratio", core_to_bus_clock_ratio);

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_burst", timing_burst);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_al", timing_al);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_cas", timing_cas);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_ccd", timing_ccd);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_cwd", timing_cwd);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_faw", timing_faw);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_ras", timing_ras);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_rc", timing_rc);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_rcd", timing_rcd);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_rp", timing_rp);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_rrd", timing_rrd);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_rtp", timing_rtp);

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_wr", timing_wr);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_wtr", timing_wtr);

    // HMC
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "hmc_size", hmc_size);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "hmc_latency_roa", hmc_latency_roa);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "hmc_latency_rowa", hmc_latency_rowa);

    // HIVE
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "hive_operation_size", hive_operation_size);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "hive_total_registers", hive_total_registers);

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "hive_latency_int_alu", hive_latency_int_alu);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "hive_latency_int_mul", hive_latency_int_mul);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "hive_latency_int_div", hive_latency_int_div);
    
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "hive_latency_fp_alu", hive_latency_fp_alu);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "hive_latency_fp_mul", hive_latency_fp_mul);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "hive_latency_fp_div", hive_latency_fp_div);
	
	// CIM
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "cim_latency_btw_or", cim_latency_btw_or);
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "cim_latency_btw_and", cim_latency_btw_and);
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "cim_latency_btw_xor", cim_latency_btw_xor);
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "cim_latency_eq_cmp", cim_latency_eq_cmp);
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "cim_latency_adr_ld", cim_latency_adr_ld);
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "cim_latency_mem_ld", cim_latency_mem_ld);
	sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "cim_latency_mem_st", cim_latency_mem_st);

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "row_bits_mask", utils_t::address_to_binary(this->row_bits_mask).c_str());
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "bank_bits_mask", utils_t::address_to_binary(this->bank_bits_mask).c_str());
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "channel_bits_mask", utils_t::address_to_binary(this->channel_bits_mask).c_str());
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "controller_bits_mask", utils_t::address_to_binary(this->controller_bits_mask).c_str());
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "colrow_bits_mask", utils_t::address_to_binary(this->colrow_bits_mask).c_str());
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "colbyte_bits_mask", utils_t::address_to_binary(this->colbyte_bits_mask).c_str());


    for (uint32_t i = 0; i < this->channels_per_controller; i++) {
        this->channels[i].print_configuration();
    }
};

