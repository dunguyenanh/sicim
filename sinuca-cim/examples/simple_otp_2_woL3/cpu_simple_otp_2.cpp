#include <iostream>
#include <sstream>
#include <fstream>
//#include "../hmc.hpp"

using namespace std;

#define SIZEOFMESSAGELINE 4096
/*
char * encryptDecrypt(char * toEncrypt) {
    char * key = new char[SIZEOFMESSAGELINE];
	key[0] = {'T'}; //Any chars will work, in an array of any size
	key[SIZEOFMESSAGELINE-1] = {'K'};
	key[SIZEOFMESSAGELINE-int(0.5*SIZEOFMESSAGELINE)] = {'H'};
    char * output = new char[SIZEOFMESSAGELINE];
	output = toEncrypt;
	//output = toEncrypt;
    
    for (int i = 0; i < SIZEOFMESSAGELINE; i++) //because size of char is 1
        output[i] = toEncrypt[i] ^ key[i];
		    //output[i] = toEncrypt[i] ^ key[i % (sizeof(key) / sizeof(char))];
    
    return output;
}*/

int main(int argc, const char * argv[])
{	
	// Check the number of parameters
	if (argc < 3) {
	// Tell the user how to run the program
	std::cerr << "Usage: " << argv[0] << " OP_SIZE" << " PROB_SIZE" << std::endl;
	/* "Usage messages" are a conventional way of telling the user
	* how to run a program if they enter the command incorrectly.
	*/
	return -1;
	}  
	
	int op_size = 128 ; //byte = 1024bit
	sscanf ( argv[1], "%d", &op_size );
	//int op_size_bit = op_size * 8;
	//cout << "operation size:" << op_size_bit<<std::endl;
	int prob_size = 1024; //bytes
	sscanf ( argv[2], "%d", &prob_size );
	//cout << "problem size:" << prob_size<<std::endl;
	int message_size = 1024; //bytes=1KB file size
	int loop_size = int(prob_size/message_size);
	//cout << "loop size:" << loop_size<<std::endl;
  
	char message[SIZEOFMESSAGELINE];
	char * encrypted = new char[SIZEOFMESSAGELINE];
	char * decrypted = new char[SIZEOFMESSAGELINE];	
	//cout << "--Start here:" << "\n";
	
	char * key = new char[SIZEOFMESSAGELINE];
	key[0] = {'T'}; //Any chars will work, in an array of any size
	key[SIZEOFMESSAGELINE-1] = {'K'};
	key[SIZEOFMESSAGELINE-int(0.5*SIZEOFMESSAGELINE)] = {'H'};
	
	for ( int j = 0; j < loop_size; j++)
	{
		ifstream myfile ("message.txt");
		if (myfile.is_open())
		{
			cout << "my file is open\n";
			//cout << "Please enter the message to be encrypted\n";
			//cin >> message;
			while ( myfile.getline(message, SIZEOFMESSAGELINE) )
			{
				int i;
				for (i = 0; i < SIZEOFMESSAGELINE; i++)
					//or, xor, and have the same cost
					// to do: add also xor, and, and other instr into trace gen
					encrypted[i] = message[i] ^ key[i];
				cout << "--Encrypted " << encrypted << "\n";
				for (i = 0; i < SIZEOFMESSAGELINE; i++)
					decrypted[i] = encrypted[i] ^ key[i];
				cout << "--Decrypted " << decrypted << "\n";
			}
		}
		else
		{
			cout << "my file is not open\n";
		}
	}
    
    return 0;
}