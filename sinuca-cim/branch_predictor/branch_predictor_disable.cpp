/*
 * Copyright (C) 2009~2017  Marco Antonio Zanata Alves
 *                          (mazalves at inf.ufrgs.br)
 *                          GPPD - Parallel and Distributed Processing Group
 *                          Universidade Federal do Rio Grande do Sul
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../sinuca.hpp"

#ifdef BRANCH_PREDICTOR_DEBUG
    #define BRANCH_PREDICTOR_DEBUG_PRINTF(...) DEBUG_PRINTF(__VA_ARGS__);
#else
    #define BRANCH_PREDICTOR_DEBUG_PRINTF(...)
#endif

// ============================================================================
branch_predictor_disable_t::branch_predictor_disable_t() {
    this->branch_predictor_type = BRANCH_PREDICTOR_DISABLE;
};

// ============================================================================
branch_predictor_disable_t::~branch_predictor_disable_t() {
    /// De-Allocate memory to prevent memory leak
};

// ============================================================================
void branch_predictor_disable_t::allocate() {
    branch_predictor_t::allocate();
};

// ============================================================================
/// Always consider as it is NOT_TAKEN
processor_stage_t branch_predictor_disable_t::predict_branch(const opcode_package_t& actual_opcode, uint64_t next_opcode_address) {
    processor_stage_t solve_stage = PROCESSOR_STAGE_FETCH;

    if (actual_opcode.opcode_operation != INSTRUCTION_OPERATION_BRANCH) {
        solve_stage = PROCESSOR_STAGE_FETCH;
    }
    else {
        this->add_stat_branch_predictor_operation();

        bool predicted_direction_is_taken = false;
        uint64_t predicted_target_address = 0;

        uint64_t next_sequential_address = actual_opcode.opcode_address + actual_opcode.opcode_size;
        bool next_real_direction_is_taken = (next_sequential_address != next_opcode_address);


        switch (actual_opcode.branch_type) {
            // Conditional, 2 targets, Solve on Execution
            case BRANCH_COND:
                add_stat_branch_predictor_conditional();
                if (next_real_direction_is_taken) {
                    add_stat_branch_predictor_conditional_taken();
                }
                else {
                    add_stat_branch_predictor_conditional_not_taken();
                }

                // Direction Miss Prediction
                if (predicted_direction_is_taken != next_real_direction_is_taken) {
                    add_stat_branch_predictor_conditional_miss();
                    solve_stage = PROCESSOR_STAGE_EXECUTION;
                }
                // Direction Correct Prediction
                // If Target Miss Prediction
                else if(predicted_target_address == 0) {
                    add_stat_branch_predictor_conditional_address_miss();
                    solve_stage = PROCESSOR_STAGE_DECODE;
                }
            break;

            // Syscall, 1 target, Solve on Fetch
            case BRANCH_SYSCALL:
                add_stat_branch_predictor_syscall();
                solve_stage = PROCESSOR_STAGE_EXECUTION;    // Add some extra latency (we do not trace syscalls)
            break;

            // Unconditional: Direct, 1 target, Solve on Decode
            // Unconditional: Indirect, N targets, Solve on Execution
            case BRANCH_UNCOND:
                if (actual_opcode.is_indirect) {
                    add_stat_branch_predictor_indirect();
                    if (predicted_target_address != next_opcode_address) {
                        add_stat_branch_predictor_indirect_miss();
                        solve_stage = PROCESSOR_STAGE_EXECUTION;
                    }
                }
                else {
                    add_stat_branch_predictor_unconditional();
                    if (predicted_target_address != next_opcode_address) {
                        add_stat_branch_predictor_unconditional_miss();
                        solve_stage = PROCESSOR_STAGE_DECODE;
                    }
                }
            break;

            // Call: Direct, 1 target, Solve on Decode
            case BRANCH_CALL:
                add_stat_branch_predictor_call();
                if (predicted_target_address != next_opcode_address) {
                    add_stat_branch_predictor_call_miss();
                    solve_stage = PROCESSOR_STAGE_DECODE;
                }
            break;

            // Return: Indirect, N targets, Solve on Execution
            case BRANCH_RETURN:
                add_stat_branch_predictor_return();
                if (predicted_target_address != next_opcode_address) {
                    add_stat_branch_predictor_return_miss();
                    solve_stage = PROCESSOR_STAGE_EXECUTION;
                }
            break;
        };
    }


    return solve_stage;
};


// ============================================================================
void branch_predictor_disable_t::print_structures() {
    branch_predictor_t::print_structures();
};

// ============================================================================
void branch_predictor_disable_t::panic() {
    branch_predictor_t::panic();
    this->print_structures();
};

// ============================================================================
void branch_predictor_disable_t::periodic_check(){
    branch_predictor_t::periodic_check();
    #ifdef BRANCH_PREDICTOR_DEBUG
        this->print_structures();
    #endif
};

// ============================================================================
/// STATISTICS
// ============================================================================
void branch_predictor_disable_t::reset_statistics() {
    branch_predictor_t::reset_statistics();
};

// ============================================================================
void branch_predictor_disable_t::print_statistics() {
    branch_predictor_t::print_statistics();
};

// ============================================================================
// ============================================================================
void branch_predictor_disable_t::print_configuration() {
    branch_predictor_t::print_configuration();
};
