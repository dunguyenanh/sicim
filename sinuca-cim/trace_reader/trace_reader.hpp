/*
 * Copyright (C) 2009~2017  Marco Antonio Zanata Alves
 *                          (mazalves at inf.ufrgs.br)
 *                          GPPD - Parallel and Distributed Processing Group
 *                          Universidade Federal do Rio Grande do Sul
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/// Trace Reader
// ============================================================================
 /*! Read the instruction/memory trace files
  * Open the static trace => Dictionary of instruction per Basic Block
  * Open the dynamic trace(s) => Reads the threads Basic Block execution trace
  * Open the memory trace(s) => Match the memory addresses for R/W Instructions
  */
class trace_reader_t {
    private:
        gzFile *gzStaticTraceFile;
        gzFile *gzDynamicTraceFile;
        gzFile *gzMemoryTraceFile;

        /// Control the trace reading
        bool *is_inside_bbl;
        uint32_t *currect_bbl;
        uint32_t *currect_opcode;
        uint64_t *current_target_address;

        uint64_t *trace_opcode_counter;
        uint64_t *trace_opcode_max;
        uint64_t trace_opcode_total;

        uint32_t total_cores;
        uint32_t total_groups;

        uint32_t *group_binary_total_bbls;   /// Total of BBLs for each binary
        uint32_t **group_binary_bbl_size;    /// Total of instructions for each BBL for each binary
        opcode_package_t ***group_binary;

        uint32_t *core_to_group_translation;
        uint64_t *group_address_translation;


    public:
        // ====================================================================
        /// Methods
        // ====================================================================
        trace_reader_t();
        ~trace_reader_t();
        void allocate(uint32_t total_traces, char **trace_files, uint32_t number_cores);  /// must be called after the parameters are set properly
        inline const char* get_label() {
            return "TRACE_READER";
        };
        inline const char* get_type_component_label() {
            return "TRACE_READER";
        };

        void define_group_binary_total_bbls(uint32_t group);
        void define_group_binary_bbl_size(uint32_t group);
        void define_trace_size(uint32_t cpuid);

        void generate_group_binary(uint32_t group);
        void check_group_binary(uint32_t group);

        uint32_t trace_next_dynamic(uint32_t cpuid, sync_t *sync_found);
        void trace_next_memory(uint32_t cpuid, char *memory_line);
        bool trace_fetch(uint32_t cpuid, opcode_package_t *m);

        bool is_part_of_group(uint32_t cpu_a, uint32_t cpu_b){ return this->core_to_group_translation[cpu_a] == this->core_to_group_translation[cpu_b];}

        /// Progress
        uint64_t get_trace_opcode_counter(uint32_t cpuid) { return this->trace_opcode_counter[cpuid]; }
        uint64_t get_trace_opcode_max(uint32_t cpuid) { return this->trace_opcode_max[cpuid]; }
        INSTANTIATE_GET_SET_ADD(uint64_t, trace_opcode_total);
};
