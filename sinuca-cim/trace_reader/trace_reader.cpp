/*
 * Copyright (C) 2009~201  Marco Antonio Zanata Alves
 *                          (mazalves at inf.ufrgs.br)
 *                          GPPD - Parallel and Distributed Processing Group
 *                          Universidade Federal do Rio Grande do Sul
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../sinuca.hpp"
#include <string>

#ifdef TRACE_READER_DEBUG
    #define TRACE_READER_DEBUG_PRINTF(...) DEBUG_PRINTF(__VA_ARGS__);
#else
    #define TRACE_READER_DEBUG_PRINTF(...)
#endif

// =============================================================================
trace_reader_t::trace_reader_t() {
    this->gzStaticTraceFile = NULL;
    this->gzDynamicTraceFile = NULL;
    this->gzMemoryTraceFile = NULL;

    this->total_cores = 0;
    this->total_groups = 0;

    this->trace_opcode_counter = NULL;
    this->trace_opcode_max = NULL;
    this->trace_opcode_total = 0;

    this->group_binary_total_bbls = NULL;
    this->group_binary_bbl_size = NULL;

    this->is_inside_bbl = NULL;
    this->currect_bbl = NULL;
    this->currect_opcode = NULL;
};

// =============================================================================
trace_reader_t::~trace_reader_t() {

    /// Close trace files
    if (this->gzStaticTraceFile != NULL) {
        for (uint32_t group = 0; group < this->total_groups; group++) {
            gzclose(gzStaticTraceFile[group]);
        }
    }

    if (this->gzDynamicTraceFile != NULL && this->gzMemoryTraceFile != NULL) {
        for (uint32_t core = 0; core < this->total_cores; core++) {
            gzclose(gzDynamicTraceFile[core]);
            gzclose(gzMemoryTraceFile[core]);
        }
    }

    utils_t::template_delete_array<gzFile>(gzStaticTraceFile);
    utils_t::template_delete_array<gzFile>(gzDynamicTraceFile);
    utils_t::template_delete_array<gzFile>(gzMemoryTraceFile);

    for (uint32_t group = 0; group < this->total_groups; group++) {
        if (this->group_binary_total_bbls[group] != 0 && group_binary[group] != NULL ) {
            for (uint32_t bbl = 1; bbl < this->group_binary_total_bbls[group]; bbl++) {
                utils_t::template_delete_array<opcode_package_t>(this->group_binary[group][bbl]);
            }
            utils_t::template_delete_array<opcode_package_t*>(this->group_binary[group]);
        }
        utils_t::template_delete_array<uint32_t>(group_binary_bbl_size[group]);
    }
    utils_t::template_delete_array<opcode_package_t**>(group_binary);
    utils_t::template_delete_array<uint32_t*>(group_binary_bbl_size);
    utils_t::template_delete_array<uint32_t>(group_binary_total_bbls);

    utils_t::template_delete_array<bool>(is_inside_bbl);
    utils_t::template_delete_array<uint64_t>(trace_opcode_counter);
    utils_t::template_delete_array<uint64_t>(trace_opcode_max);

    utils_t::template_delete_array<uint32_t>(currect_bbl);
    utils_t::template_delete_array<uint32_t>(currect_opcode);
};

// =============================================================================
void trace_reader_t::allocate(uint32_t total_files, char **trace_files, uint32_t number_cores) {

    this->total_cores = number_cores;
    this->total_groups = total_files;

    uint32_t core_id = 0;
    uint32_t affinity_core_id = 0;


    this->group_address_translation = utils_t::template_allocate_array<uint64_t>(this->total_groups);
    for (uint64_t group = 0; group < this->total_groups; group++) {
        this->group_address_translation[group] = group << 56;
    }


    // =======================================================================
    // Control the cores without trace
    bool need_fake_trace[MAX_CORES];
    for (core_id = 0; core_id < this->total_cores; core_id++) {
        need_fake_trace[core_id] = true;
    }

    // =======================================================================
    // Check the Affinity
    uint32_t check_affinity[MAX_CORES];

    for (core_id = 0; core_id < this->total_cores; core_id++) {
        check_affinity[core_id] = 0;
    }

    for (core_id = 0; core_id < this->total_cores; core_id++) {
        affinity_core_id = sinuca_engine.thread_affinity[core_id];
        check_affinity[affinity_core_id]++;
        ERROR_ASSERT_PRINTF(check_affinity[affinity_core_id] == 1, "Affinity set twice for core_id:%u\n", affinity_core_id)
    }

    char file_name[500];
    struct stat st;

    // =======================================================================
    // Static Trace Files
    // =======================================================================
    this->gzStaticTraceFile = utils_t::template_allocate_array<gzFile>(this->total_groups);

    this->group_binary_total_bbls = utils_t::template_allocate_initialize_array<uint32_t>(this->total_groups, 0);
    this->group_binary_bbl_size = utils_t::template_allocate_array<uint32_t*>(this->total_groups);
    this->group_binary = utils_t::template_allocate_initialize_array<opcode_package_t**>(this->total_groups, NULL);

    for (uint32_t group = 0; group < this->total_groups; group++) {
        SINUCA_PRINTF("GROUP: %02u - TRACE: %s\n", group, trace_files[group]);

        file_name[0] = '\0';
        snprintf(file_name, sizeof(file_name), "%s.tid%d.stat.out.gz", trace_files[group], 0);

        gzStaticTraceFile[group] = gzopen(file_name, "ro");    /// Open the .gz file
        ERROR_ASSERT_PRINTF(gzStaticTraceFile[group] != NULL, "Could not open the file.\n%s\n", file_name);
        TRACE_READER_DEBUG_PRINTF("Static File = %s => READY !\n", file_name);



        /// Obtain the total of BBLs
        this->define_group_binary_total_bbls(group);
        this->group_binary_bbl_size[group] = utils_t::template_allocate_initialize_array<uint32_t>(this->group_binary_total_bbls[group], 0);
        /// Obtain the total of Opcodes per BBL
        this->define_group_binary_bbl_size(group);

        /// Allocate only the required space for the static file packages
        this->group_binary[group] = utils_t::template_allocate_initialize_array<opcode_package_t*>(this->group_binary_total_bbls[group], NULL);
        for (uint32_t bbl = 1; bbl < this->group_binary_total_bbls[group]; bbl++) {
            this->group_binary[group][bbl] = utils_t::template_allocate_array<opcode_package_t>(this->group_binary_bbl_size[group][bbl]);
        }

        this->generate_group_binary(group);
    }

    // =======================================================================
    // Dynamic and Memory Trace Files
    // =======================================================================
    this->gzDynamicTraceFile = utils_t::template_allocate_array<gzFile>(this->total_cores);
    this->gzMemoryTraceFile = utils_t::template_allocate_array<gzFile>(this->total_cores);

    this->core_to_group_translation = utils_t::template_allocate_initialize_array<uint32_t>(this->total_cores, 0);

    core_id = 0;
    affinity_core_id = 0;

    for (uint32_t group = 0; group < this->total_groups; group++) {
        /// Find all the (dyn/mem) threads for the given file
        for (uint32_t thread_id = 0; thread_id < this->total_cores; thread_id++, core_id++) {
            /// Get the thread affinity
            affinity_core_id = sinuca_engine.thread_affinity[core_id];
            ERROR_ASSERT_PRINTF(affinity_core_id <= this->total_cores, "Affinity set to a core > total_cores\n")

            /// Set the cores part of each group
            this->core_to_group_translation[affinity_core_id] = group;

            // =======================================================================
            // Dynamic Trace Files
            // =======================================================================
            file_name[0] = '\0';
            snprintf(file_name, sizeof(file_name), "%s.tid%d.dyn.out.gz", trace_files[group], thread_id);

            /// Check if the trace is formed by more threads
            if (stat(file_name, &st) != false)
                break;

            this->gzDynamicTraceFile[affinity_core_id] = gzopen(file_name, "ro");    /// Open the .gz group
            ERROR_ASSERT_PRINTF(this->gzDynamicTraceFile[core_id] != NULL, "Could not open the file.\n%s\n", file_name);
            TRACE_READER_DEBUG_PRINTF("Dynamic File = %s => READY !\n", file_name);

            // =======================================================================
            // Memory Trace Files
            // =======================================================================
            file_name[0] = '\0';
            snprintf(file_name, sizeof(file_name), "%s.tid%d.mem.out.gz", trace_files[group], thread_id);

            /// In case dynamic group exists the memory must exist as well
            this->gzMemoryTraceFile[affinity_core_id] = gzopen(file_name, "ro");    /// Open the .gz group
            ERROR_ASSERT_PRINTF(this->gzMemoryTraceFile[affinity_core_id] != NULL, "Could not open the file.\n%s\n", file_name);
            TRACE_READER_DEBUG_PRINTF("Memory File = %s => READY !\n", file_name);

            need_fake_trace[affinity_core_id] = false;
        }
    }

    /// Create fake traces for unused cores
    for (uint32_t core_id = 0; core_id < this->total_cores; core_id++) {
        if (need_fake_trace[core_id]) {

            /// Set the cores part of each group
            this->core_to_group_translation[core_id] = total_groups - 1; /// Force to the last group

            // Dynamic Trace Files
            snprintf(file_name, sizeof(file_name), "/tmp/NULL_tid%d.dyn.out.gz", core_id);
            this->gzDynamicTraceFile[core_id] = gzopen(file_name, "wo");    /// Create the .gz file
            gzwrite(this->gzDynamicTraceFile[core_id], "#Empty Trace\n", strlen("#Empty Trace\n"));
            gzclose(this->gzDynamicTraceFile[core_id]);

            WARNING_PRINTF("=> CREATED %s.\n", file_name);
            this->gzDynamicTraceFile[core_id] = gzopen(file_name, "ro");    /// Open the .gz file

            // Memory Trace Files
            snprintf(file_name, sizeof(file_name), "/tmp/NULL_tid%d.mem.out.gz", core_id);
            this->gzMemoryTraceFile[core_id] = gzopen(file_name, "wo");    /// Create the .gz file
            gzwrite(this->gzMemoryTraceFile[core_id], "#Empty Trace\n", strlen("#Empty Trace\n"));
            gzclose(this->gzMemoryTraceFile[core_id]);

            WARNING_PRINTF("=> CREATED %s.\n", file_name);
            this->gzMemoryTraceFile[core_id] = gzopen(file_name, "ro");    /// Open the .gz file
        }
    }


    this->trace_opcode_counter = utils_t::template_allocate_initialize_array<uint64_t>(this->total_cores, 1);
    this->trace_opcode_max = utils_t::template_allocate_array<uint64_t>(this->total_cores);
    for (uint32_t core_id = 0; core_id < this->total_cores; core_id++) {
        SINUCA_PRINTF("Loading trace for CPU:%d\n", core_id);
        this->define_trace_size(core_id);
    }

    /// Allocate the trace_reader controls
    this->is_inside_bbl = utils_t::template_allocate_initialize_array<bool>(this->total_cores, false);
    this->currect_bbl = utils_t::template_allocate_initialize_array<uint32_t>(this->total_cores, 0);
    this->currect_opcode = utils_t::template_allocate_initialize_array<uint32_t>(this->total_cores, 0);
};

// =============================================================================
void trace_reader_t::define_group_binary_total_bbls(uint32_t group) {
    char file_line[TRACE_LINE_SIZE] = "";
    bool file_eof = false;
    uint32_t bbl = 0;
    this->group_binary_total_bbls[group] = 0;

    gzclearerr(this->gzStaticTraceFile[group]);
    gzseek(this->gzStaticTraceFile[group], 0, SEEK_SET);   /// Go to the Begin of the File
    file_eof = gzeof(this->gzStaticTraceFile[group]);      /// Check is file not EOF
    ERROR_ASSERT_PRINTF(!file_eof, "Static File Unexpected EOF.\n")

    while (!file_eof) {
        gzgets(this->gzStaticTraceFile[group], file_line, TRACE_LINE_SIZE);
        file_eof = gzeof(this->gzStaticTraceFile[group]);

        if (file_line[0] == '@') {
            bbl = (uint32_t)strtoul(file_line + 1, NULL, 10);
            this->group_binary_total_bbls[group]++;
            ERROR_ASSERT_PRINTF(bbl == this->group_binary_total_bbls[group], "Expected sequenced bbls.\n")
        }
    }

    this->group_binary_total_bbls[group]++;
    TRACE_READER_DEBUG_PRINTF("Total of bbls %" PRIu32 "\n", this->group_binary_total_bbls[group]);
};

// =============================================================================
void trace_reader_t::define_group_binary_bbl_size(uint32_t group) {
    char file_line[TRACE_LINE_SIZE] = "";
    bool file_eof = false;
    uint32_t bbl = 0;

    gzclearerr(this->gzStaticTraceFile[group]);
    gzseek(this->gzStaticTraceFile[group], 0, SEEK_SET);   /// Go to the Begin of the File
    file_eof = gzeof(this->gzStaticTraceFile[group]);      /// Check is file not EOF
    ERROR_ASSERT_PRINTF(!file_eof, "Static File Unexpected EOF.\n")

    while (!file_eof) {
        gzgets(this->gzStaticTraceFile[group], file_line, TRACE_LINE_SIZE);
        file_eof = gzeof(this->gzStaticTraceFile[group]);

        if (file_line[0] == '\0' || file_line[0] == '#') {     /// If Comment, then ignore
            continue;
        }
        else if (file_line[0] == '@') {
            bbl++;
            this->group_binary_bbl_size[group][bbl] = 0;
        }
        else {
            this->group_binary_bbl_size[group][bbl]++;
        }
    }
};

// =============================================================================
/// Get the total number of opcodes
void trace_reader_t::define_trace_size(uint32_t cpuid) {
    char file_line[TRACE_LINE_SIZE] = "";
    bool file_eof = false;
    uint32_t bbl = 0;
    uint32_t group = this->core_to_group_translation[cpuid];
    this->trace_opcode_max[cpuid] = 0;

    gzclearerr(this->gzDynamicTraceFile[cpuid]);
    gzseek(this->gzDynamicTraceFile[cpuid], 0, SEEK_SET);   /// Go to the Begin of the File
    file_eof = gzeof(this->gzDynamicTraceFile[cpuid]);      /// Check is file not EOF
    ERROR_ASSERT_PRINTF(!file_eof, "Dynamic File Unexpected EOF.\n")

    while (!file_eof) {
        gzgets(this->gzDynamicTraceFile[cpuid], file_line, TRACE_LINE_SIZE);
        file_eof = gzeof(this->gzDynamicTraceFile[cpuid]);

        if (file_line[0] != '\0' && file_line[0] != '#' && file_line[0] != '$') {
            bbl = (uint32_t)strtoul(file_line, NULL, 10);
            ERROR_ASSERT_PRINTF(bbl < this->group_binary_total_bbls[group], "Dynamic BBL bigger than Static BBL\n")
            ERROR_ASSERT_PRINTF(bbl != 0 , "Dynamic BBL equal to zero\n")
            ERROR_ASSERT_PRINTF(this->group_binary_bbl_size[group][bbl] != 0, "Static BBL size equal to zero\n")
            this->trace_opcode_max[cpuid] += this->group_binary_bbl_size[group][bbl];
        }
    }

    gzclearerr(this->gzDynamicTraceFile[cpuid]);            /// Go to the Begin of the File
    gzseek(this->gzDynamicTraceFile[cpuid], 0, SEEK_SET);
};


// =============================================================================
void trace_reader_t::generate_group_binary(uint32_t group) {
    TRACE_READER_DEBUG_PRINTF("Static_dict generator for cpu:%d started\n", group);

    char file_line[TRACE_LINE_SIZE] = "";
    bool file_eof = false;
    uint32_t BBL = 0;                           /// Actual BBL (Index of the Vector)
    uint32_t opcode = 0;
    opcode_package_t NewOpcode;                 /// Actual Opcode

    gzclearerr(this->gzStaticTraceFile[group]);
    gzseek(this->gzStaticTraceFile[group], 0, SEEK_SET);  /// Go to the Begin of the File
    file_eof = gzeof(this->gzStaticTraceFile[group]);      /// Check is file not EOF
    ERROR_ASSERT_PRINTF(!file_eof, "Static File Unexpected EOF.\n")

    while (!file_eof) {
        gzgets(this->gzStaticTraceFile[group], file_line, TRACE_LINE_SIZE);
        file_eof = gzeof(this->gzStaticTraceFile[group]);

        TRACE_READER_DEBUG_PRINTF("Read: %s\n", file_line);
        if (file_line[0] == '\0' || file_line[0] == '#') {     /// If Comment, then ignore
            continue;
        }
        else if (file_line[0] == '@') {                       /// If New BBL
            TRACE_READER_DEBUG_PRINTF("BBL %u with %u instructions.\n", BBL, opcode);

            opcode = 0;
            BBL = (uint32_t)strtoul(file_line + 1, NULL, 10);
            ERROR_ASSERT_PRINTF(BBL < this->group_binary_total_bbls[group], "Static has more BBLs than previous analyzed static file.\n");
        }
        else {                                                  /// If Inside BBL
            NewOpcode.trace_string_to_opcode(file_line);
            ERROR_ASSERT_PRINTF(NewOpcode.opcode_address != 0, "Static trace file generating opcode address equal to zero.\n")
            this->group_binary[group][BBL][opcode++] = NewOpcode;                 /// Save the new opcode
        }
    }

    this->check_group_binary(group);
};

// =============================================================================
/// Check if the map was successfully generated
void trace_reader_t::check_group_binary(uint32_t group) {
    TRACE_READER_DEBUG_PRINTF("Check for group_binary cpu:%d started\n", group);

    char file_line[TRACE_LINE_SIZE] = "";
    char aux_line[TRACE_LINE_SIZE] = "";
    bool file_eof = false;
    opcode_package_t trace_opcode, dict_opcode;  /// Actual Opcode

    gzclearerr(this->gzStaticTraceFile[group]);
    gzseek(this->gzStaticTraceFile[group], 0, SEEK_SET);   /// Go to the Begin of the File
    file_eof = gzeof(this->gzStaticTraceFile[group]);      /// Check is file not EOF
    ERROR_ASSERT_PRINTF(!file_eof, "Static File Unexpected EOF.\n")

    for (uint32_t bbl = 1; bbl < this->group_binary_total_bbls[group]; bbl++) {
        ERROR_ASSERT_PRINTF(this->group_binary_bbl_size[group][bbl], "BBL[%d] has no instruction inside.\n", bbl);

        for (uint32_t j = 0; j < this->group_binary_bbl_size[group][bbl]; j++) {
            dict_opcode = this->group_binary[group][bbl][j];

            while (true) {
                gzgets(this->gzStaticTraceFile[group], file_line, TRACE_LINE_SIZE);
                file_eof = gzeof(this->gzStaticTraceFile[group]);
                ERROR_ASSERT_PRINTF(!file_eof, "Static File Smaller than StaticDict.\n")

                if (file_line[0] != '\0' && file_line[0] != '#' && file_line[0] != '@') {
                    break;
                }
            }
            trace_opcode.trace_string_to_opcode(file_line);
            dict_opcode.opcode_to_trace_string(aux_line);
            ERROR_ASSERT_PRINTF(trace_opcode == dict_opcode, "Input and Output Variable are different\n In: [%s]\n Out: [%s]\n ", file_line, aux_line)
        }
    }

    file_eof = gzeof(this->gzStaticTraceFile[group]);
    while (!file_eof) {
        gzgets(this->gzStaticTraceFile[group], file_line, TRACE_LINE_SIZE);
        file_eof = gzeof(this->gzStaticTraceFile[group]);

        if (file_line[0] != '\0' && file_line[0] != '#' && file_line[0] != '@') {
            ERROR_ASSERT_PRINTF(file_eof, "Map is smaller than file.\n %s \n", file_line);
        }
    }
};


// =============================================================================
uint32_t trace_reader_t::trace_next_dynamic(uint32_t cpuid, sync_t *new_sync) {
    char file_line[TRACE_LINE_SIZE] = "";
    uint32_t BBL = 0;

    bool valid_dynamic = false;

    while (!valid_dynamic) {
        if (gzeof(this->gzDynamicTraceFile[cpuid])) {
            sinuca_engine.set_is_processor_trace_eof(cpuid);
            return FAIL;
        }
        gzgets(this->gzDynamicTraceFile[cpuid], file_line, TRACE_LINE_SIZE);

        if (file_line[0] == '\0' || file_line[0] == '#') {
            TRACE_READER_DEBUG_PRINTF("cpu[%d] - %s\n", cpuid, file_line);
            continue;
        }
        else if (file_line[0] == '$') {
            *new_sync = (sync_t)strtoul(file_line + 1, NULL, 10);
            return FAIL;
        }
        else {
            /// BBL is always greater than 0
            /// If strtoul==0 the line could not be converted.
            TRACE_READER_DEBUG_PRINTF("cpu[%" PRIu32 "] dynamic line = %s\n", cpuid, file_line);

            BBL = utils_t::string_to_uint32(file_line);
            ERROR_ASSERT_PRINTF(BBL != 0, "First BBL from the dynamic trace file should be zero. Dynamic line %s\n", file_line);

            if (BBL != 0) {
                valid_dynamic = true;
            }
        }
    }
    return BBL;
};

// =============================================================================
void trace_reader_t::trace_next_memory(uint32_t cpuid, char *memory_line) {

    bool valid_memory = false;
    memory_line[0] = '\0';

    while (!valid_memory) {
        ERROR_ASSERT_PRINTF(!gzeof(this->gzMemoryTraceFile[cpuid]), "MemoryTraceFile EOF - cpu id %d\n", cpuid);
        char *buffer = gzgets(this->gzMemoryTraceFile[cpuid], memory_line, TRACE_LINE_SIZE);
        ERROR_ASSERT_PRINTF(buffer != NULL, "MemoryTraceFile EOF - cpu id %" PRIu32 "\n", cpuid);

        if (strlen(memory_line) != 0 && memory_line[0] != '#') {
            valid_memory = true;
        }
    }
};

// =============================================================================
bool trace_reader_t::trace_fetch(uint32_t cpuid, opcode_package_t *m) {
    opcode_package_t NewOpcode;
    sync_t sync_found = SYNC_FREE;
    uint32_t group = this->core_to_group_translation[cpuid];

    /// Spawn Warmup
    if (trace_opcode_total == sinuca_engine.arg_warmup_instructions) {
        /// Next cycle all the statistics will be reset
        sinuca_engine.set_is_warmup(true);
    }

    /// Spawn Stopat
    if (sinuca_engine.arg_stopat_instructions != 0 && trace_opcode_total == sinuca_engine.arg_stopat_instructions) {
        /// All the cores will achieve the EOF
        sinuca_engine.set_is_processor_trace_eof(cpuid);
        return FAIL;
    }

    // =========================================================================
    /// Fetch new BBL inside the dynamic file.
    // =========================================================================
    if (!this->is_inside_bbl[cpuid]) {
        uint32_t new_BBL = this->trace_next_dynamic(cpuid, &sync_found);
        if (new_BBL == FAIL) {
            if (sync_found == SYNC_FREE) {      /// BBL=Fail Sync=FREE ==> EOF
                return FAIL;
            }
            else {                              /// BBL=Fail Sync=OTHER ==> SYNC
                /// SINUCA control variables.
                m->opcode_operation = INSTRUCTION_OPERATION_NOP;
                m->state = PACKAGE_STATE_UNTREATED;
                m->born_cycle = sinuca_engine.get_global_cycle();
                m->ready_cycle = sinuca_engine.get_global_cycle();
                m->opcode_number = trace_opcode_counter[cpuid];
                m->sync_type = sync_found;
                return OK;
            }
        }
        else {
            this->currect_bbl[cpuid] = new_BBL;
            this->currect_opcode[cpuid] = 0;
            this->is_inside_bbl[cpuid] = true;
        }
    }

    // =========================================================================
    /// Fetch new INSTRUCTION inside the static file.
    // =========================================================================
    NewOpcode = this->group_binary[group][this->currect_bbl[cpuid]][this->currect_opcode[cpuid]];
    TRACE_READER_DEBUG_PRINTF("CPU:%u  BBL:%u  OPCODE:%u = %s\n",cpuid, this->currect_bbl[cpuid], this->currect_opcode[cpuid], NewOpcode.content_to_string().c_str());

    this->currect_opcode[cpuid]++;
    if (this->currect_opcode[cpuid] >= this->group_binary_bbl_size[group][this->currect_bbl[cpuid]]) {
        this->is_inside_bbl[cpuid] = false;
        this->currect_opcode[cpuid] = 0;
    }

    // =========================================================================
    /// Add SiNUCA information
    // =========================================================================
    *m = NewOpcode;
    /// SINUCA control variables.
    m->state = PACKAGE_STATE_UNTREATED;
    m->born_cycle = sinuca_engine.get_global_cycle();
    m->ready_cycle = sinuca_engine.get_global_cycle();
    m->opcode_number = trace_opcode_counter[cpuid];
    m->sync_type = sync_found;
    m->opcode_address |= this->group_address_translation[group]; // Perform the address translation for each group (program trace)

    this->trace_opcode_counter[cpuid]++;
    this->trace_opcode_total++;


    // =========================================================================
    /// If it is LOAD/STORE -> Fetch new MEMORY inside the memory file
    // =========================================================================
    char file_line[TRACE_LINE_SIZE] = "";
    if (m->is_read) {
        this->trace_next_memory(cpuid, file_line);
        m->trace_string_to_read(file_line, this->currect_bbl[cpuid]);
        m->read_address |= this->group_address_translation[group]; // Perform the address translation for each group (program trace)
    }

    if (m->is_read2) {
        this->trace_next_memory(cpuid, file_line);
        m->trace_string_to_read2(file_line, this->currect_bbl[cpuid]);
        m->read2_address |= this->group_address_translation[group]; // Perform the address translation for each group (program trace)
    }

    if (m->is_write) {
        this->trace_next_memory(cpuid, file_line);
        m->trace_string_to_write(file_line, this->currect_bbl[cpuid]);
        m->write_address |= this->group_address_translation[group]; // Perform the address translation for each group (program trace)
    }

    TRACE_READER_DEBUG_PRINTF("CPU[%d] Found Operation [%s]. Found Memory [%s].\n", cpuid, m->content_to_string().c_str(), file_line);
    return OK;
};
